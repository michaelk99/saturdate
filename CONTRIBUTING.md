## Contributing

### Branching strategy

Feature branches should be branched off of `master`

### Pull requests

Open a pull request against master. You can mark all in progress PRs with `WIP` if
needed. When the PR is ready for review, remove the `WIP` and assign
reviewers. Or, open a PR without `WIP` and assign reviewers.

### Release candidates

When you are ready to cut a new release candidate (RC) branch, open a branch as
`rc-[version]`. If there is an active release candidate branch, any bug fixes
should be submitted in 2 PRs, one against the `rc-[version]` and one against
`master`.

### Releases

When consensus is made around the `rc-[version]` branch, a `release-[version]`
tag can be created.
