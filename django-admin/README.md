# Django Admin
This repo holds code for saturdate making up the Django admin application.

### Admin portal
Migrate models
```
kubectl --namespace=saturdate exec -it $(kubectl get pod -l app=django -o jsonpath="{.items[0].metadata.name}") -- python src/manage.py migrate
```

Create a superuser for admin login
```
kubectl --namespace=saturdate exec -it $(kubectl get pod -l app=django -o jsonpath="{.items[0].metadata.name}") -- python src/manage.py createsuperuser
```

Create the django static files
```
kubectl --namespace=saturdate exec -it $(kubectl get pod -l app=django -o jsonpath="{.items[0].metadata.name}") -- python src/manage.py collectstatic
```
