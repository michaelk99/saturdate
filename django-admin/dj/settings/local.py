from dj.settings.base import Base

class Local(Base):
    DEBUG = True
    ALLOWED_HOSTS = ['*']
    ENV_NAME = 'local'
