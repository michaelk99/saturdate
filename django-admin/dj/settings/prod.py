from dj.settings.base import Base

class Prod(Base):
    DEBUG = False
    ALLOWED_HOSTS = ['*']
