"""
WSGI config for django-admin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_CONFIGURATION', 'Prod')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dj.settings.prod')

from configurations import setup
from django.conf import settings
setup()

application = get_wsgi_application()
