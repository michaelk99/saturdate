from django.contrib import admin
from django.db import models

class BarAdmin(admin.ModelAdmin):

    list_display = (
        'bar_name',
        'bar_category',
        'bar_price_range',
        'bar_number_reviews',
        'bar_hood',
        'bar_phone',
        'bar_reservations',
        'bar_credit_card',
        'bar_parking',
        'bar_weel_chair',
        'bar_attire',
        'bar_dancing',
        'bar_happy',
        'bar_outdoor',
        'bar_tv',
        'bar_dogs',
        'bar_pool_table',
        'longitude',
        'latitude',
        'image_url',
        'yelp_url',
        'address',
        'address_display',
    )

    list_filter = (
        'bar_category',
        'bar_price_range',
        'bar_hood',
    )

    search_fields = [
        'bar_name', 'bar_category', 'bar_hood',
    ]

    readonly_fields = (
        'id',
    )

    add_fieldsets = (
        (None, {
            'fields': ('id'),
        }),
    )

    # Other stuff here
    def has_delete_permission(self, request, obj=None):
        return False

__all__ = [
    'BarAdmin',
]
