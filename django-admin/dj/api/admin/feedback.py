from django.contrib import admin
from django.db import models

class FeedbackAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'event_id',
        'profile_id',
        'profile_id_match',
        'did_show',
        'bar_rating',
        'comment',
        'created_at'
    )

    list_filter = (
        'bar_rating',
        'did_show',
    )

    search_fields = [
        'profile_id', 'profile_id_match',
    ]

    readonly_fields = (
        'id',
        'event_id',
        'profile_id',
        'profile_id_match'
    )

    add_fieldsets = (
        (None, {
            'fields': ('id'),
        }),
    )

    # Other stuff here
    def has_delete_permission(self, request, obj=None):
        return False

__all__ = [
    'FeedbackAdmin',
]
