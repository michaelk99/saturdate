from django.contrib.admin import AdminSite
from django.contrib.auth.models import User, Group
from dj.api.models import Bar, Event, Account, Blacklist, MatchesMissed, Feedback
from django.db import models

from dj.api.admin.auth import *
from dj.api.admin.bar import *
from dj.api.admin.event import *
from dj.api.admin.account import *
from dj.api.admin.blacklist import *
from dj.api.admin.matches_missed import *
from dj.api.admin.feedback import *
from redis_admin.admin import RedisAdmin, Meta

class SaturdateAdminSite(AdminSite):
    site_header = 'Saturdate Admin'
    index_title = 'App Control Panel'

saturdate_site = SaturdateAdminSite()

# Auth
saturdate_site.register(User, UserAdmin)
saturdate_site.register(Group, GroupAdmin)

# Bars
saturdate_site.register(Bar, BarAdmin)

# Events
saturdate_site.register(Event, EventAdmin)

# Account
saturdate_site.register(Account, AccountAdmin)

# Blacklist
saturdate_site.register(Blacklist, BlacklistAdmin)

# Matches Missed
saturdate_site.register(MatchesMissed, MatchesMissedAdmin)

# Feedback
saturdate_site.register(Feedback, FeedbackAdmin)

# Redis admin
saturdate_site.register(type('manage', (models.Model,), {'__module__': '', 'Meta': Meta}), RedisAdmin)
