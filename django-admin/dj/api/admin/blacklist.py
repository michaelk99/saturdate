from django.contrib import admin
from django.db import models

class BlacklistAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'profile_id',
        'profile_id_blacklisted',
        'created_at',
        'expires_at',
    )

    list_filter = (
        'created_at',
        'expires_at',
    )

    readonly_fields = (
        'id',
        'profile_id',
        'profile_id_blacklisted',
        'created_at',
        'expires_at',
        'updated_at',
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

__all__ = [
    'BlacklistAdmin',
]
