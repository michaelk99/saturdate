from django.contrib import admin
from django.db import models

class MatchesMissedAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'profiles',
        'created_at',
        'updated_at',
    )

    list_filter = (
        'created_at',
    )

    readonly_fields = (
        'id',
        'profiles',
        'created_at',
        'updated_at',
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

__all__ = [
    'MatchesMissedAdmin',
]
