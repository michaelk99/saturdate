from django.contrib import admin
from django.db import models

class EventAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'start_date',
        'start_time',
        'state',
        'turn',
        'notified',
        'bars_display'
    )

    list_filter = (
        'state',
        'notified',
        'start_date',
    )

    readonly_fields = (
        'id',
        'profiles',
        'match',
    )

    add_fieldsets = (
        (None, {
            'fields': ('id'),
        }),
    )

    # Other stuff here
    def has_delete_permission(self, request, obj=None):
        return False

__all__ = [
    'EventAdmin',
]
