from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

class UserAdmin(BaseUserAdmin):

    search_fields = (
        'email',
        'first_name',
    )

    list_display = (
        'email',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
    )

    readonly_fields = (
        'password',
    )

class GroupAdmin(admin.ModelAdmin):
    filter_horizontal = ('permissions',)

__all__ = [
    'UserAdmin',
    'GroupAdmin',
]
