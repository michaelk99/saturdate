from django.contrib import admin
from django.db import models
from dj.api.models import Profile, Account
from django.contrib.auth.admin import UserAdmin

class ProfileInline(admin.StackedInline):
    model = Profile
    verbose_name = 'Profile'
    can_delete = False
    editable_fields = []

    list_display = (
            'name',
            'gender',
            'birthday',
            'preference_sex',
            'preference_min_age',
            'preference_max_age',
            'city',
            'state',
            'zipcode',
            'latitude',
            'longitude',
    )

    readonly_fields = (
        'id',
        'account_id',
        'images',
    )

    add_fieldsets = (
        (None, {
            'fields': ('id', 'account_id'),
        }),
    )

class AccountAdmin(admin.ModelAdmin):
    inlines = (ProfileInline,)

    list_display = (
        'email',
        'enabled',
        'profile',
        'created_at',
    )

    list_filter = (
        'enabled',
    )

    search_fields = [
        'email',
    ]

    readonly_fields = (
        'id',
    )

    add_fieldsets = (
        (None, {
            'fields': ('id'),
        }),
    )

    # Other stuff here
    def has_delete_permission(self, request, obj=None):
        return False

__all__ = [
    'AccountAdmin',
]
