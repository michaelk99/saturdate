from django.db import models

class Match(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    profiles = models.TextField()  # This field type is a guess.
    notified = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'matches'

    def __str__(self):
        return self.id
