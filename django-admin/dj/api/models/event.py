from django.db import models
from dj.api.models import Match
from django.contrib.postgres.fields import ArrayField

class Event(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    profiles = ArrayField(models.TextField(), default=list)
    notified = ArrayField(models.TextField(), default=list)
    turn = models.CharField(max_length=255, blank=True, null=True)
    state = models.IntegerField(blank=True, null=True)
    bars = ArrayField(models.TextField(), default=list)
    start_date = models.DateTimeField()
    start_time = models.TimeField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    match = models.ForeignKey(
        Match,
        on_delete=models.CASCADE,
        null=False,
        related_name='match')

    class Meta:
        managed = False
        db_table = 'events'

    @property
    def bars_display(self):
        bar_names = []
        for bar in self.bars:
            bar_names.append(bar['bar_name'])
        return bar_names

