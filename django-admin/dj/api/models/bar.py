from django.db import models

class Bar(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    bar_name = models.CharField(max_length=255, blank=True, null=True)
    bar_category = models.CharField(max_length=255, blank=True, null=True)
    bar_price_range = models.CharField(max_length=255, blank=True, null=True)
    bar_number_reviews = models.CharField(max_length=255, blank=True, null=True)
    bar_number_stars = models.CharField(max_length=255, blank=True, null=True)
    bar_hood = models.CharField(max_length=255, blank=True, null=True)
    bar_phone = models.CharField(max_length=255, blank=True, null=True)
    bar_reservations = models.CharField(max_length=255, blank=True, null=True)
    bar_credit_card = models.CharField(max_length=255, blank=True, null=True)
    bar_parking = models.CharField(max_length=255, blank=True, null=True)
    bar_weel_chair = models.CharField(max_length=255, blank=True, null=True)
    bar_attire = models.CharField(max_length=255, blank=True, null=True)
    bar_dancing = models.CharField(max_length=255, blank=True, null=True)
    bar_happy = models.CharField(max_length=255, blank=True, null=True)
    bar_outdoor = models.CharField(max_length=255, blank=True, null=True)
    bar_tv = models.CharField(max_length=255, blank=True, null=True)
    bar_dogs = models.CharField(max_length=255, blank=True, null=True)
    bar_pool_table = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    image_url = models.CharField(max_length=255, blank=True, null=True)
    yelp_url = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    address_display = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bars'
