from django.db import models
from dj.api.models import Account
from enum import Enum
import uuid

GenderChoice = (
    (0, 'Male'),
    (1, 'Female'),
    (2, 'Unspecified'),
)

SexChoice = (
    (0, 'Male'),
    (1, 'Female'),
    (2, 'Both'),
)

class Profile(models.Model):
    id = models.CharField(primary_key=True, default=uuid.uuid4, editable=False, max_length=255)
    images = models.TextField(blank=True, null=True)  # This field type is a guess.
    name = models.CharField(max_length=255, blank=True, null=True)
    gender = models.IntegerField(
        choices=GenderChoice,
        default=GenderChoice[0][0]
    )
    birthday = models.DateTimeField(blank=True, null=True)
    preference_sex = models.IntegerField(
        choices=SexChoice,
        default=SexChoice[1][0],
    )
    preference_min_age = models.IntegerField(blank=True, null=True)
    preference_max_age = models.IntegerField(blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    bio = models.TextField(blank=True, default='', max_length=2048)
    zipcode = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    neighborhood = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    account = models.OneToOneField(
            Account,
            on_delete=models.CASCADE,
    )

    class Meta:
        managed = False
        db_table = 'profile'

    def __str__(self):
        return self.name
