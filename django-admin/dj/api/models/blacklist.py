from django.db import models

class Blacklist(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    profile_id = models.CharField(max_length=255)
    profile_id_blacklisted = models.CharField(max_length=255)
    expires_at = models.DateTimeField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'blacklist'
