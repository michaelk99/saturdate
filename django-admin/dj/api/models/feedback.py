from django.db import models

class Feedback(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    event_id = models.CharField(max_length=255)
    profile_id = models.CharField(max_length=255)
    profile_id_match = models.CharField(max_length=255)
    did_show = models.BooleanField(blank=True, null=True)
    bar_rating = models.SmallIntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'feedback'
        verbose_name_plural = 'feedback'
