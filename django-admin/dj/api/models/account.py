from django.db import models
import uuid
import bcrypt


class Account(models.Model):
    id = models.CharField(primary_key=True, default=uuid.uuid4, editable=False, max_length=255)
    email = models.CharField(unique=True, max_length=255)
    password = models.CharField(max_length=255)
    enabled = models.BooleanField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'account'

    def save(self, *args, **kwargs):
        """ Hash password if set """
        if not self.id and self.password:
            salt = bcrypt.gensalt(rounds=7)
            pswd = self.password.encode('utf-8')
            hashed = bcrypt.hashpw(pswd, salt)
            self.password = hashed.decode('utf-8')

        # Call the "real" save() method.
        super().save(*args, **kwargs)

    def __str__(self):
        return self.email
