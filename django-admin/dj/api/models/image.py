from django.db import models

class Image(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    profile_id = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=255)
    url_fallback = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    approval_score = models.FloatField(blank=True, null=True)
    is_archived = models.BooleanField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'images'
