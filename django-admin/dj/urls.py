"""URL Configuration
"""
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from dj.api.admin import saturdate_site

urlpatterns = [
    path('internal/', saturdate_site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
