//
//  Nothing.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation

struct Nothing: Codable {
    
}

enum APIError: String, Codable, Error {
    case noResponse
    case generic
    case decoding
    case invalidToken
}
