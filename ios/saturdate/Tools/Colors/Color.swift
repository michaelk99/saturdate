//
//  Color.swift
//  Saturdate
//
//  Created by Francois Lambert on 11/28/18.
//

import UIKit

/// Color based on Style Guide
/// - Links : https://projects.invisionapp.com/share/5JO01FZD8M2#/screens/319115289
enum Color: String {
    /// Black #2B2B2B
    case black
    /// Dark Gray #555555
    case darkGray
    /// Gray #7e7e7e
    case gray
    /// Light Gray #d1d1d1
    case lightGray
    /// Extra Light Gray #f1f1f1
    case extraLightGray
    /// White #ffffff
    case white
    /// Green #5cb849
    case green
    /// Light Green #7dc66d
    case lightGreen
    /// Red #ff3700
    case red
    /// Light Red #ff5f33
    case lightRed
    /// Yellow #F5A623
    case yellow
    /// Clear
    case clear
    
    var value: UIColor {
        return UIColor(named: self.rawValue)!
    }
}
