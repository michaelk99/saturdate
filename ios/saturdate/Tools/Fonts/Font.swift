//
//  Fonts.swift
//  Saturdate
//
//  Created by Francois Lambert on 11/28/18.
//

import UIKit

enum Font {
    enum Name: String {
        case regularItalic = "SFCompactDisplay-RegularItalic"
        case regular = "SFCompactDisplay-Regular"
        case mediumItalic = "SFCompactDisplay-MediumItalic"
        case medium = "SFCompactDisplay-Medium"
        case bold = "SFCompactDisplay-Bold"
        case heavy = "SFCompactDisplay-Heavy"
    }
    
    enum ContentType {
        case header(of: Size.Header)
        case body(of: Size.Body)
        case special(of: Size.Special)
        case button(of: Size.Button)
        case form(element: Size.Form)
        case custom(of: Size.Custom)
        
        var size: CGFloat {
            switch self {
            case .body(let size):
                return size.rawValue
            case .header(let size):
                return size.rawValue
            case .special(of: let size):
                return size.rawValue
            case .button(of: let size):
                return size.rawValue
            case .form(element: let size):
                return size.rawValue
            case .custom(of: let size):
                guard case .size (let value) = size else { return 4 }
                return value
            }
        }
    }
    
    enum Size {
        enum Header: CGFloat {
            case small = 12
            case medium = 14
            case large = 24
            case extraLarge = 32
        }
        
        enum Body: CGFloat {
            case tiny = 12
            case small = 14
            case medium = 16
            case large = 20
        }
        
        enum Special: CGFloat {
            case large = 32
            case medium = 28
        }
        
        enum Button: CGFloat {
            case small = 12
            case regular = 14
            case large = 18
        }
        
        enum Form: CGFloat {
            case textfield = 18
            case textfieldLarge = 49
        }
        
        enum Custom {
            case size(_ size: CGFloat)
        }
    }
    
    static func regular(for type: ContentType) -> UIFont {
        return UIFont(name: Name.regular.rawValue, size: type.size) ?? UIFont.systemFont(ofSize: type.size, weight: .regular)
    }
    
    static func bold(for type: ContentType) -> UIFont {
        return UIFont(name: Name.bold.rawValue, size: type.size) ?? UIFont.systemFont(ofSize: type.size, weight: .bold)
    }
    
    static func heavy(for type: ContentType) -> UIFont {
        return UIFont(name: Name.heavy.rawValue, size: type.size) ?? UIFont.systemFont(ofSize: type.size, weight: .bold)
    }
    
    static func medium(for type: ContentType) -> UIFont {
        return UIFont(name: Name.medium.rawValue, size: type.size) ?? UIFont.systemFont(ofSize: type.size, weight: .medium)
    }
}

extension UIFont {
    static func regular(for type: Font.ContentType) -> UIFont {
        return Font.regular(for: type)
    }
    
    static func medium(for type: Font.ContentType) -> UIFont {
        return Font.medium(for: type)
    }
    
    static func bold(for type: Font.ContentType) -> UIFont {
        return Font.bold(for: type)
    }
    
    static func heavy(for type: Font.ContentType) -> UIFont {
        return Font.heavy(for: type)
    }
}
