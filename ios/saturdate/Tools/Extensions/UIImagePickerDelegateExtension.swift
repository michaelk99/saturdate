//
//  UIImagePickerDelegateExtension.swift
//  saturdate
//
//  Created by Ali Ersoz on 7/28/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit

extension UIImagePickerControllerDelegate where Self: UIViewController, Self: UINavigationControllerDelegate {
	func presentImagePicker(type: UIImagePickerController.SourceType = .camera, device: UIImagePickerController.CameraDevice = .rear) {
		let picker = UIImagePickerController()
		picker.delegate = self
		
		if UIImagePickerController.isSourceTypeAvailable(type) {
			picker.sourceType = type
		}
		
		if picker.sourceType == .camera && UIImagePickerController.isCameraDeviceAvailable(device) {
			picker.cameraDevice = device
			// picker.addOverlay()
		}
		
		present(picker, animated: true)
	}
	
	func presentImagePickerTypes() {
		let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		sheet.addAction(UIAlertAction(title: "Take a photo", style: .default) {_ in
			self.presentImagePicker(type: .camera, device: .front)
		})
		
		sheet.addAction(UIAlertAction(title: "Select from album", style: .default) {_ in
			self.presentImagePicker(type: .savedPhotosAlbum)
		})
		
		sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
		
		// for ipad
		if let popover = sheet.popoverPresentationController {
			popover.sourceView = view
			popover.sourceRect = CGRect(origin: view.center, size: .zero)
			popover.permittedArrowDirections = []
		}
		
		present(sheet, animated: true)
	}
	
	func image(fromMediaWithInfo info: [String: Any]) -> UIImage? {
		return info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
	}
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
