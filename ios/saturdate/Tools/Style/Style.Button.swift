//
//  Style.Button.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/1/18.
//

import Foundation
import UIKit

enum ButtonProp: StylingProp {
    case align(_: NSTextAlignment)
    case alignHorizontally(_: UIControl.ContentHorizontalAlignment)
    case title(_: String, `for`: UIControl.State)
    case tag(_: Int)
    case alpha(_: CGFloat)
    case hidden(_: Bool)
    case enabled(_: Bool)
    case lines(_: Int)
    case color(_: Color)
    case colorForSelected(_: Color)
    case backgroundColor(_: Color)
    case backgroundColorForSelected(_: Color)
    case font(_: UIFont)
    case image(_: UIImage?)
    case selectedImage(_: UIImage?)
    case selected(_: Bool)
    case templateImage(_: UIImage)
    case contentMode(_: UIView.ContentMode)
    case spacing(_: CGFloat)
    case imageEdgeInsets(_: UIEdgeInsets)
    case letterSpacing
    case onTap(_: () -> ())
    case contentEdgeInsets(_: UIEdgeInsets)
    
    typealias T = UIButton
    func install(to button: UIButton) {
        switch self {
        case .align(let align):
            button.titleLabel?.textAlignment = align
        case .alignHorizontally(let align):
            button.contentHorizontalAlignment = align
        case .title(let string, let state):
            button.setTitle(string, for: state)
        case .tag(let tag):
            button.tag = tag
        case .alpha(let alpha):
            button.alpha = alpha
        case .hidden(let isHidden):
            button.isHidden = isHidden
        case .enabled(let enabled):
            button.isEnabled = enabled
            button.alpha = enabled ? 1 : 0.7
        case .lines(let count):
            button.titleLabel?.numberOfLines = count
            button.titleLabel?.lineBreakMode = .byWordWrapping
        case .color(let color):
            button.setTitleColor(color.value, for: .normal)
        case .colorForSelected(let color):
            button.setTitleColor(color.value, for: .selected)
        case .backgroundColor(let color):
            guard let button = (button as? Button) else { return }
            button.unselectedBackgroundColor = color
        case .backgroundColorForSelected(let color):
            guard let button = (button as? Button) else { return }
            button.selectedBackgroundColor = color
        case .font(let font):
            button.titleLabel?.font = font
        case .image(let image):
            button.setImage(image, for: .normal)
            button.setImage(image, for: [.normal, .highlighted])
        case .selectedImage(let image):
            button.setImage(image, for: .selected)
            button.setImage(image, for: [.selected, .highlighted])
        case .selected(let selected):
            button.isSelected = selected
        case .templateImage(let image):
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        case .contentMode(let mode):
            button.contentMode = mode
            button.imageView?.contentMode = mode
        case .spacing(let spacing):
            let attributedString = NSMutableAttributedString(string: (button.titleLabel?.text!)!)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSRange(location: 0, length: (button.titleLabel?.text!.count)!))
            button.setAttributedTitle(attributedString, for: .normal)
        case .imageEdgeInsets(let insets):
            button.imageEdgeInsets = insets
        case .letterSpacing:
            guard let label = button.titleLabel else { return }
            LabelProp.letterSpacing(1).install(to: label)
        case .onTap(let closure):
            button.addTapListener { sender in
                closure()
            }
        case .contentEdgeInsets(let contentEdgeInsets):
            button.contentEdgeInsets = contentEdgeInsets
        }
    }
}

enum ButtonStyle {
    // Global
    case block
    case plain
    case selectable
    case swipe
    case none
    
    // Shouldnt exists : no regular font for buttons in the style guide
    case regularPlain
    
    private var style: Style<ButtonProp> {
        switch self {
        case .block:
            return .with(base: [.background(color:  .yellow), .corner(radius: 25)],
                         component: .color(.white), .font(.bold(for: .button(of: .large))))
        case .plain:
            return .with(base: [.background(color: .clear)],
                         component: .color( .black), .font(.regular(for: .button(of: .small))))
        case .selectable:
            return .with(base: [.corner(radius: 16)],
                         component: .backgroundColor(.lightGray), .backgroundColorForSelected(.yellow), .color(.white), .font(.bold(for: .button(of: .small))))
        case .swipe:
            return .with(base: [.corner(radius: 50), .shadow(color: .black, opacity: 0.2, offset: .zero, radius: 5), .background(color: .white)],
                         component: .backgroundColor(.white))
        default:
            return Style()
        }
    }
    
    func install(to button: UIButton) {
        style.install(to: button)
    }
}
