//
//  Style.PageControl.swift
//  Saturdate
//
//  Created by Ali Ersoz on 11/5/18.
//

import Foundation
import UIKit

enum PageControlProp: StylingProp {
    case pageCount(_: Int)
    case currentPage(_: Int)
    case currentPageTint(color: Color)
    case pageTint(color: Color)
    
    typealias T = UIPageControl
    func install(to pageControl: UIPageControl) {
        switch self {
        case .currentPage(let p):
            pageControl.currentPage = p
        case .pageCount(let c):
            pageControl.numberOfPages = c
        case .currentPageTint(let color):
            pageControl.currentPageIndicatorTintColor = color.value
        case .pageTint(let color):
            pageControl.pageIndicatorTintColor = color.value
        }
    }
}

enum PageControlStyle {
    case black
    
    private var style: Style<PageControlProp> {
        switch self {
        case .black:
            return .with(component: .currentPageTint(color: .black), .pageTint(color: .lightGray))
        }
    }
    
    func install(to pageControl: UIPageControl) {
        style.install(to: pageControl)
    }
}
