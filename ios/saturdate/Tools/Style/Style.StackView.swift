//
//  Style.StackView.swift
//  Saturdate
//
//  Created by Andres Trevino on 9/14/18.
//

import Foundation
import UIKit

enum StackViewProp: StylingProp {
    case axis(_: NSLayoutConstraint.Axis)
    case distribution(_: UIStackView.Distribution)
    case spacing(_: CGFloat)
    case alignment(_: UIStackView.Alignment)
    
    typealias T = UIStackView
    func install(to stackView: UIStackView) {
        switch self {
        case .axis(let axis):
            stackView.axis = axis
        case .distribution(let distribution):
            stackView.distribution = distribution
        case .spacing(let spacing):
            stackView.spacing = spacing
        case .alignment(let alignment):
            stackView.alignment = alignment
        }
    }
}
