//
//  Style.ScrollView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 10/16/18.
//

import Foundation
import UIKit

enum ScrollViewProp: StylingProp {
    case contentInset(_: UIEdgeInsets)
    case contentOffset(_: CGPoint)
    case delegate(_: UIScrollViewDelegate)
    case verticalScrollBar(visible: Bool)
    case horizontalScrollBar(visible: Bool)
    case paginated
    
    typealias T = UIScrollView
    func install(to scrollView: UIScrollView) {
        switch self {
        case .contentInset(let insets):
            scrollView.contentInset = insets
        case .contentOffset(let offset):
            scrollView.contentOffset = offset
        case .delegate(let delegate):
            scrollView.delegate = delegate
        case .verticalScrollBar(let visible):
            scrollView.showsVerticalScrollIndicator = visible
        case .horizontalScrollBar(let visible):
            scrollView.showsHorizontalScrollIndicator = visible
        case .paginated:
            scrollView.isPagingEnabled = true
        }
    }
}
