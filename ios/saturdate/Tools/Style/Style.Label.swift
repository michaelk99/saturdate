//
//  Style.swift
//  Saturdate
//
//  Created by Ali Ersoz on 5/23/18.
//

import Foundation
import UIKit
import CoreText

enum LabelProp: StylingProp {
    case align(_: NSTextAlignment)
    case tag(_: Int)
    case alpha(_: CGFloat)
    case hidden(_: Bool)
    case multiline
    case lines(_: Int)
    case text(_: String?)
    case color(_: Color)
    case font(_: UIFont)
    case bold(range: NSRange)
    case singleLine
    case letterSpacing(_: CGFloat)
    case lineSpacing(_: CGFloat, heightMultiple: CGFloat)
    case attribute(_: NSAttributedString.Key, value: Any, for: String)
    case sizeToFit
    case imageAttachement(_: UIImage, position: Position)
    case imageAttachementWithSize(_: UIImage, position: Position, size: CGFloat)
    case minimumScaleFactor(_: CGFloat)
    
    typealias T = UILabel
    func install(to label: UILabel) {
        switch self {
        case .align(let align):
            label.textAlignment = align
        case .tag(let tag):
            label.tag = tag
        case .alpha(let alpha):
            label.alpha = alpha
        case .hidden(let isHidden):
            label.isHidden = isHidden
        case .multiline:
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        case .lines(let count):
            label.numberOfLines = count
            label.lineBreakMode = .byWordWrapping
        case .text(let string):
            if let string = string, label.attributedText != nil {
                label.attributedText = NSAttributedString(string: string)
            } else {
                label.text = string
            }
        case .color(let color):
            label.textColor = color.value
        case .font(let font):
            label.font = font
        case .bold(let range):
            let attributedText = NSMutableAttributedString(string: label.text!)
            attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: label.font.pointSize, weight: .bold), range: range)
            label.attributedText = attributedText
        case .singleLine:
            label.adjustsFontSizeToFitWidth = true
        case .letterSpacing(let spacing):
            letterSpacing(spacing, for: label)
        case .lineSpacing(let spacing, let heightMultiple):
            lineSpacing(spacing, heightMultiple: heightMultiple, for: label)
        case .attribute(let attribute, let value, let string):
            addAttribute(forText: string, attribute: attribute, value: value, for: label)
        case .sizeToFit:
            label.sizeToFit()
        case .imageAttachement(let image, let position):
            imageAttachement(image, position: position, label: label)
        case .imageAttachementWithSize(let image, let position, let size):
            imageAttachement(image, position: position, label: label, size: size)
        case .minimumScaleFactor(let minimumScaleFactor):
            label.minimumScaleFactor = minimumScaleFactor
        }
    }
    
    private func letterSpacing(_ spacing: CGFloat, for label: UILabel) {
        var text: String
        var attributedString  = label.attributedText?.mutableCopy() as? NSMutableAttributedString
        if attributedString == nil {
            text = label.text ?? ""
            attributedString = NSMutableAttributedString(string: text)
        }
        else {
            attributedString?.removeAttribute(.kern, range: NSMakeRange(0, attributedString!.length))
            text = attributedString?.string ?? ""
        }
        
        attributedString!.addAttribute(.kern, value: spacing, range: NSMakeRange(0, text.count))
        label.attributedText = attributedString
    }
    
    private func lineSpacing(_ spacing: CGFloat, heightMultiple: CGFloat = 1, for label: UILabel) {
        var text: String
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.lineHeightMultiple = heightMultiple
        var attributedString  = label.attributedText?.mutableCopy() as? NSMutableAttributedString
        if attributedString == nil {
            text = label.text ?? ""
            attributedString = NSMutableAttributedString(string: text)
        }
        else {
            attributedString?.removeAttribute(.paragraphStyle, range: NSMakeRange(0, attributedString!.length))
            text = attributedString?.string ?? ""
        }
        
        attributedString!.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, text.count))
        label.attributedText = attributedString!
    }
    
    private func paragraphSpacing(_ spacing: CGFloat, for label: UILabel) {
        let attributedString = label.attributedText?.mutableCopy() as? NSMutableAttributedString ?? NSMutableAttributedString(string: label.text ?? "")
        let ranges: [NSRange]
        do {
            let regex = try NSRegularExpression(pattern: "\n", options: [])
            ranges = regex.matches(in: label.text!, options: [], range: NSMakeRange(0, label.text!.count)).map {$0.range}
        }
        catch {
            ranges = []
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.paragraphSpacing = spacing
        
        var lineIndex = 0
        for range in ranges {
            let modifiedRange = NSMakeRange(lineIndex, range.location - lineIndex)
            lineIndex = range.location + range.length
            attributedString.removeAttribute(.paragraphStyle, range: range)
            attributedString.addAttribute(.paragraphStyle, value:paragraphStyle, range:modifiedRange)
        }
        
        label.attributedText = attributedString
    }
    
    private func addAttribute(forText text: String, attribute: NSAttributedString.Key, value: Any, for label: UILabel) {
        guard let labelText = label.text else { return }
        
        let attributedString  = label.attributedText?.mutableCopy() as? NSMutableAttributedString ?? NSMutableAttributedString(string: label.text ?? "")
        guard let range = labelText.range(of: text) else { return }
        
        let nsrange = labelText.nsRange(from: range)
        attributedString.removeAttribute(attribute, range: nsrange)
        attributedString.addAttribute(attribute, value: value, range: nsrange)
        
        label.attributedText = attributedString
    }
    
    private func imageAttachement(_ image: UIImage, position: Position, label: UILabel, size: CGFloat? = nil) {
        let attributedString  = label.attributedText?.mutableCopy() as? NSMutableAttributedString ?? NSMutableAttributedString(string: label.text ?? "")

        let attachment = NSTextAttachment()
        attachment.image = image
        
        let size = size ?? label.font.pointSize
        let width = (image.size.width * size) / image.size.height //Keep the image ratio
        attachment.bounds = CGRect(x: 0, y: label.font.descender, width: width, height: size)
        let attachmentStr = NSAttributedString(attachment: attachment)
        
        let labelString = NSMutableAttributedString(string: "")

        switch position {
        case .prefix:
            labelString.append(attachmentStr)
            labelString.append(NSMutableAttributedString(string: " "))
            labelString.append(attributedString)
        case .suffix:
            labelString.append(attributedString)
            labelString.append(NSMutableAttributedString(string: " "))
            labelString.append(attachmentStr)
        }

        label.attributedText = labelString
    }
}

enum LabelStyle {
    // Global
    case header
    case title
    case smallTitle
    case subtitle
    case error
    case none
    
    private var style: Style<LabelProp> {
        switch self {
        case .header:
            return .with(component: .align(.left), .singleLine, .color(.yellow), .font(.heavy(for: .header(of: .extraLarge))))
        case .title:
            return .with(component: .align(.left), .lines(0), .color(.black), .font(.bold(for: .header(of: .large))))
        case .smallTitle:
            return .with(component: .align(.left), .lines(0), .color(.black), .font(.bold(for: .header(of: .medium))))
        case .subtitle:
            return .with(component: .align(.left), .lines(0), .color(.black), .font(.regular(for: .header(of: .medium))))
        case .error:
            return .with(component: .align(.left), .lines(0), .color(.red), .font(.regular(for: .body(of: .small))))
        case .none:
            return Style()
        }
    }
    
    func install(to label: UILabel) {
        style.install(to: label)
    }
}

enum Position {
    case prefix
    case suffix
}

