//
//  Style.TextField.swift
//  Saturdate
//
//  Created by Ali Ersoz on 7/5/18.
//

import Foundation
import UIKit

enum TextViewProp: StylingProp {
    case align(_: NSTextAlignment)
    case text(_: String)
    case color(_: Color)
    case font(_: UIFont)
    case secure(_: Bool)
    case keyboardType(_: UIKeyboardType)
    case autoCapitalization(type: UITextAutocapitalizationType)
    case autoCorrection(type: UITextAutocorrectionType)
    case spellCheck(type: UITextSpellCheckingType)
    case format(capitalize: UITextAutocapitalizationType, correction: UITextAutocorrectionType, spelling: UITextSpellCheckingType)
    case textInset(horizontal: CGFloat, vertical: CGFloat)
    case placeholder(text: String)
    
    typealias T = UITextView
    func install(to textView: UITextView) {
        switch self {
        case .align(let align):
            textView.textAlignment = align
        case .text(let string):
            textView.text = string
        case .color(let color):
            textView.textColor = color.value
        case .font(let font):
            textView.font = font
        case .secure(let isSecure):
            textView.isSecureTextEntry = isSecure
        case .keyboardType(let type):
            textView.keyboardType = type
        case .autoCorrection(let type):
            textView.autocorrectionType = type
        case .autoCapitalization(let type):
            textView.autocapitalizationType = type
        case .spellCheck(let type):
            textView.spellCheckingType = type
        case .format(let capitalize, let correction, let spelling):
            textView.autocapitalizationType = capitalize
            textView.autocorrectionType = correction
            textView.spellCheckingType = spelling
        case .textInset(let horizontal, let vertical):
            textView.textContainerInset = UIEdgeInsets(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
        case .placeholder(let text):
            (textView as? TextView)?.placeholder = text
        }
    }
}

enum TextViewStyle {
    case standard
    
    private var style: Style<TextViewProp> {
        switch self {
        case .standard:
            return .with(base: [.tint(color: .black), .border(edges: .bottom, width: 1, color: .black)],
                         component: .font(Font.regular(for: .form(element: .textfield))))
        }
    }
    
    func install(to textView: UITextView) {
        style.install(to: textView)
    }
}
