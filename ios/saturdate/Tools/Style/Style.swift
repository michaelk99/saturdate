//
//  Style.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/1/18.
//

import Foundation
import UIKit

protocol StylingProp {
    associatedtype T: UIView
    func install(to: T)
}

final class Style<V: StylingProp> {
    var component = [V]()
    var base = [ViewProp]()
    
    init(base: [ViewProp] = [], component: V...) {
        self.component = component
        self.base = base
    }
    
    static func with(base: [ViewProp] = [], component: V...) -> Style<V> {
        let style = Style<V>()
        style.component = component
        style.base = base
        
        return style
    }
    
    func install(to view: V.T) {
        component.forEach {$0.install(to: view)}
        base.forEach {$0.install(to: view)}
    }
    
    private static func with(_ populator: (inout Style<V>) -> ()) -> Style<V> {
        var style = Style<V>()
        populator(&style)
        return style
    }
}
