//
//  Style.TableView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/8/18.
//

import Foundation
import UIKit

enum TableViewProp: StylingProp {
    case separator(style: UITableViewCell.SeparatorStyle, color: Color, inset: UIEdgeInsets)
    case rowHeight(_: CGFloat)
    case paginate
    case delegate(_: UITableViewDelegate)
    case dataSource(_: UITableViewDataSource)
    case register(cell: AnyClass, identifier: String)
    case bounces(_: Bool)
    case tableHeader(_: UIView)
    case tableFooter(_: UIView)
    case scrollable(_: Bool)
    case showIndicators(_: Bool)
    
    typealias T = UITableView
    func install(to tableView: UITableView) {
        switch self {
        case .separator(let style, let color, let inset):
            tableView.separatorColor = color.value
            tableView.separatorStyle = style
            tableView.separatorInset = inset
        case .rowHeight(let height):
            tableView.rowHeight = height
        case .paginate:
            tableView.isPagingEnabled = true
        case .delegate(let delegate):
            tableView.delegate = delegate
        case .dataSource(let dataSource):
            tableView.dataSource = dataSource
        case .register(let cell, let identifier):
            tableView.register(cell, forCellReuseIdentifier: identifier)
        case .bounces(let bounces):
            tableView.bounces = bounces
        case .tableHeader(let view):
            tableView.tableHeaderView = view
        case .tableFooter(let view):
            tableView.tableFooterView = view
        case .scrollable(let scrollable):
            tableView.isScrollEnabled = scrollable
        case .showIndicators(let show):
            tableView.showsVerticalScrollIndicator = show
            tableView.showsHorizontalScrollIndicator = show
        }
    }
}

enum TableViewStyle {
    case plain
    case grouped
    
    private var style: Style<TableViewProp> {
        switch self {
        case .plain:
            return .with(component: .separator(style: .singleLine, color: .extraLightGray, inset: .zero))
        case .grouped:
            return .with(component: .separator(style: .singleLine, color: .extraLightGray, inset: .zero))
        }
    }
    
    func install(to tableView: UITableView) {
        style.install(to: tableView)
    }
}
