//
//  Style.ScrollView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 10/16/18.
//

import Foundation
import UIKit

enum SliderProp: StylingProp {
    case range(min: Float, max: Float)
    case minValueImage(image: UIImage?)
    case maxValueImage(image: UIImage?)
    case enabled(_: Bool)
    case continuous(_: Bool)

    typealias T = UISlider
    func install(to slider: UISlider) {
        switch self {
        case .range(let min, let max):
            slider.minimumValue = min
            slider.maximumValue = max
        case .minValueImage(let image):
            slider.minimumValueImage = image
        case .maxValueImage(let image):
            slider.maximumValueImage = image
        case .enabled(let enabled):
            slider.isEnabled = enabled
        case .continuous(let continuous):
            slider.isContinuous = continuous
        }
    }
}
