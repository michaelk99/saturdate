//
//  Style.View.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/1/18.
//

import Foundation
import UIKit
// note: if you need to set  both corner radius and shadow, make sure that you are setting corner radius first.
enum ViewProp: StylingProp {
    case background(color: Color)
    case layerBorder(width: CGFloat, color: Color)
    case border(edges: UIRectEdge, width: CGFloat, color: Color)
    case corner(radius: CGFloat)
    case tint(color: Color)
    case hidden(_: Bool)
    case alpha(_: CGFloat)
    case contentMode(_: UIView.ContentMode)
    case shadow(color: Color, opacity: Float, offset: CGSize, radius: CGFloat)
    case shadowWithCornerRadius(color: Color, opacity: Float, offset: CGSize, radius: CGFloat, cornerRadius: CGFloat, backgroundColor: Color)
    case id(_: String)
    case tag(_: Int)
    case clipsToBounds
    case flipHorizontally
    case isUserInteractionEnabled(_: Bool)
    case translatesAutoresizingMaskIntoConstraints(_: Bool)
    case noAutoresizingMask
    
    typealias T = UIView
    func install(to view: UIView) {
        switch self {
        case .background(let color):
            view.backgroundColor = color.value
        case .layerBorder(let width, let color):
            view.layer.borderWidth = width
            view.layer.borderColor = color.value.cgColor
        case .corner(let radius):
            view.layer.cornerRadius = radius
            view.layer.masksToBounds = true
        case .border(let edges, let width, let color):
            view.addBorders(edges: edges, color: color.value, thickness: width)
        case .tint(let color):
            view.tintColor = color.value
        case .hidden(let isHidden):
            view.isHidden = isHidden
        case .alpha(let alpha):
            view.alpha = alpha
        case .contentMode(let mode):
            view.contentMode = mode
        case .shadow(let color, let opacity, let offset, let radius):
            view.layer.shadowColor = color.value.cgColor
            view.layer.shadowOpacity = opacity
            view.layer.shadowOffset = offset
            view.layer.shadowRadius = radius
            view.layer.masksToBounds = false
        case .shadowWithCornerRadius(let color, let opacity, let offset, let shadowRadius, let cornerRadius, let backColor):
            
            // fixme: test applying corner radius first then the shadow.
            if let _ = view.layer.sublayers?.first(where: { $0.name == "shadow"}) {
                return
            }
            
            let shadowLayer = CAShapeLayer()
            shadowLayer.name = "shadow"
            shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = backColor.value.cgColor
            
            shadowLayer.shadowColor = color.value.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = offset
            shadowLayer.shadowOpacity = opacity
            shadowLayer.shadowRadius = shadowRadius
            shadowLayer.masksToBounds = false
            
            view.layer.insertSublayer(shadowLayer, at: 0)
        case .id(let id):
            view.accessibilityIdentifier = id
        case .tag(let tag):
            view.tag = tag
        case .clipsToBounds:
            view.clipsToBounds = true
        case .flipHorizontally:
            view.transform = CGAffineTransform(scaleX: -1, y: 1)
        case .isUserInteractionEnabled(let enabled):
            view.isUserInteractionEnabled = enabled
        case .translatesAutoresizingMaskIntoConstraints(let enabled):
            view.translatesAutoresizingMaskIntoConstraints = enabled
        case .noAutoresizingMask:
            view.translatesAutoresizingMaskIntoConstraints = false
        }
    }
}

enum ViewStyle {
    case greenStatus
    case redStatus
    case yellowStatus
    case shadowedCard
    case none
    
    private var style: Style<ViewProp> {
        switch self {
        case .greenStatus:
            return .with(base: [.corner(radius: 4), .background(color:  .green)])
        case .redStatus:
            return .with(base: [.corner(radius: 4), .background(color:  .red)])
        case .yellowStatus:
            return .with(base: [.corner(radius: 4), .background(color:  .yellow)])
        case .shadowedCard:
            return .with(base: [.corner(radius: 4), .background(color: .white), .shadow(color:  .black, opacity: 0.15, offset: CGSize(width: 0, height: 5), radius: 10)])
        case .none:
            return .with(base: [.corner(radius: 4), .background(color: .clear)])
        }
    }
    
    func install(to textView: UIView) {
        style.install(to: textView)
    }
}

extension UIView {
    func set(viewProps props: ViewProp...) {
        props.forEach { $0.install(to: self) }
    }
}
