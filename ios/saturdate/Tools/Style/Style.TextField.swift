//
//  Style.TextField.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/5/18.
//

import Foundation
import UIKit

enum TextFieldProp: StylingProp {
    case align(_: NSTextAlignment)
    case inputType(_: TextFieldType)
    case text(_: String)
    case placeholder(_: String)
    case tag(_: Int)
    case delegate(_: UITextFieldDelegate)
    case alpha(_: CGFloat)
    case hidden(_: Bool)
    case id(_: String)
    case color(_: Color)
    case font(_: UIFont)
    case secure(_: Bool)
    case keyboardType(_: UIKeyboardType)
    case autoCapitalization(type: UITextAutocapitalizationType)
    case autoCorrection(type: UITextAutocorrectionType)
    case spellCheck(type: UITextSpellCheckingType)
    case format(capitalize: UITextAutocapitalizationType, correction: UITextAutocorrectionType, spelling: UITextSpellCheckingType)
    case nameFormat
    case inputAccessoryView(_: UIView)
    case additionalActions(enable: Bool)
    
    typealias T = UITextField
    func install(to textField: UITextField) {
        switch self {
        case .align(let align):
            textField.textAlignment = align
        case .inputType(let type):
            // fixme: inputtype can be an associated property to UITextField
            guard let tf = textField as? TextField else { return }
            tf.inputType = type
        case .text(let string):
            textField.text = string
        case .placeholder(let string):
            textField.placeholder = string
        case .tag(let tag):
            textField.tag = tag
        case .delegate(let delegate):
            textField.delegate = delegate
        case .alpha(let alpha):
            textField.alpha = alpha
        case .hidden(let isHidden):
            textField.isHidden = isHidden
        case .id(let id):
            textField.accessibilityIdentifier = id
        case .color(let color):
            textField.textColor = color.value
        case .font(let font):
            textField.font = font
        case .secure(let secure):
            textField.isSecureTextEntry = secure
        case .keyboardType(let type):
            textField.keyboardType = type
        case .autoCorrection(let type):
            textField.autocorrectionType = type
        case .autoCapitalization(let type):
            textField.autocapitalizationType = type
        case .spellCheck(let type):
            textField.spellCheckingType = type
        case .format(let capitalize, let correction, let spelling):
            textField.autocapitalizationType = capitalize
            textField.autocorrectionType = correction
            textField.spellCheckingType = spelling
        case .nameFormat:
            textField.autocapitalizationType = .words
            textField.autocorrectionType = .no
            textField.spellCheckingType = .no
        case .inputAccessoryView(let inputAccessoryView):
            textField.inputAccessoryView = inputAccessoryView
        case .additionalActions(let enable):
            (textField as? TextField)?.canPerformActions = enable
        }
    }
}

enum TextFieldStyle {
    case underlined
    case underlinedCentered
    case standard
    case password(secure: Bool)
    case email
    
    private var style: Style<TextFieldProp> {
        switch self {
        case .underlined:
            return .with(base: [.tint(color:  .black), .border(edges: .bottom, width: 1, color: .black)],
                         component: .font(.medium(for: .form(element: .textfield))))
        case .underlinedCentered:
            return .with(base: [.tint(color:  .black)],
                         component: .font(.medium(for: .form(element: .textfieldLarge))), .align(.center), .keyboardType(.numberPad))
        case .password(let secure):
            return .with(base: [.tint(color:  .black), .border(edges: .bottom, width: 1, color: .black)],
                         component: .font(.medium(for: .form(element: .textfield))), .secure(secure), .format(capitalize: .none, correction: .no, spelling: .no))
        case .email:
        	return .with(component: .color( .black), .font(.medium(for: .form(element: .textfield))), .keyboardType(.emailAddress), .format(capitalize: .none, correction: .no, spelling: .no))
        case .standard:
            return .with(base: [.tint(color: .black)],
                         component: .font(Font.medium(for: .form(element: .textfield))))
        }
    }
    
    func install(to textField: UITextField) {
        style.install(to: textField)
    }
}
