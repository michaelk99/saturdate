//
//  Style.ImageView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/6/18.
//

import Foundation
import UIKit
import Kingfisher

typealias ImageCallback = (UIImage?, NSError?) -> ()

enum ImageViewProp: StylingProp {
    case contentMode(_: UIView.ContentMode)
    case color(_: Color)
    case alpha(_: CGFloat)
    case url(_: String)
    case image(_: UIImage?)
    
    typealias T = UIImageView
    func install(to imageView: UIImageView) {
        switch self {
        case .contentMode(let mode):
            imageView.contentMode = mode
        case .alpha(let alpha):
            imageView.alpha = alpha
        case .color(let color):
            imageView.tintColor = color.value
        case .image(let image):
            imageView.image = image
        case .url(let url):
            imageView.kf.setImage(with: URL(string: url))
        }
    }
}

enum ImageViewStyle {
    case circular(radius: CGFloat)
    case standard
    case tint(color: Color)
    
    private var style: Style<ImageViewProp> {
        switch self {
        case .circular(let radius):
            return .with(base: [.corner(radius: radius)], component: .contentMode(.scaleAspectFill))
        case .standard:
            return .with(component: .contentMode(.scaleAspectFit))
        case .tint(let color):
            return .with(component: .contentMode(.scaleAspectFit), .color(color))
        }
    }
    
    func install(to imageView: UIImageView) {
        style.install(to: imageView)
    }
}
