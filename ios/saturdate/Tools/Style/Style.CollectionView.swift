//
//  Style.CollectionView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/8/18.
//

import Foundation
import UIKit

enum CollectionViewProp: StylingProp {
    case paginate
    case delegate(_: UICollectionViewDelegate)
    case dataSource(_: UICollectionViewDataSource)
    case showIndicators(_: Bool)
    case registerCell(_: AnyClass, identifier: String)
    case registerHeader(_: AnyClass, identifier: String)
    case scrollEnabled(_: Bool)
    case delaysContentTouches(_: Bool)
    case bounces(_: Bool)
    case pullToRefresh(_: UIRefreshControl)
    case contentInset(_: UIEdgeInsets)
    case scrollIndicatorInset(_: UIEdgeInsets)
    
    typealias T = UICollectionView
    func install(to collectionView: UICollectionView) {
        switch self {
        case .paginate:
            collectionView.isPagingEnabled = true
        case .delegate(let delegate):
            collectionView.delegate = delegate
        case .dataSource(let dataSource):
            collectionView.dataSource = dataSource
        case .showIndicators(let show):
            collectionView.showsVerticalScrollIndicator = show
            collectionView.showsHorizontalScrollIndicator = show
        case .registerCell(let cellClass, let identifier):
            collectionView.register(cellClass, forCellWithReuseIdentifier: identifier)
        case .registerHeader(let suppClass, let identifier):
            collectionView.register(suppClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: identifier)
        case .scrollEnabled(let enabled):
            collectionView.isScrollEnabled = enabled
        case .delaysContentTouches(let enabled):
            collectionView.delaysContentTouches = enabled
        case .bounces(let bounces):
            collectionView.bounces = bounces
        case .pullToRefresh(let refreshControl):
            collectionView.refreshControl = refreshControl
        case .contentInset(let insets):
            collectionView.contentInset =  insets
        case .scrollIndicatorInset(let insets):
            collectionView.scrollIndicatorInsets = insets
        }
    }
}

enum CollectionViewStyle {
    case horizontal
    case vertical
    case horizontalPaginated
    
    private var style: Style<CollectionViewProp> {
        switch self {
        case .horizontal, .vertical:
            return Style<CollectionViewProp>()
        case .horizontalPaginated:
            return .init(component: .paginate)
        }
    }
    
    func install(to collectionView: UICollectionView) {
        style.install(to: collectionView)
    }
    
    var layout: UICollectionViewLayout {
        switch self {
        case .horizontal, .horizontalPaginated:
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            return layout
        case .vertical:
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
            return layout
        }
    }
}
