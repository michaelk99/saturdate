//
//  Style.TableViewCell.swift
//  Saturdate
//
//  Created by Ali Ersoz on 7/14/18.
//

import Foundation
import UIKit

enum TableViewCellProp: StylingProp {
    case separator(inset: UIEdgeInsets)
    case selectionStyle(_: UITableViewCell.SelectionStyle)
    case accessoryType(_: UITableViewCell.AccessoryType)
    case background(view: UIView)
    
    typealias T = UITableViewCell
    func install(to cell: UITableViewCell) {
        switch self {
        case .separator(let inset):
            cell.separatorInset = inset
        case .selectionStyle(let style):
            cell.selectionStyle = style
        case .accessoryType(let type):
            cell.accessoryType = type
        case .background(let view):
            cell.backgroundView = view
        }
    }
}

extension UITableViewCell {
    func set(props: TableViewCellProp...) {
        props.forEach { $0.install(to: self) }
    }
}
