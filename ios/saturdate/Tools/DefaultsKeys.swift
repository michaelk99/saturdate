//
//  UserDefaults.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let token = DefaultsKey<String?>("token")
}

