//
//  Screen.swift
//  flow-playground
//
//  Created by Ali Ersoz on 11/19/18.
//  Copyright © 2018 Saturdate. All rights reserved.
//

import Foundation
import UIKit

protocol ScreenActionDelegate: AnyObject {
    associatedtype S: Screen
    
    func onAction(_ action: S.A, for: S)
}

protocol ScreenViewController: AnyObject {
    associatedtype S: Screen
    var screen: S? { get set }
    
    func onReaction(_ reaction: S.R)
}

protocol Screen: AnyObject {
    associatedtype R: Reaction
    associatedtype A: Action
    associatedtype SRD: ScreenViewController & UIViewController
    associatedtype FAD: FlowActionDelegate
    
    var viewController: SRD { get }
    var actionDelegate: FAD? { get }
    
    func trigger(flow action: FlowAction)
    func trigger(screen action: ScreenAction)
    func trigger(navigation action: NavigationAction)

    func trigger(action: A)
    func trigger(reaction: R)
}

extension Screen {
    func trigger(flow action: FlowAction) {
        actionDelegate?.onAction(action)
    }
    
    func trigger(screen action: ScreenAction) {
        actionDelegate?.onAction(action)
    }

    func trigger(navigation action: NavigationAction) {
        actionDelegate?.onAction(action)
    }
}
