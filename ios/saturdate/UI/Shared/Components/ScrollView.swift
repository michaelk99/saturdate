//
//  ScrollView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 7/26/18.
//

import Foundation
import UIKit

class ScrollView: UIScrollView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(props: ScrollViewProp...) {
        props.forEach { $0.install(to: self) }
    }
}
