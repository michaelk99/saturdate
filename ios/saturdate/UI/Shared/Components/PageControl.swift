//
//  File.swift
//  Saturdate
//
//  Created by Ali Ersoz on 11/5/18.
//

import Foundation
import UIKit

class PageControl: UIPageControl {
    var style: PageControlStyle = .black {
        didSet {
            style.install(to: self)
        }
    }
    
    convenience init(style: PageControlStyle) {
        self.init(frame: .zero)
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.style = style
        }
    }
    
    func set(props: PageControlProp...) {
        props.forEach { $0.install(to: self) }
    }
}
