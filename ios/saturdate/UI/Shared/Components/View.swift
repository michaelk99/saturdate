//
//  View.swift
//  Saturdate
//
//  Created by Ali Ersoz on 4/9/18.
//

import Foundation
import UIKit
import SnapKit

class View: UIView {
    var didSetupConstraints = false
    
    var viewStyle: ViewStyle = .none {
        didSet {
            viewStyle.install(to: self)
        }
    }
    
    convenience init(style: ViewStyle) {
        self.init(frame: .zero)
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.viewStyle = style
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        set(viewProps: .noAutoresizingMask)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

