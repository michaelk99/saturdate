//
//  Label.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/1/18.
//

import Foundation
import UIKit

class Label: UILabel {
    var style: LabelStyle = .none {
        didSet {
            style.install(to: self)
        }
    }
    
    convenience init(style: LabelStyle) {
        self.init(frame: .zero)
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.style = style
        }
    }
    
    func set(props: LabelProp...) {
        props.forEach { $0.install(to: self) }
    }
}
