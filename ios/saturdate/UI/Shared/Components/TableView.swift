//
//  TableView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/8/18.
//

import Foundation
import UIKit

class TableView: UITableView {
    var uiStyle: TableViewStyle = .plain {
        didSet {
            uiStyle.install(to: self)
        }
    }
    
    convenience init(style: TableViewStyle) {
        self.init(frame: .zero, style: style == .plain ? .plain : .grouped)
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.uiStyle = style
        }
    }
    
    func set(props: TableViewProp...) {
        props.forEach { $0.install(to: self) }
    }
}


