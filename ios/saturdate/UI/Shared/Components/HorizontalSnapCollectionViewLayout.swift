//
//  HorizontalSnapCollectionViewLayout.swift
//  Saturdate
//

import UIKit

class HorizontalSnapCollectionViewLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
        }
        
        var offsetAdjustment: CGFloat = .greatestFiniteMagnitude
        let horizontalOffset: CGFloat = proposedContentOffset.x + collectionView.contentInset.left

        let targetRect: CGRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)

        let layoutAttrArray = super.layoutAttributesForElements(in: targetRect)

        layoutAttrArray?.forEach({ (layoutAttrs) in
            let itemOffset = layoutAttrs.frame.origin.x

            if fabsf(Float(itemOffset - horizontalOffset)) < fabsf(Float(offsetAdjustment)) {
                offsetAdjustment = itemOffset - horizontalOffset
            }
        })

        return CGPoint(x: proposedContentOffset.x + offsetAdjustment - sectionInset.left, y: proposedContentOffset.y)
    }
    
}
