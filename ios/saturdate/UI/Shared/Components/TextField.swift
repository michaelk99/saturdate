//
//  TextField.swift
//  Saturdate
//
//  Created by Ali Ersoz on 4/12/18.
//

import Foundation
import UIKit

class TextField: UITextField {
    var style: TextFieldStyle = .standard {
        didSet {
            style.install(to: self)
        }
    }
    
    var info: String? {
        didSet {
            infoLabel.text = info
        }
    }
    
    var canPerformActions: Bool = true
    var inputType: TextFieldType = .standard
    var isValid: Bool {
        guard let text = self.text else {
            if case .standard = inputType {
                return true
            }
            
            return false
        }
        
        var valid = true
        switch inputType {
        case .email:
            valid = text.isValidEmail
        case .password:
            valid = text.count >= 6
        case .required(let min, let max):
            valid = min...max ~= text.count
        default:
            valid = true
        }
        
        info = valid ? nil : inputType.message
        return valid
    }

    var didSetupConstraints = false
    var infoLabel = Label(style: .error)

    convenience init(style: TextFieldStyle) {
        self.init(frame: .zero)
        
        set(viewProps: .noAutoresizingMask)
        addSubview(infoLabel)
        
        defer {
            self.style = style
            if let accessoryView = style.accessoryView(target: self) {
                // accessoryView.right?.setImage(isSecureTextEntry ? #imageLiteral(resourceName: "show_hide.png"): #imageLiteral(resourceName: "show_hide_active"), for: .normal)
                accessoryView.right?.addTarget(self, action: #selector(rightAccessoryTapped), for: .touchUpInside)
                self.rightView = accessoryView.right
                
                accessoryView.left?.addTarget(self, action: #selector(leftAccessoryTapped), for: .touchUpInside)
                self.leftView = accessoryView.left
                
                self.rightViewMode = .always
                self.leftViewMode = .always
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        if didSetupConstraints || infoLabel.superview == nil {
            return
        }
        
        infoLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(15)
            make.height.equalTo(15)
        }
        
        didSetupConstraints = true
    }
    
    func set(props: TextFieldProp...) {
        props.forEach { $0.install(to: self) }
    }
    
    @objc
    private func leftAccessoryTapped(_ button: Button) {
        switch style {
        default:
            return
        }
    }
    
    @objc
    private func rightAccessoryTapped(_ button: Button) {
        switch style {
        case .password:
            isSecureTextEntry = !isSecureTextEntry
            button.setImage(isSecureTextEntry ? #imageLiteral(resourceName: "show_hide.png"): #imageLiteral(resourceName: "show_hide_active"), for: .normal)
            
            if isSecureTextEntry {
                // todo: take the font from the style
                font = nil
                font = Font.regular(for: .form(element: .textfield))
            }
        default:
            return
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return canPerformActions
    }
}

extension TextFieldStyle {
    func accessoryView(target: Any) -> (left: Button?, right: Button?)? {
        switch self {
        case .password:
            let button = Button(style: .plain)
//            button.setImage(#imageLiteral(resourceName: "show_hide.png"), for: .normal)
            button.tag = 1
            button.snp.makeConstraints { (make) in
                make.width.height.equalTo(25)
            }
            
            return (left: nil, right: button)    
        default:
            return nil
        }
    }
}

enum TextFieldType {
    case email
    case password
    case required(min: Int, max: Int)
    case standard
    
    var message: String? {
        switch self {
        case .email:
            return "Invalid Email"
        case .password:
            return "Invalid Password"
        case .required(let min, let max):
            return String.init(format: "Min: %d Max: %d characters", min, max)
        default:
            return nil
        }
    }
}
