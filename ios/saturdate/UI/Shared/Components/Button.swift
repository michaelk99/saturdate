//
//  Button.swift
//  Saturdate
//
//  Created by Ali Ersoz on 4/9/18.
//

import Foundation
import UIKit

class Button: UIButton {
    var style: ButtonStyle = .none {
        didSet {
            style.install(to: self)
        }
    }
    
    private var spinnerView: UIActivityIndicatorView?
    
    override var isHighlighted: Bool {
        didSet {
            set(props: .alpha(isHighlighted ? 0.85 : 1))
        }
    }
    
    var selectedBackgroundColor: Color?
    var unselectedBackgroundColor: Color?
    override var isSelected: Bool {
        didSet {
            guard let selectedColor = selectedBackgroundColor, let unselectedColor = unselectedBackgroundColor else { return }
            set(viewProps: .background(color: isSelected ? selectedColor : unselectedColor))
        }
    }
    
    convenience init(style: ButtonStyle) {
        self.init(frame: .zero)
        defer {
            self.style = style
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        set(viewProps: .noAutoresizingMask)
        
        addTarget(self, action: #selector(defaultTap), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimating() {
        if spinnerView == nil {
            spinnerView = UIActivityIndicatorView(style: .white)
            addSubview(spinnerView!)
            
            spinnerView?.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
            })
        }
        
        titleLabel?.removeFromSuperview()
        spinnerView?.startAnimating()
        isEnabled = false
    }
    
    func stopAnimating() {
        isEnabled = true
        addSubview(titleLabel!)
        spinnerView?.stopAnimating()
    }
        
    func set(props: ButtonProp...) {
        props.forEach { $0.install(to: self) }
    }
    
    @objc
    func defaultTap() {
        hapticFeedback()
    }
}
