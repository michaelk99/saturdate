//
//  SegmentedControl.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/27/18.
//

import Foundation
import UIKit
import SnapKit

class SegmentedControl: UIControl {
    private var labels = [Label]()
    private var options = [String]()
    private var selectionView = View()
    private var courierLabel = Label(style: .title)
    
    private var tgr = UITapGestureRecognizer()
    private var didSetupConstraints = false
    var selectedIndex: Int = -1 {
        didSet {
            guard oldValue != selectedIndex else { return }
            selectItem(at: selectedIndex)
        }
    }
    
    var selectedValue: String {
        return options[selectedIndex]
    }
    
    var courierText: String? {
        didSet {
            courierLabel.text = courierText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(options: [String], style: LabelStyle = .title) {
        self.init(frame: .zero)
        set(viewProps: .noAutoresizingMask)
        
        self.options = options
        for (i, o) in options.enumerated() {
            let label = Label(style: style)
            label.set(props: .text(o), .align(.center), selectedIndex == i ? .alpha(1) : .alpha(0.2))
            labels.append(label)
            addSubview(label)
        }
        
        selectionView.set(viewProps: .hidden(selectedIndex < 0))
        selectionView.backgroundColor = Color.black.value
        addSubview(selectionView)
        
        courierLabel.set(viewProps: .hidden(selectedIndex < 0))
        courierLabel.set(props: .alpha(0.7))
        addSubview(courierLabel)
        
        tgr.addTarget(self, action: #selector(tapped))
        addGestureRecognizer(tgr)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        if didSetupConstraints {
            return
        }
        
        var leftView: UILabel? = nil
        for l in labels {
            l.snp.makeConstraints { (make) in
                if let lv = leftView {
                    make.left.equalTo(lv.snp.right)
                    make.width.equalTo(lv)
                }
                else {
                    make.left.equalToSuperview()
                }
                
                make.top.bottom.equalToSuperview()
            }
            
            leftView = l
        }
        
        leftView?.snp.makeConstraints({ (make) in
            make.right.equalToSuperview()
        })
        
        selectionView.snp.makeConstraints { (make) in
            make.height.equalTo(2)
            make.width.equalTo(leftView!)
            make.left.equalTo(labels[max(selectedIndex, 0)].snp.left)
            make.bottom.equalToSuperview().inset(5)
        }
        
        courierLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(selectionView)
            make.top.equalTo(selectionView.snp.bottom)
        }
        
        didSetupConstraints = true
    }
    
    @objc
    private func tapped() {
        let point = tgr.location(in: self)
        guard let label = labels.first else { return }
        
        let index = Int(floor(point.x / label.frame.width))
        guard index != selectedIndex else { return }
        selectedIndex = index
        
        sendActions(for: .valueChanged)
        let notification = UIImpactFeedbackGenerator(style: .light)
        notification.impactOccurred()
    }
    
    private func selectItem(at index: Int) {
        guard let label = labels.first else { return }
        if didSetupConstraints {
            selectionView.snp.remakeConstraints { (make) in
                make.height.equalTo(2)
                make.width.equalTo(label)
                make.left.equalTo(labels[selectedIndex].snp.left)
                make.bottom.equalToSuperview().inset(5)
            }
        }
        
        let wasHidden = selectionView.isHidden
        selectionView.set(viewProps: .hidden(selectedIndex < 0))
        courierLabel.set(viewProps: .hidden(selectedIndex < 0))
        setNeedsUpdateConstraints()
        if wasHidden {
            self.labels.forEach { $0.set(props: .alpha(0.2)) }
            self.labels[index].set(props: .alpha(1))
            self.layoutIfNeeded()
        }
        else {
            UIView.animate(withDuration: 0.3) {
                self.labels.forEach { $0.set(props: .alpha(0.2)) }
                self.labels[index].set(props: .alpha(1))
                self.layoutIfNeeded()
            }
        }
    }
}
