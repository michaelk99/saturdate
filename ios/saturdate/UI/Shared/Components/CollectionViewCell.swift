//
//  CollectionViewCell.swift
//  Saturdate
//
//  Created by Jonathan Moraly on 11/7/18.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    var didSetupConstraints = false
    var interactiveTouch = false
    
    override var isHighlighted: Bool {
        didSet {
            if !interactiveTouch { return }

            var highlightTransform = CGAffineTransform(scaleX: 1, y: 1)

            if isHighlighted {
                highlightTransform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            }

            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                self.transform = highlightTransform
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        set(viewProps: .noAutoresizingMask)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
