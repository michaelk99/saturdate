//
//  ImageView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/7/18.
//

import Foundation
import UIKit

class ImageView: UIImageView {
    var style: ImageViewStyle = .standard {
        didSet {
            style.install(to: self)
        }
    }
    
    convenience init(style: ImageViewStyle, image: UIImage? = nil) {
        self.init(frame: .zero)
        set(props: .image(image))
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.style = style
        }
    }
    
    func set(props: ImageViewProp...) {
        props.forEach { $0.install(to: self) }
    }
}
