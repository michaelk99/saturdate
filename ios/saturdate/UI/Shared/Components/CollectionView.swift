//
//  CollectionView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/8/18.
//

import Foundation
import UIKit

class CollectionView: UICollectionView {
    var style: CollectionViewStyle = .horizontal {
        didSet {
            style.install(to: self)
        }
    }
    
    convenience init(style: CollectionViewStyle) {
        self.init(frame: .zero, collectionViewLayout: style.layout)
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.style = style
        }
    }
    
    func set(props: CollectionViewProp...) {
        props.forEach { $0.install(to: self) }
    }
}
