//
//  StackView.swift
//  Saturdate
//
//  Created by Andres Trevino on 9/14/18.
//

import Foundation
import UIKit

class StackView: UIStackView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        set(viewProps: .noAutoresizingMask)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(props: StackViewProp...) {
        props.forEach { $0.install(to: self) }
    }
}
