//
//  Slider.swift
//  Saturdate
//
//  Created by Ali Ersoz on 8/22/18.
//

import Foundation
import UIKit

class Slider: UISlider {
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(props: SliderProp...) {
        props.forEach { $0.install(to: self) }
    }
}
