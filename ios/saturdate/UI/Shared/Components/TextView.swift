//
//  TextView.swift
//  Saturdate
//
//  Created by Ali Ersoz on 6/7/18.
//

import Foundation
import UIKit

class TextView: UITextView {
    var style: TextViewStyle = .standard {
        didSet {
            style.install(to: self)
        }
    }
    
    private var placeholderLabel: Label?
    var placeholder: String? = nil {
        didSet {
            if placeholderLabel == nil {
                // addPlaceholder()
            }
            
            placeholderLabel?.set(props: .text(placeholder))
        }
    }
    
    convenience init(style: TextViewStyle) {
        self.init(frame: .zero)
        set(viewProps: .noAutoresizingMask)
        
        defer {
            self.style = style
        }
    }
    
    func set(props: TextViewProp...) {
        props.forEach { $0.install(to: self) }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let placeholderLabel = placeholderLabel {
            placeholderLabel.snp.remakeConstraints { (make) in
                make.top.equalToSuperview().inset(textContainerInset.top)
                make.left.equalToSuperview().inset(textContainerInset.left)
                make.width.equalTo(frame.size.width - textContainerInset.left  - textContainerInset.right)
            }
        }
    }
    
    @objc
    private func didBeginEditing() {
        placeholderLabel?.set(viewProps: .hidden(true))
    }
    
    @objc
    private func didEndEditing() {
        placeholderLabel?.set(viewProps: .hidden(text.count != 0))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
