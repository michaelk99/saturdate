//
//  TableViewCell.swift
//  Saturdate
//
//  Created by Ali Ersoz on 4/11/18.
//

import Foundation
import UIKit

class TableViewCell: UITableViewCell {
    var didSetupConstraints = false
    var interactiveTouch = false
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        set(viewProps: .noAutoresizingMask)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if !interactiveTouch { return }
        
        var highlightTransform = CGAffineTransform(scaleX: 1, y: 1)
        
        if highlighted {
            highlightTransform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.transform = highlightTransform
            self.layoutIfNeeded()
        }, completion: nil)
    }
}
