//
//  LocationManager.swift
//  saturdate
//
//  Created by Ali Ersoz on 4/6/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    static let shared = LocationManager()
    
    private let locationManager = CLLocationManager()
    private var permissionRequestCallback: ((CLAuthorizationStatus) -> Void)?
    private var locationUpdateCallback: ((CLLocation) -> Void)?
    
    private override init() {
        super.init()
        locationManager.delegate = self
    }
    
    var authorizationStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    func requestPersmission(_ completion: @escaping (CLAuthorizationStatus) -> Void) {
        guard authorizationStatus == .notDetermined else { return completion(authorizationStatus) }
        
        locationManager.requestAlwaysAuthorization()
        permissionRequestCallback = completion
    }
    
    func requestLocation(_ completion: @escaping (CLLocation) -> Void) {
        locationUpdateCallback = completion
        let status = CLLocationManager.authorizationStatus()
        guard status == .authorizedAlways || status == .authorizedWhenInUse else { return }
        
        locationManager.requestLocation()
    }
    
    func addLocationObserver() {
        
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        debugPrint("location auth status: \(status)")
        permissionRequestCallback?(status)
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.requestLocation()
        default:
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        debugPrint("location update success: \(locations)")
        guard let location = locations.first else { return }
        
        locationUpdateCallback?(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugPrint("location update error: \(error)")
    }
}
