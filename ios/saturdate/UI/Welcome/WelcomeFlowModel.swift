//
//  WelcomeFlowModel.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
typealias BoolClosure = (Bool) -> ()

class WelcomeFlowModel {
    let api = APIClient.shared
    var auth: Auth? {
        didSet {
            guard let token = auth?.token else { return }
            Defaults[.token] = token
        }
    }
    
    var account: Account?
    var profile: Profile?
    private var authViewModel: AuthScreen.ViewModel?
    
    func login(model: AuthScreen.ViewModel, _ completion: @escaping (AuthError?) -> Void) {
        guard let email = model.email, let password = model.password else { return completion(.invalidCredentials) }
        guard isValidEmail(email) else { return completion(.invalidEmail) }
        guard password.count >= 6 else { return completion(.weakPassword) }
        
        authViewModel = model
        api.send(request: .login(email, password))
            .success { [weak self] (auth: Auth) in
                debugPrint("auth: \(auth)")
                self?.auth = auth
            }
            .finalize { [weak self] (success, _) in
                guard success else {
                    completion(.invalidCredentials)
                    return
                }
                
                self?.fetchProfile { (success) in
                    completion(success ? nil : .profileNotFound)
                }
        	}
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailFormat: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate: NSPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
    }
    
    func register(model: AuthScreen.ViewModel, _ completion: @escaping (AuthError?) -> Void) {
        guard let email = model.email, let password = model.password else { return completion(.invalidCredentials) }
        guard isValidEmail(email) else { return completion(.invalidEmail) }
        guard password.count >= 6 else { return completion(.weakPassword) }
        
        authViewModel = model
        api.send(request: .register(email, password))
            .success { [weak self] (account: Account) in
                self?.account = account
                debugPrint("account created: \(account)")
            }
            .finalize { [weak self] (success, statusCode) in
                guard success else {
                    completion(.emailInUse)
                    return
                }
                
                self?.login(model: model, completion)
	        }
    }
    
    func fetchProfile(_ completion: @escaping BoolClosure) {
        api.send(request: .profile)
            .success { [weak self] (profile: Profile) in
                self?.profile = profile
            }
            .finalize { (success, statusCode) in
                completion(success)
            }
    }
    
    func save(viewModel: ProfileInfoStepScreen.ViewModel) -> Bool {
        guard let name = viewModel.name, let birthday = viewModel.birthday else { return false }
        
        self.profile = Profile()
        profile?.name = name
        profile?.birthday = birthday
        profile?.age = 35
        
        return true
    }
    
    func save(viewModel: ProfilePreferencesStepScreen.ViewModel) -> Bool {
        guard let gender = viewModel.gender, let lookingFor = viewModel.lookingFor else { return false }
        
        let preferences = Preference(gender: lookingFor, minAge: 18, maxAge: 30)
        profile?.preferences = preferences
        profile?.gender = gender

        return true
    }
    
    func createProfile(_ completion: @escaping BoolClosure) {
        guard let profile = profile else { return completion(false) }
        
        debugPrint("profile: \(profile.dictionary!)")
        api.send(request: .createProfile(profile))
            .success { [weak self] (profile: Profile) in
                debugPrint("returned profile: \(profile)")
                self?.profile = profile
            }
            .finalize { [weak self] (success, statusCode) in
                guard let self = self, let avm = self.authViewModel else { return }
                guard success else { return completion(success) }
                
                self.login(model: avm, { (err) in
                    completion(success)
                })
            }
    }
}

enum AuthError: Error {
    case invalidEmail
    case weakPassword
    case invalidCredentials
    case emailInUse
    case profileNotFound
    case unexpected
    
    var title: String {
        switch self {
        case .emailInUse:
            return "Email address is in use"
        case .weakPassword:
            return "Password is less than 6 characters"
        case .invalidCredentials:
            return "Email or password is invalid"
        case .invalidEmail:
            return "Email is not valid"
        case .profileNotFound:
            return "Profile not found"
        case .unexpected:
            return "An unexpected error happened"
        }
    }
}
