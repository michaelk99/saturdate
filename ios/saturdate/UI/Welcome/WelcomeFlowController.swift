//
//  WelcomeFlowController.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation

class WelcomeFlowController: FlowController, WelcomeScreenActionDelegate {
    let flowModel = WelcomeFlowModel()
    
    override func onNavigate(to screen: ScreenResult) {
        switch screen {
        case .auth(let screen):
            screen.actionDelegate = self
        case .welcome(let screen):
            screen.actionDelegate = self
        case .profileInfoStep(let screen):
            screen.actionDelegate = self
        case .profilePreferencesStep(let screen):
            screen.actionDelegate = self
        default:
            return
        }
    }
}

extension WelcomeFlowController: AuthScreenActionDelegate {
    func onAction(_ action: AuthScreen.Actions, for screen: AuthScreen) {
        switch action {
        case .login:
            debugPrint("log in will start here")
            flowModel.login(model: screen.viewModel) { authError in
                guard let authError = authError else {
                    screen.trigger(flow: .home)
                    return
                }
                
                switch authError {
                case .profileNotFound:
                    screen.trigger(screen: .profileInfoStep)
                default:
                    screen.trigger(reaction: .problem(authError))
                }
            }
        case .register:
            debugPrint("register will start here")
            flowModel.register(model: screen.viewModel) { error in
                guard let authError = error else {
                    screen.trigger(flow: .home)
                    return
                }
                
                switch authError {
                case .profileNotFound:
                    screen.trigger(reaction: .allGood)
                default:
                    screen.trigger(reaction: .problem(authError))
                }
            }
        }
    }
}

extension WelcomeFlowController: ProfileInfoStepScreenActionDelegate {
    func onAction(_ action: ProfileInfoStepScreen.Actions, for screen: ProfileInfoStepScreen) {
        switch action {
        case .continue:
            let success = flowModel.save(viewModel: screen.viewModel)
            screen.trigger(reaction: success ? .allgood : .problem)
        }
    }
}

extension WelcomeFlowController: ProfilePreferencesStepScreenActionDelegate {
    func onAction(_ action: ProfilePreferencesStepScreen.Actions, for screen: ProfilePreferencesStepScreen) {
        switch action {
        case .continue:
            let success = flowModel.save(viewModel: screen.viewModel)
            screen.trigger(reaction: success ? .allgood : .problem)
            flowModel.createProfile { (success) in
                if success {
                    screen.trigger(flow: .home)
                }
                else {
                    screen.trigger(reaction: success ? .allgood : .problem)
                }
            }
        }
    }
}
