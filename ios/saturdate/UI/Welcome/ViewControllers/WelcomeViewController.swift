//
//  WelcomeViewController.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit

class WelcomeScreen: Screen {
    typealias R = Reactions
    typealias A = Actions
    
    var viewController: WelcomeViewController
    weak var actionDelegate: WelcomeFlowController? = nil
    
    enum Actions: FunctionAction {
        case login
        case register
    }
    
    enum Reactions: Reaction {
        
    }
    
    init(viewController: WelcomeViewController) {
        self.viewController = viewController
        self.viewController.screen = self
    }
    
    func trigger(action: Actions) {
        actionDelegate?.onAction(action, for: self)
    }
    
    func trigger(reaction: Reactions) {
        viewController.onReaction(reaction)
    }
}

protocol WelcomeScreenActionDelegate: FlowActionDelegate {
    func onAction(_ action: WelcomeScreen.Actions, for: WelcomeScreen)
}

extension WelcomeScreenActionDelegate {
    func onAction(_ action: WelcomeScreen.Actions, for: WelcomeScreen) { }
}

class WelcomeViewController: ViewController<WelcomeScreen>, ScreenViewController {
    private let headerLabel = Label(style: .header)
    private let titleLabel = Label(style: .title)
    private let loginButton = Button(style: .block)
    private let registerButton = Button(style: .block)
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isHidden = true
        headerLabel.set(props: .text("SATURDATE"))
        view.addSubview(headerLabel)
        
        titleLabel.set(props: .text("Date with People\nNot with your phone"))
        view.addSubview(titleLabel)
        
        registerButton.set(props: .title("LET ME IN", for: .normal), .onTap({ self.screen?.trigger(screen: .register) }))
        view.addSubview(registerButton)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()

        headerLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(view.safeArea.top).inset(20)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(headerLabel.snp.bottom).offset(7)
        }
        
        registerButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.bottom.equalTo(view.safeArea.bottom).inset(25)
            make.height.equalTo(50)
        }
    }
    
    func onReaction(_ reaction: WelcomeScreen.Reactions) {
        
    }
}
