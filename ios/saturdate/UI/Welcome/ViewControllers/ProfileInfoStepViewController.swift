//
//  WelcomeViewController.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit

class ProfileInfoStepScreen: Screen {
    typealias R = Reactions
    typealias A = Actions
    
    var viewController: ProfileInfoStepViewController
    weak var actionDelegate: WelcomeFlowController? = nil
    var viewModel = ViewModel()
    
    enum Actions: FunctionAction {
        case `continue`
    }
    
    enum Reactions: Reaction {
        case allgood
        case problem
    }
    
    struct ViewModel {
        var name: String?
        var birthday: Date?
    }
    
    init(viewController: ProfileInfoStepViewController) {
        self.viewController = viewController
        self.viewController.screen = self
    }
    
    func trigger(action: Actions) {
        actionDelegate?.onAction(action, for: self)
    }
    
    func trigger(reaction: Reactions) {
        viewController.onReaction(reaction)
    }
}

protocol ProfileInfoStepScreenActionDelegate: FlowActionDelegate {
    func onAction(_ action: ProfileInfoStepScreen.Actions, for: ProfileInfoStepScreen)
}

extension ProfileInfoStepScreenActionDelegate {
    func onAction(_ action: ProfileInfoStepScreen.Actions, for: ProfileInfoStepScreen) { }
}

class ProfileInfoStepViewController: ViewController<ProfileInfoStepScreen>, ScreenViewController {
    private let headerLabel = Label(style: .header)
    private let titleLabel = Label(style: .title)
    private let nameView = TitledView(customView: TextField(style: .underlined))
    private let birthdateView = TitledView(customView: TextField(style: .underlined))
    private let continueButton = Button(style: .block)
    private let datePicker = UIDatePicker()
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isHidden = true
        
        headerLabel.set(props: .text("SATURDATE"))
        view.addSubview(headerLabel)
        
        titleLabel.set(props: .text("Your Profile"))
        view.addSubview(titleLabel)
        
        nameView.title = "NAME"
        view.addSubview(nameView)
        
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChange), for: .valueChanged)
        birthdateView.title = "BIRTHDAY"
        birthdateView.customView.inputView = datePicker
        birthdateView.customView.set(props: .additionalActions(enable: false))
        view.addSubview(birthdateView)
        
        let continueBlock = {
            guard let name = self.nameView.customView.text, name.count > 3 else {
                self.nameView.shake()
                return
            }
            
            guard let birthdate = self.birthdateView.customView.text, birthdate.count > 3 else {
                self.birthdateView.shake()
                return
            }
            
            self.screen?.viewModel.name = name
            self.screen?.viewModel.birthday = Date()
            self.screen?.trigger(action: .continue)
        }
        
        continueButton.set(props: .title("CONTINUE", for: .normal), .onTap(continueBlock))
        view.addSubview(continueButton)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        headerLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(view.safeArea.top).inset(20)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(headerLabel.snp.bottom).offset(7)
        }
        
        nameView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(60)
            make.left.right.equalToSuperview().inset(25)
            make.height.equalTo(64)
        }
        
        birthdateView.snp.makeConstraints { (make) in
            make.top.equalTo(nameView.snp.bottom).offset(20)
            make.left.right.equalToSuperview().inset(25)
            make.height.equalTo(64)
        }
        
        continueButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            bottomConstraint = make.bottom.equalTo(view.safeArea.bottom).inset(25).constraint
            make.height.equalTo(50)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeKeyboardNotifications()
    }
    
    @objc
    func dateChange() {
        debugPrint("sender: \(datePicker.date)")
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-YYYY"
        let dateString = formatter.string(from: datePicker.date)
        birthdateView.customView.set(props: .text(dateString))
        screen?.viewModel.birthday = datePicker.date
    }
    
    func onReaction(_ reaction: ProfileInfoStepScreen.Reactions) {
        switch reaction {
        case .allgood:
            self.screen?.trigger(screen: .profilePreferencesStep)
        case .problem:
            continueButton.shake()
        }
    }
}
