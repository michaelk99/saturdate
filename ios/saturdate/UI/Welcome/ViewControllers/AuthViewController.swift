//
//  AuthViewController.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import Foundation
import SnapKit
import UIKit

class AuthScreen: Screen {
    typealias R = Reactions
    typealias A = Actions
    
    var viewController: AuthViewController
    weak var actionDelegate: WelcomeFlowController? = nil
    var viewModel = ViewModel()
    
    enum Actions: FunctionAction {
        case login
        case register
    }
    
    enum Reactions: Reaction {
        case allGood
        case problem(_: AuthError)
    }
    
    struct ViewModel {
        var email: String?
        var password: String?
    }
    
    init(viewController: AuthViewController) {
        self.viewController = viewController
        self.viewController.screen = self
    }
    
    func trigger(action: Actions) {
        actionDelegate?.onAction(action, for: self)
    }
    
    func trigger(reaction: Reactions) {
        viewController.onReaction(reaction)
    }
}

protocol AuthScreenActionDelegate: FlowActionDelegate {
    func onAction(_ action: AuthScreen.Actions, for: AuthScreen)
}

class AuthViewController: ViewController<AuthScreen>, ScreenViewController {
    private let headerLabel = Label(style: .header)
    private let titleLabel = Label(style: .title)
    private let emailView = TitledView(customView: TextField(style: .underlined))
    private let passwordView = TitledView(customView: TextField(style: .password(secure: true)))
    private let infoLabel = Label(style: .error)
    
    private let haveAccountButton = Button(style: .plain)
    private let submitButton = Button(style: .block)
    
    private var mode: Mode {
        didSet {
            titleLabel.set(props: .text(mode.title))
            submitButton.set(props: .title(mode.ctaTitle, for: .normal))
            haveAccountButton.set(props: .title(mode.accountTitle, for: .normal))
        }
    }
    
    enum Mode {
    	case login
        case register
        
        var title: String {
            switch self {
            case .login:
                return "Log In"
            case .register:
                return "Create Account"
            }
        }
        
        var ctaTitle: String {
            switch self {
            case .login:
                return "Continue".uppercased()
            case .register:
                return "Join".uppercased()
            }
        }
        
        var accountTitle: String {
            switch self {
            case .login:
                return "Wanna join?"
            case .register:
                return "I already have an account"
            }
        }
    }
    
    init(mode: Mode) {
        self.mode = mode
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        addKeyboardNotifications()
                
        headerLabel.set(props: .text("SATURDATE"))
        view.addSubview(headerLabel)
        
        titleLabel.set(props: .text(mode.title))
        view.addSubview(titleLabel)
        
        emailView.title = "EMAIL"
        view.addSubview(emailView)
        
        passwordView.title = "PASSWORD"
        view.addSubview(passwordView)
        
        view.addSubview(infoLabel)
        
        let haveAccountBlock = { [unowned self] in
            self.mode = self.mode == .login ? .register : .login
        }
        
        haveAccountButton.set(props: .title(mode.accountTitle, for: .normal), .onTap(haveAccountBlock))
        view.addSubview(haveAccountButton)
        
        let actionBlock = { [unowned self] in
            self.screen?.viewModel.email = self.emailView.customView.text
            self.screen?.viewModel.password = self.passwordView.customView.text
            self.submitButton.startAnimating()
            self.infoLabel.set(props: .text(nil))
            self.screen?.trigger(action: self.mode == .login ? .login : .register)
        }
        
        submitButton.set(props: .title(mode.ctaTitle, for: .normal), .onTap(actionBlock))
        view.addSubview(submitButton)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if didSetupConstraints { return }
        
        headerLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.top.equalTo(view.safeArea.top).inset(20)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.top.equalTo(headerLabel.snp.bottom).offset(7)
        }
        
        emailView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(60)
            make.left.right.equalToSuperview().inset(25)
            make.height.equalTo(64)
        }
        
        passwordView.snp.makeConstraints { (make) in
            make.top.equalTo(emailView.snp.bottom).offset(20)
            make.left.right.equalToSuperview().inset(25)
            make.height.equalTo(64)
        }
        
        infoLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.top.equalTo(passwordView.snp.bottom).offset(20)
        }
        
        haveAccountButton.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(submitButton.snp.top).inset(-25)
        }
        
        submitButton.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.left.right.equalToSuperview().inset(25)
            bottomConstraint = make.bottom.equalTo(view.safeArea.bottom).inset(25).constraint
        }
        
        didSetupConstraints = true
    }
    
    func onReaction(_ reaction: AuthScreen.Reactions) {
        submitButton.stopAnimating()
        
        switch reaction {
        case .allGood:
            debugPrint("all good!")
            notificationHapticFeedback(type: .success)
            if mode == .register {
                self.screen?.trigger(screen: .profileInfoStep)
            }
        case .problem(let error):
            debugPrint("problem")
            infoLabel.set(props: .text(error.title))
            switch error {
            case .emailInUse:
                emailView.shake()
            case .invalidEmail:
                emailView.shake()
            case .invalidCredentials:
                emailView.shake()
                passwordView.shake()
            case .weakPassword:
                passwordView.shake()
            case .profileNotFound:
                // take user to profile screen
                break
            default:
                emailView.shake()
            }
            
            notificationHapticFeedback(type: .error)
        }
    }
}

class TitledView<T: UIView>: View {
    private let titleLabel = Label(style: .smallTitle)
    let customView: T
    
    var title: String? {
        didSet {
            titleLabel.set(props: .text(title))
        }
    }
    
    init(customView: T) {
        self.customView = customView
        super.init(frame: .zero)
        
        addSubview(titleLabel)
        addSubview(customView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(24)
        }
        
        customView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.left.bottom.right.equalToSuperview()
        }
    }
}
