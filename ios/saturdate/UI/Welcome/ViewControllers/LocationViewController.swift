//
//  LocationViewController.swift
//  saturdate
//
//  Created by Ali Ersoz on 3/16/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit

class LocationViewController: BaseViewController {
    private let headerLabel = Label(style: .header)
    private let titleLabel = Label(style: .title)
    private let secondaryTitleLabel = Label(style: .title)
    private let subtitleLabel = Label(style: .subtitle)
    private let continueButton = Button(style: .block)
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isHidden = true
        headerLabel.set(props: .text("SATURDATE"))
        view.addSubview(headerLabel)
        
        titleLabel.set(props: .text("Your Profile"))
        view.addSubview(titleLabel)
        
        secondaryTitleLabel.set(props: .text("Let us know\nWhere you are"))
        view.addSubview(secondaryTitleLabel)
        
        subtitleLabel.set(props: .text("We will use your location\nto match you with people around you"))
        view.addSubview(subtitleLabel)
        
        let continueBlock = { [unowned self] in
            LocationManager.shared.requestPersmission { [unowned self] permission in
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        continueButton.set(props: .title("CONTINUE", for: .normal), .onTap(continueBlock))
        view.addSubview(continueButton)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        headerLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(view.safeArea.top).inset(20)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(headerLabel.snp.bottom).offset(7)
        }
        
        secondaryTitleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(titleLabel.snp.bottom).offset(40)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(secondaryTitleLabel.snp.bottom).offset(10)
        }
        
        continueButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.bottom.equalTo(view.safeArea.bottom).inset(25)
            make.height.equalTo(50)
        }
    }
}
