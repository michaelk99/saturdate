//
//  ProfilePreferencesStepViewController.swift
//  saturdate
//
//  Created by Ali Ersoz on 3/16/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit

class ProfilePreferencesStepScreen: Screen {
    typealias R = Reactions
    typealias A = Actions
    
    var viewController: ProfilePreferencesStepViewController
    weak var actionDelegate: WelcomeFlowController? = nil
    var viewModel = ViewModel()
    
    enum Actions: FunctionAction {
        case `continue`
    }
    
    enum Reactions: Reaction {
        case allgood
        case problem
    }
    
    struct ViewModel {
        var gender: Gender?
        var lookingFor: Gender?
    }
    
    init(viewController: ProfilePreferencesStepViewController) {
        self.viewController = viewController
        self.viewController.screen = self
    }
    
    func trigger(action: Actions) {
        actionDelegate?.onAction(action, for: self)
    }
    
    func trigger(reaction: Reactions) {
        viewController.onReaction(reaction)
    }
}

protocol ProfilePreferencesStepScreenActionDelegate: FlowActionDelegate {
    func onAction(_ action: ProfilePreferencesStepScreen.Actions, for: ProfilePreferencesStepScreen)
}

extension ProfilePreferencesStepScreenActionDelegate {
    func onAction(_ action: ProfilePreferencesStepScreen.Actions, for: ProfilePreferencesStepScreen) { }
}

class ProfilePreferencesStepViewController: ViewController<ProfilePreferencesStepScreen>, ScreenViewController {
    private let headerLabel = Label(style: .header)
    private let titleLabel = Label(style: .title)
    private let genderView = TitledView(customView: GenderSelectionView())
    private let lookingForView = TitledView(customView: GenderSelectionView())
    private let continueButton = Button(style: .block)
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isHidden = true
        headerLabel.set(props: .text("SATURDATE"))
        view.addSubview(headerLabel)
        
        titleLabel.set(props: .text("Your Profile"))
        view.addSubview(titleLabel)
        
        genderView.title = "GENDER"
        view.addSubview(genderView)
        
        lookingForView.title = "LOOKING FOR"
        view.addSubview(lookingForView)
        
        let continueBlock = {
            guard let gender = self.genderView.customView.selectedGender else {
                self.genderView.shake()
                return
            }
            
            guard let lookingFor = self.lookingForView.customView.selectedGender else {
                self.lookingForView.shake()
                return
            }
            
            self.screen?.viewModel.gender = gender
            self.screen?.viewModel.lookingFor = lookingFor
            self.screen?.trigger(action: .continue)
        }
        
        continueButton.set(props: .title("CONTINUE", for: .normal), .onTap(continueBlock))
        view.addSubview(continueButton)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        headerLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(view.safeArea.top).inset(20)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(25)
            make.top.equalTo(headerLabel.snp.bottom).offset(7)
        }
        
        genderView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(60)
            make.left.right.equalToSuperview().inset(25)
            make.height.equalTo(64)
        }
        
        lookingForView.snp.makeConstraints { (make) in
            make.top.equalTo(genderView.snp.bottom).offset(20)
            make.left.right.equalToSuperview().inset(25)
            make.height.equalTo(64)
        }
        
        continueButton.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.bottom.equalTo(view.safeArea.bottom).inset(25)
            make.height.equalTo(50)
        }
    }
    
    func onReaction(_ reaction: ProfilePreferencesStepScreen.Reactions) {
        switch reaction {
        case .allgood:
            continueButton.stopAnimating()
        case .problem:
            continueButton.shake()
        }
    }
}

class GenderSelectionView: View {
    private let maleButton = Button(style: .selectable)
    private let femaleButton = Button(style: .selectable)
    
    var selectedGender: Gender? {
		get {
			return !maleButton.isSelected && !femaleButton.isSelected ? nil : (maleButton.isSelected ? .male : .female)
		}
		set(newValue) {
			femaleButton.isSelected = newValue == .some(.female)
			maleButton.isSelected = newValue == .some(.male)
		}
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        maleButton.set(props: .title("MALE", for: .normal), .selected(false))
        addSubview(maleButton)
        
        maleButton.addTapListener { [unowned self] (button) in
            self.maleButton.isSelected = !self.maleButton.isSelected
            self.femaleButton.isSelected = !self.maleButton.isSelected
        }
        
        femaleButton.set(props: .title("FEMALE", for: .normal), .selected(false))
        addSubview(femaleButton)
        
        femaleButton.addTapListener { [unowned self] (button) in
            self.femaleButton.isSelected = !self.femaleButton.isSelected
            self.maleButton.isSelected = !self.femaleButton.isSelected
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        if didSetupConstraints { return }
        
        femaleButton.snp.makeConstraints { (make) in
            make.height.equalTo(32)
            make.width.equalTo(100)
            make.left.top.bottom.equalToSuperview()
        }
        
        maleButton.snp.makeConstraints { (make) in
            make.height.equalTo(32)
            make.width.equalTo(100)
            make.top.bottom.equalToSuperview()
            make.left.equalTo(femaleButton.snp.right).offset(25)
        }
        
        didSetupConstraints = true
    }
}
