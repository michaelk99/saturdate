//
//  FlowController.swift
//  flow-playground
//
//  Created by Ali Ersoz on 11/19/18.
//  Copyright © 2018 Saturdate. All rights reserved.
//

import Foundation
import UIKit

class FlowController: FlowActionDelegate {
    var router = Router.instance
    var navigationStack: [ScreenResult] = []
    
    var currentScreen: ScreenResult? {
        return navigationStack.last
    }

    func onAction(_ action: FlowAction) {
        router.navigate(to: action)
    }
    
    func onAction(_ action: ScreenAction) {
        guard let screenResult = router.navigate(to: action) else { return }
        
        navigationStack.append(screenResult)
        onNavigate(to: screenResult)
    }

    func onAction(_ action: NavigationAction) {
        guard let currentScreen = currentScreen else { return }

        switch action {
        case .screenDismissal:
            popScreenForStack()
        default:
            debugPrint("navigation action - \(currentScreen): \(action) ")
        }
    }

    func onNavigate(to screen: ScreenResult) {
        
    }
    
    func onPop(screen: ScreenResult) {
        
    }

    func onStart(screen: ScreenResult) {
        navigationStack.append(screen)
        onNavigate(to: screen)
    }
    
    func dismiss() {
        router.dismissFlow()
    }
    
    func popScreen() {
        guard navigationStack.count > 0 else { return }
        
        router.popScreen()
        let screenResult = navigationStack.removeLast()
        onPop(screen: screenResult)
    }

    private func popScreenForStack() {
        guard navigationStack.count > 0 else { return }

        let screenResult = navigationStack.removeLast()
        onPop(screen: screenResult)
    }
}
