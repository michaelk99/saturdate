//
//  SettingsInteractor.swift
//  saturdate
//
//  Created by Ali Ersoz on 4/28/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import UIKit

class SettingsInteractor {
    private let api: APIClient
	private var images: [Image] = [] {
		didSet {
			onImagesUpdate?(images)
		}
	}
	
	var onImagesUpdate: (([Image]) -> Void)?
	var onProfileFetch: ((Profile) -> Void)?
    
    init(api: APIClient = APIClient.shared) {
        self.api = api
    }
    
    func logout() {
        Defaults[.token] = nil
        Defaults.synchronize()
    }
	
	func fetchProfile() {
		api.send(request: .profile)
			.success { [weak self] (profile: Profile) in
				self?.onProfileFetch?(profile)
			}
			.finalize { (success, _) in
				print("fetch profile: \(success)")
			}
	}
	
	func saveProfile(_ profile: Profile) {
		api.send(request: .updateProfile(profile))
			.success { (profile) in
				self.onProfileFetch?(profile)
			}
			.finalize { (success, _) in
				print("save profile: \(success)")
			}
	}
	
	func fetchImages() {
		api.send(request: .images)
			.success { (images: [Image]) in
				self.images = images
			}
			.finalize { success, error in
				print("images fetch: \(success)")
			}
	}
	
	func uploadImage(image: UIImage) {
		guard let file = File(image: image) else { return }
		
		api.upload(.uploadImage(file: file), completion: { (response: APIResponse<Image>) in
			response.success({ [weak self] (image) in
				print("uploaded image: \(image)")
				self?.images.insert(image, at: 0)
			})
			.finalize({ (success, _) in
				print("upload result: \(success)")
			})
		})
	}
	
	func deleteImage(image: Image) {
		guard let index = self.images.firstIndex(where: { $0.id == image.id }) else { return }
		api.send(request: .deleteImage(image: image))
			.finalize { [weak self] (success, _) in
				if success {
					self?.images.remove(at: index)
				}
			}
	}
	
	func setPrimaryPicture(image: Image) {
		guard let index = self.images.firstIndex(where: { $0.id == image.id }) else { return }
		api.send(request: .makePrimaryPicture(image: image))
			.finalize { [weak self] (success, _) in
				if success {
					self?.images.remove(at: index)
					self?.images.insert(image, at: 0)
				}
		}
	}
}
