//
//  PhotoGridView.swift
//  saturdate
//
//  Created by Ali Ersoz on 7/28/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

enum ImageType {
	case empty
	case image(_: Image, index: Int)
}

protocol PhotoGridViewDelegate: AnyObject {
	func photoGridView(_ view: PhotoGridView, didSelectImage: ImageType)
}

class PhotoGridView: CollectionView {
	private var columns: Int
	private var spacing: CGFloat
	private var sectionSpacing: CGFloat
	weak var imageDelegate: PhotoGridViewDelegate?
	
	var images: [Image] = []
	
	var itemWidth: CGFloat {
		let remainingWidth: CGFloat = UIScreen.main.bounds.width - CGFloat(columns - 1) * spacing - sectionSpacing * 2.0 - 1
		return remainingWidth / CGFloat(columns)
	}
	
	var height: CGFloat {
		let rowCount = ceil(CGFloat(numberOfItems(inSection: 0)) / CGFloat(columns))
		return rowCount * itemWidth + (rowCount - 1) * spacing + sectionSpacing * 2
	}
	
	init(columns: Int = 3, spacing: CGFloat = 10, sectionSpacing: CGFloat = 10) {
		self.columns = columns
		self.spacing = spacing
		self.sectionSpacing = sectionSpacing
		
		let layout = UICollectionViewFlowLayout()
		super.init(frame: .zero, collectionViewLayout: layout)

		layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
		layout.minimumInteritemSpacing = spacing
		layout.minimumLineSpacing = spacing
		layout.sectionInset = UIEdgeInsets(top: sectionSpacing, left: sectionSpacing, bottom: sectionSpacing, right: sectionSpacing)
		
		self.dataSource = self
		self.delegate = self
		backgroundColor = .white
		set(props: .registerCell(PhotoCell.classForCoder(), identifier: PhotoCell.identifier))
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension PhotoGridView: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 6
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = dequeueReusableCell(withReuseIdentifier: PhotoCell.identifier, for: indexPath) as! PhotoCell
		if images.count > indexPath.item {
			cell.urlString = images[indexPath.item].url
		} else {
			cell.backgroundColor = .yellow
		}
		return cell
	}
}

extension PhotoGridView: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if images.count > indexPath.item {
			imageDelegate?.photoGridView(self, didSelectImage: .image(images[indexPath.item], index: indexPath.item))
		}
		else {
			imageDelegate?.photoGridView(self, didSelectImage: .empty)
		}
	}
}

class PhotoCell: CollectionViewCell {
	static let identifier = "PhotoCell"
	
	private var imageView = ImageView()
	var urlString: String? {
		didSet {
			guard let urlString = urlString else { return }
			imageView.set(props: .url(urlString))
		}
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .blue
		contentView.addSubview(imageView)
		setNeedsUpdateConstraints()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func updateConstraints() {
		super.updateConstraints()
		
		imageView.snp.makeConstraints { (make) in
			make.edges.equalToSuperview()
		}
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		imageView.image = nil
	}
}
