//
//  SettingsViewController.swift
//  saturdate
//
//  Created by Ali Ersoz on 4/28/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsFlowControllerProtocol: AnyObject {
    func logout()
}

class SettingsViewController: ScrollableViewController {
    private unowned var flowController: SettingsFlowControllerProtocol
    private var interactor: SettingsInteractor
	private var profile: Profile?
	
	private let photoGridView = PhotoGridView()
	private let datePicker = UIDatePicker()
	private let nameView = TitledView(customView: TextField(style: .underlined))
	private let birthdateView = TitledView(customView: TextField(style: .underlined))
	private let bioView = TitledView(customView: TextView(style: .standard))
	private let genderView = TitledView(customView: GenderSelectionView())
	private let lookingForView = TitledView(customView: GenderSelectionView())

	private var saveButton = Button(style:.block)
    private var logoutButton = Button(style: .plain)
    
    init(flowController: SettingsFlowControllerProtocol, interactor: SettingsInteractor) {
        self.flowController = flowController
        self.interactor = interactor
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
		
		photoGridView.imageDelegate = self
		contentView.addSubview(photoGridView)
		
		nameView.title = "NAME"
		contentView.addSubview(nameView)
		
		datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
		datePicker.datePickerMode = .date
		datePicker.addTarget(self, action: #selector(dateChange), for: .valueChanged)
		birthdateView.title = "BIRTHDAY"
		birthdateView.customView.inputView = datePicker
		birthdateView.customView.set(props: .additionalActions(enable: false))
		birthdateView.isUserInteractionEnabled = false
		contentView.addSubview(birthdateView)
		
		bioView.title = "ABOUT YOU"
		bioView.customView.placeholder = "Tell us about yourself"
		contentView.addSubview(bioView)
		
		genderView.title = "GENDER"
		contentView.addSubview(genderView)
		
		lookingForView.title = "LOOKING FOR"
		contentView.addSubview(lookingForView)
		
		saveButton.set(props: .title("Save", for: .normal))
		contentView.addSubview(saveButton)
		
		saveButton.addTapListener { (_) in
			guard var profile = self.profile else { return }
			profile.bio = self.bioView.customView.text
			profile.gender = self.genderView.customView.selectedGender
			profile.name = self.nameView.customView.text
			profile.preferences?.gender = self.lookingForView.customView.selectedGender
			// profile.birthday = self.birthdateView.customView.text
			self.interactor.saveProfile(profile)
		}
		
        logoutButton.set(props: .title("Log out", for: .normal))
        contentView.addSubview(logoutButton)
		
        logoutButton.addTapListener { (button) in
            self.interactor.logout()
            self.flowController.logout()
        }
		
		interactor.onImagesUpdate = { [weak self] images in
			guard let self = self else { return }
			
			DispatchQueue.main.async {
				self.photoGridView.images = images
				self.photoGridView.reloadData()
			}
		}
		
		interactor.onProfileFetch = { [weak self] profile in
			guard let self = self else { return }
			
			DispatchQueue.main.async {
				self.profile = profile
				self.nameView.customView.text = profile.name
				self.bioView.customView.text = profile.bio
				self.genderView.customView.selectedGender = profile.gender
				self.lookingForView.customView.selectedGender = profile.preferences?.gender
				self.updateBirthDate(profile.birthday)
			}
		}
		
		interactor.fetchImages()
		interactor.fetchProfile()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        if didSetupConstraints { return }
		
		scrollView.snp.makeConstraints { make in
			make.left.right.equalToSuperview()
			make.bottom.equalToSuperview()
			make.top.equalToSuperview()
		}

		photoGridView.snp.makeConstraints { (make) in
			make.top.equalToSuperview()
			make.left.right.equalToSuperview()
			make.height.equalTo(photoGridView.height)
		}
		
		nameView.snp.makeConstraints { (make) in
			make.top.equalTo(photoGridView.snp.bottom).offset(25)
			make.left.right.equalToSuperview().inset(25)
			make.height.equalTo(64)
		}
		
		birthdateView.snp.makeConstraints { (make) in
			make.top.equalTo(nameView.snp.bottom).offset(25)
			make.left.right.equalToSuperview().inset(25)
			make.height.equalTo(64)
		}
		
		bioView.snp.makeConstraints { (make) in
			make.top.equalTo(birthdateView.snp.bottom).offset(25)
			make.left.right.equalToSuperview().inset(25)
			make.height.equalTo(88)
		}
		
		genderView.snp.makeConstraints { (make) in
			make.top.equalTo(bioView.snp.bottom).offset(25)
			make.left.right.equalToSuperview().inset(25)
			make.height.equalTo(64)
		}
		
		lookingForView.snp.makeConstraints { (make) in
			make.top.equalTo(genderView.snp.bottom).offset(25)
			make.left.right.equalToSuperview().inset(25)
			make.height.equalTo(64)
		}
		
		saveButton.snp.makeConstraints { (make) in
			make.top.equalTo(lookingForView.snp.bottom).offset(40)
			make.left.right.equalToSuperview().inset(25)
			make.height.equalTo(50)
		}
		
        logoutButton.snp.makeConstraints { (make) in
			make.top.equalTo(saveButton.snp.bottom).offset(40)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(25)
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
        
        didSetupConstraints = true
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		addKeyboardNotifications { (height) in
			self.scrollView.snp.updateConstraints({ (make) in
				make.bottom.equalToSuperview().offset(-height)
			})
		}
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		removeKeyboardNotifications()
	}
	
	func updateBirthDate(_ date: Date?) {
		guard let date = date else { return }
		
		let formatter = DateFormatter()
		formatter.dateFormat = "MMM dd, YYYY"
		let dateString = formatter.string(from: date)
		
		birthdateView.customView.set(props: .text(dateString))
	}
	
	@objc
	func dateChange() {
		debugPrint("sender: \(datePicker.date)")

		updateBirthDate(datePicker.date)
		// screen?.viewModel.birthday = datePicker.date
	}
}

extension SettingsViewController: PhotoGridViewDelegate {
	func photoGridView(_ view: PhotoGridView, didSelectImage imageType: ImageType) {
		let alertController = UIAlertController(title: "Image", message: "Select one of the options below", preferredStyle: .actionSheet)
		
		switch imageType {
		case .empty:
			alertController.addAction(createUploadImageAction())
			alertController.addAction(createCancelAction())
		case .image(let image, let index):
			alertController.addAction(createDeleteImageAction(for: image))
			alertController.addAction(createSetPrimaryImageAction(for: image))
			alertController.addAction(createCancelAction())
		}
		
		present(alertController, animated: true, completion: nil)
	}
	
	private func createUploadImageAction() -> UIAlertAction{
		return UIAlertAction(title: "Upload", style: .default) { (action) in
			// todo: show upload sheet
			print("todo: show upload sheet")
			self.presentImagePicker()
		}
	}
	
	private func createSetPrimaryImageAction(for image: Image) -> UIAlertAction {
		return UIAlertAction(title: "Make profile picture", style: .default) { (action) in
			// todo: make profile picture
			print("todo: make profile picture")
			self.interactor.setPrimaryPicture(image: image)
		}
	}
	
	private func createDeleteImageAction(for image: Image) -> UIAlertAction {
		return UIAlertAction(title: "Delete image", style: .destructive) { [unowned self] (action) in
			// todo: make profile picture
			print("todo: make profile picture")
			
			self.interactor.deleteImage(image: image)
		}
	}
	
	private func createCancelAction() -> UIAlertAction {
		return UIAlertAction(title: "Cancel", style: .cancel) { (action) in }
	}
}

extension SettingsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
		// Local variable inserted by Swift 4.2 migrator.
		let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
		
		let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
		// avatarButton.set(props: .image(image))
		picker.dismiss(animated: true) {
			guard let image = image else { return }
			self.interactor.uploadImage(image: image)
		}
	}
	
	// Helper function inserted by Swift 4.2 migrator.
	private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
		return Dictionary(uniqueKeysWithValues: input.map { key, value in (key.rawValue, value) })
	}
	
	// Helper function inserted by Swift 4.2 migrator.
	private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
		return input.rawValue
	}

}

