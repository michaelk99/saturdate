//
//  HomeInteractor.swift
//  saturdate
//
//  Created by Ali Ersoz on 4/7/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class HomeInteractor {
    private let api: APIClient
    
    private var location: (lat: Float, lon: Float)?
	
    public var onCandidatesUpdate: (() -> Void)?
    public var onLocationUpdate: (() -> Void)?
    public var onMatch: ((MatchViewModel) -> Void)?
    
    private var passes: [Profile] = []
    private var likes: [Profile] = []
    private var candidates: Set<Profile> = []
	
	private var isLoading = false {
		didSet {
			self.viewModel = HomeViewModel(candidates: Array(candidates))
			onCandidatesUpdate?()
		}
	}
    
    var viewModel: HomeViewModel
    
    init(api: APIClient = APIClient.shared) {
        self.api = api
        self.viewModel = HomeViewModel(candidates: [])
    }
    
    func fetchCandidates() {
        guard let location = location else { return }
        
        isLoading = true
        api.send(request: .candidates(latitude: location.lat, longitude: location.lon))
            .success { [weak self] (profiles: [Profile]) in
                guard let self = self else { return }
                self.candidates = self.candidates.union(Set(profiles))
            }
            .finalize { [weak self] _, _ in
                self?.isLoading = true
            }
    }
    
    func like(profileId: String) {
        guard let likedProfile = candidates.first(where: { $0.id == profileId }) else { return }
        
        candidates.remove(likedProfile)
        likes.append(likedProfile)
        
        api.send(request: .swipe(right: true, forProfile: profileId))
            .finalize { (success, _) in
                print("like success: \(success)")
            }
    }
    
    func pass(profileId: String) {
        guard let passedProfile = candidates.first(where: { $0.id == profileId }) else { return }
        
        candidates.remove(passedProfile)
        passes.append(passedProfile)
        
        api.send(request: .swipe(right: true, forProfile: profileId))
            .finalize { (success, _) in
                print("pass success: \(success)")
            }
    }
    
    func matches() {
        api.send(request: .matches)
            .success { (match: Match) in
                print("matches: \(match)")
            }
            .failure({ (error) in
                print("matches error: \(error)")
            })
            .finalize { (success, _) in
                print("match success: \(success)")
            }
    }
    
    func updateLocation(latitude: Float, longitude: Float) {
        location = (lat: latitude, lon: longitude)
        api.send(request: .location(latitude: latitude, longitude: longitude))
            .finalize { [weak self] (success, _) in
                debugPrint("update location sent: \(success)")
                self?.onLocationUpdate?()
                self?.fetchCandidates()
            }
    }
    
    func logout() {
        Defaults[.token] = nil
        Defaults.synchronize()
    }
}

struct HomeViewModel {
    var candidates: [ProfileViewModel] = []
    
    init(candidates: [Profile]) {
        self.candidates = candidates.map { (p: Profile) in
            return ProfileViewModel(name: p.name ?? "[name]", info: "\(p.birthday ?? Date())" + ", " + (p.location?.city ?? "[city]"))
        }
    }
}

struct MatchViewModel {
    var profile: ProfileViewModel
}

struct ProfileViewModel {
    var name: String
    var info: String
}

