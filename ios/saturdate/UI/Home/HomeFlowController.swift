//
//  HomeFlowController.swift
//  saturdate
//
//  Created by Ali Ersoz on 3/16/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation

protocol HomeFlowControllerProtocol: AnyObject {
    func navigateToLocation()
    func navigateToSettings()
    func logout()
}

class HomeFlowController: FlowController {
    
}

extension HomeFlowController: HomeFlowControllerProtocol {
    func navigateToLocation() {
        self.router.navigate(to: .location, style: .modal)
    }
    
    func navigateToSettings() {
        self.router.navigate(to: .settings(self), style: .push)
    }
}

extension HomeFlowController: SettingsFlowControllerProtocol {
    func logout() {
        self.router.navigate(to: .welcome)
    }
}
