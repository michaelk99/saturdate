//
//  HomeViewController.swift
//  saturdate
//
//  Created by Ali Ersoz on 3/16/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation
import UIKit
import Koloda

class HomeViewController: BaseViewController {
    private let headerLabel = Label(style: .header)
    private let titleLabel = Label(style: .title)
    private let locationButton = Button(style: .block)
    private let settingsButton = Button(style: .plain)
    private let spinnerView = UIActivityIndicatorView(style: .gray)
    private let kolodaView = KolodaView()
    private let yesButton = Button(style: .swipe)
    private let noButton = Button(style: .swipe)
    
    private unowned let flowController: HomeFlowControllerProtocol
    private let interactor: HomeInteractor
    
    init(flowController: HomeFlowControllerProtocol, interactor: HomeInteractor) {
        self.flowController = flowController
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        isNavigationBarHidden = true
        
        headerLabel.set(props: .text("HOME"))
        view.addSubview(headerLabel)
        
        titleLabel.set(props: .text("Welcome"))
        view.addSubview(titleLabel)
        
        view.addSubview(spinnerView)
        
        yesButton.set(viewProps: .alpha(0))
        yesButton.set(props: .image(UIImage(named: "heart")!))
        view.addSubview(yesButton)
        
        noButton.set(viewProps: .alpha(0))
        noButton.set(props: .image(UIImage(named: "cross")!))
        view.addSubview(noButton)
        
        kolodaView.dataSource = self
        kolodaView.delegate = self
        view.addSubview(kolodaView)
        
        locationButton.set(viewProps: .hidden(true))
        locationButton.set(props: .title("Location", for: .normal))
        view.addSubview(locationButton)
        
        settingsButton.set(props: .title("⚙️", for: .normal))
        view.addSubview(settingsButton)
        
        locationButton.addTapListener { [unowned self] (button) in
            self.flowController.navigateToLocation()
        }
        
        settingsButton.addTapListener { (button) in
            self.flowController.navigateToSettings()
        }
        
        interactor.onCandidatesUpdate = { [weak self] in
            guard let self = self else { return }
            print("viewModel: \(self.interactor.viewModel)")
            self.kolodaView.reloadData()
            
            self.spinnerView.stopAnimating()
            self.yesButton.transform = CGAffineTransform(translationX: 0, y: -160)
            self.noButton.transform = CGAffineTransform(translationX: 0, y: -160)
            UIView.animate(withDuration: 0.3, delay: 1.5, options: .curveEaseOut, animations: {
                self.yesButton.set(viewProps: .alpha(1))
                self.noButton.set(viewProps: .alpha(1))
                
                self.yesButton.transform = .identity
                self.noButton.transform = .identity
            })
        }
        
        yesButton.addTapListener { [unowned self] _ in
            guard !self.kolodaView.isRunOutOfCards else { return }
            self.kolodaView.swipe(.right)
            self.yesButton.set(props: .enabled(false))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: {
                self.yesButton.set(props: .enabled(true))
            })
        }
        
        noButton.addTapListener { [unowned self] _ in
            guard !self.kolodaView.isRunOutOfCards else { return }
            self.kolodaView.swipe(.left)
            self.noButton.set(props: .enabled(false))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.75, execute: {
                self.noButton.set(props: .enabled(true))
            })
        }
        
        spinnerView.startAnimating()
        interactor.fetchCandidates()
        interactor.matches()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        if didSetupConstraints { return }
        
        headerLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.top.equalTo(view.safeArea.top).inset(20)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(25)
            make.top.equalTo(titleLabel.snp.bottom).offset(7)
        }
        
        kolodaView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(headerLabel.snp.bottom).offset(40)
            make.height.equalTo(500)
        }
        
        spinnerView.snp.makeConstraints { (make) in
            make.center.equalTo(kolodaView)
            make.width.height.equalTo(44)
        }
        
        yesButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(100)
            make.top.equalTo(kolodaView.snp.bottom).offset(40)
            make.centerX.equalToSuperview().offset(80)
        }
        
        noButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(100)
            make.top.equalTo(kolodaView.snp.bottom).offset(40)
            make.centerX.equalToSuperview().offset(-80)
        }
        
        locationButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(40)
            make.centerX.equalToSuperview()
            make.width.equalTo(200)
            make.height.equalTo(50)
        }
        
        settingsButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(headerLabel)
            make.right.equalToSuperview().inset(20)
            make.width.height.equalTo(44)
        }
        
        didSetupConstraints = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        LocationManager.shared.requestLocation { (location) in
            self.interactor.updateLocation(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude))
        }
    }
}

extension HomeViewController: KolodaViewDataSource {
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return interactor.viewModel.candidates.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let cardView = CardView()
        cardView.viewModel = interactor.viewModel.candidates[index]
        return cardView
    }
}

class CardView: View {
    private let infoView = View()
    private let titleLabel = Label(style: .title)
    private let subtitleLabel = Label(style: .subtitle)
    private let imageView = ImageView(style: .standard)
    
    var viewModel: ProfileViewModel? {
        didSet {
            titleLabel.set(props: .text(viewModel?.name))
            subtitleLabel.set(props: .text(viewModel?.info))
            let number = arc4random() % 300
            imageView.set(props: .url("https://picsum.photos/id/\(number)/800"))
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        set(viewProps: .background(color: .white), .corner(radius: 20), .layerBorder(width: 1, color: .extraLightGray))
        
        imageView.set(props: .contentMode(.scaleAspectFill))
        addSubview(imageView)
        
        infoView.set(viewProps: .background(color: .white))
        addSubview(infoView)
        infoView.addSubview(titleLabel)
        infoView.addSubview(subtitleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        if didSetupConstraints { return }
        
        imageView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
        }
        
        infoView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(100)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview().inset(20)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
        }
        
        didSetupConstraints = true
    }
}

extension HomeViewController: KolodaViewDelegate {
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        // call swipe endpoint
        debugPrint("did swipe")
        UIView.animate(withDuration: 0.3) {
            self.noButton.transform = .identity
            self.yesButton.transform = .identity
        }
    }
    
    func koloda(_ koloda: KolodaView, draggedCardWithPercentage finishPercentage: CGFloat, in direction: SwipeResultDirection) {
        // animate buttons
        switch direction {
        case .left, .topLeft, .bottomLeft:
            debugPrint("swiping left: \(finishPercentage)")
            let scale: CGFloat = 1 + (finishPercentage / 100) * 0.3
            noButton.transform = CGAffineTransform(scaleX: scale, y: scale)
            yesButton.transform = .identity
        case .right, .topRight, .bottomRight:
            debugPrint("swiping right: \(finishPercentage)")
            let scale: CGFloat = 1 + (finishPercentage / 100) * 0.3
            yesButton.transform = CGAffineTransform(scaleX: scale, y: scale)
            noButton.transform = .identity
        default:
            return
        }
    }
    
    func kolodaDidResetCard(_ koloda: KolodaView) {
        debugPrint("did reset")
        UIView.animate(withDuration: 0.3) {
            self.noButton.transform = .identity
            self.yesButton.transform = .identity
        }
    }
}
