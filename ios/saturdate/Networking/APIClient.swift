//
//  APIClient.swift
//  Saturdate
//
//  Created by Ali Ersoz on 5/30/18.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

final class APIClient {
    static let shared = APIClient()
    
    static var debugDomain = ""
    let sessionManager: SessionManager
    let config = APIConfig.shared
    
    private init() {
        let configuration = URLSessionConfiguration.default
        let policies:[String: ServerTrustPolicy] = APIClient.debugDomain != "" ? [APIClient.debugDomain: .disableEvaluation] : [:]
        sessionManager = SessionManager(configuration: configuration, serverTrustPolicyManager: ServerTrustPolicyManager(policies: policies))
    }
    
    func send<T: Codable>(request endpoint: Endpoint) -> APIResponse<T> {
        return send(request: endpoint.request)
    }
    
    func send(request endpoint: Endpoint) -> APIResponse<Nothing> {
        return send(request: endpoint.request)
    }
    
    func upload<T: Codable>(_ endpoint: Endpoint, progress: Request.ProgressHandler? = nil, completion: @escaping (APIResponse<T>) -> ()) {
        let request = endpoint.request
        let response = APIResponse<T>()
        let url = config.baseURL(with: request.path, for: .image)
        
        sessionManager.upload(multipartFormData: { data in
//            for (name, param) in request.params {
//                data.append(param as! Data, withName: name)
//            }
			for case let (name, param as MultipartFormDataAppendable) in request.params {
				param.append(to: data, withName: name)
			}
        }, to: url, headers: config.headers) { (result) in
            switch result {
            case .success(let request, _, _):
                if let progress = progress {
                    request.uploadProgress(closure: progress)
                }
                
                completion(self.send(request: request))
            case .failure:
                completion(response)
            }
        }
    }

    private func send<T: Codable>(request apiRequest: APIRequest) -> APIResponse<T> {
        let request = sessionManager.request(apiRequest)
        let response: APIResponse<T> = send(request: request)
        
        return response
    }

    private func send<T: Codable>(request: DataRequest) -> APIResponse<T> {
        let response = APIResponse<T>()
        debugPrint("api call: \(request.request!.url!.absoluteString) with request: \(String(describing: request.request))")
        request.responseJSON { (res) in
            guard let statusCode = res.response?.statusCode else {
                response.onFailure?(APIError.noResponse)
                response.onFinalize?(false, 444)
                return
            }
            
            switch res.result {
            case .success:
                if statusCode >= 400 {
                    let decodedResponse: APIError = self.decodeError(response: res)

                    if decodedResponse == .invalidToken {
                        response.onTokenFailure?()
                        return
                    }
                    
                    response.onFailure?(decodedResponse)
                    response.onFinalize?(false, statusCode)

                    return
                }
                
                if let decodedResponse: T = self.decode(response: res) {
                    response.onSuccess?(decodedResponse)
                }
                
                response.onFinalize?(true, statusCode)
            case .failure:
                if statusCode >= 200 && statusCode < 400 {
                    response.onFinalize?(true, statusCode)
                    return
                }
                
                response.onFailure?(APIError.generic)
                response.onFinalize?(false, statusCode)
            }
        }
        
        return response
    }
    
    private func decode<T: Codable>(response: DataResponse<Any>) -> T? {
        let decoder = JSONDecoder()
        let data: T!
        do {
            data = try decoder.decode(T.self, from: response.data!)
        } catch {
            debugPrint("ex: \(error.localizedDescription)")
            return nil
        }
        
        return data
    }
    
    private func decodeError(response: DataResponse<Any>) -> APIError {
        let decoder = JSONDecoder()
        let decodedResponse: APIError = (try? decoder.decode(APIError.self, from: response.data!)) ?? .decoding
        return decodedResponse
    }
}

class APIRequest: URLRequestConvertible {
    fileprivate let method: HTTPMethod
    fileprivate let service: APIConfig.Service
    fileprivate let path: String
    fileprivate let params: [String: Any]
    fileprivate let config = APIConfig.shared
    
    init(service: APIConfig.Service, method: HTTPMethod, path: String, params: [String: Any] = [:]) {
        self.method = method
        self.path = path
        self.params = params
        self.service = service
    }
    
    func asURLRequest() throws -> URLRequest {
        return request(for: config.baseURL(with: path, for: service))
    }

    func request(for url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = config.headers

        do {
            let encoding: ParameterEncoding = method == .get ? URLEncoding.default : JSONEncoding.default
            if params.count > 0 {
                let encodedURLRequest = try encoding.encode(request, with: params)
                return encodedURLRequest
            }
            let encodedURLRequest = try encoding.encode(request, with: nil)
            return encodedURLRequest
        } catch let error {
            debugPrint("something went wrong while serializing the parameters: \(error)")
            return request
        }
    }
}

final class APIResponse<T: Codable> {
    fileprivate var onSuccess: SuccessBlock<T>?
    fileprivate var onFailure: FailureBlock?
    fileprivate var onTokenFailure: (EmptyBlock)?
    fileprivate var onFinalize: FinalizeBlock?
    
    @discardableResult
    func success(_ onSuccess: @escaping (T) -> ()) -> APIResponse {
        self.onSuccess = onSuccess
        return self
    }
    
    @discardableResult
    func failure(_ onFailure: @escaping (APIError) -> ()) -> APIResponse {
        self.onFailure = onFailure
        return self
    }
    
    @discardableResult
    func finalize(_ onFinalize: @escaping FinalizeBlock) -> APIResponse {
        self.onFinalize = onFinalize
        return self
    }

    @discardableResult
    fileprivate func tokenFailure(_ onTokenFailure: @escaping EmptyBlock) -> APIResponse {
        self.onTokenFailure = onTokenFailure
        return self
    }

    fileprivate func passthrough(_ response: APIResponse) {
        self.onSuccess = response.onSuccess
        self.onFailure = response.onFailure
        self.onFinalize = response.onFinalize
    }
}

typealias FinalizeBlock = (_ success: Bool, _ statusCode: Int) -> ()
typealias FailureBlock = ((_ error: APIError) -> ())
typealias SuccessBlock<T> = ((T) -> ())
typealias EmptyBlock = (() -> ())

final class APIConfig {
    static let shared = APIConfig() // todo: APIConfig singleton
    
    fileprivate var baseURL: String = "api.saturdate.co"
    
    func baseURL(with path: String, for service: Service) -> URL {
        return URL(string: "https://" + baseURL + "/api" + path)!
    }

    var headers: HTTPHeaders {
        return ["Authorization": "Bearer " + (Defaults[.token] ?? "")]
    }
    
    enum Service {
        case account
        case profile
        case image
        case geo
        case match
        
        var name: String {
            switch self {
            case .account:
                return "account"
            case .image:
                return "image"
            case .profile:
                return "profile"
            case .geo:
                return "geo"
            case . match:
                return "match"
            }
        }
    }
}
