//
//  Endpoints.swift
//  Saturdate
//
//  Created by Ali Ersoz on 5/31/18.
//

import Foundation
import UIKit
import Alamofire

enum Endpoint {
    case login(_ email: String, _ password: String)
    case register(_ email: String, _ password: String)
    case profile
	case updateProfile(_ profile: Profile)
    case createProfile(_ profile: Profile)
    case location(latitude: Float, longitude: Float)
    case candidates(latitude: Float, longitude: Float)
    case matches
    case swipe(right: Bool, forProfile: String)
	case images
	case uploadImage(file: File)
	case deleteImage(image: Image)
	case makePrimaryPicture(image: Image)
    
    var request: APIRequest {
        switch self {
        case .login(let email, let password):
            return APIRequest(service: .account, method: .post, path: "/v1/account/login/", params: ["email": email, "password": password])
        case .register(let email, let password):
            return APIRequest(service: .account, method: .post, path: "/v1/account/signup/", params: ["email": email, "password": password])
        case .profile:
            return APIRequest(service: .profile, method: .get, path: "/v1/profile/")
		case .updateProfile(let profile):
			return APIRequest(service: .profile, method: .put, path: "/v1/profile/", params: profile.dictionary ?? [:])
        case .createProfile(let profile):
            return APIRequest(service: .profile, method: .post, path: "/v1/profile/", params: profile.dictionary ?? [:])
        case .location(let latitude, let longitude):
            return APIRequest(service: .geo, method: .post, path: "/v1/geo/", params: ["latitude": latitude, "longitude": longitude])
        case .candidates(let latitude, let longitude):
            return APIRequest(service: .match, method: .get, path: "/v1/matches/candidates/", params: ["lat": latitude, "lon": longitude])
        case .matches:
            return APIRequest(service: .match, method: .get, path: "/v1/matches/")
        case .swipe(let right, let profileId):
            return APIRequest(service: .match, method: .post, path: "/v1/matches/swipe/", params: ["decision": right, "profile_id": profileId])
		case .images:
			return APIRequest(service: .image, method: .get, path: "/v1/images/")
		case .uploadImage(let file):
			// let filename = "file.jpg".data(using: .utf8)
			return APIRequest(service: .image, method: .post, path: "/v1/images/", params: ["file": file, "filename": file.name])
		case .deleteImage(let image):
			return APIRequest(service: .image, method: .delete, path: "/v1/images/\(image.id)/")
		case .makePrimaryPicture(let image):
			return APIRequest(service: .image, method: .patch, path: "/v1/images/\(image.id)/", params: ["id": image.id, "approval_score": image.approvalScore, "is_archived": image.archived])
        }
    }
}

struct File {
	let data: Data
	let name: String
	let format: Format
	
	var mimeType: String {
		switch format {
		case .jpg: return "image/jpeg"
		}
	}
	
	enum Format {
		case jpg
	}
}

extension File {
	init?(image: UIImage) {
		guard let jpeg = image.jpegData(compressionQuality: 0.1) else { return nil }
		
		self.init(data: jpeg, name: "file.jpg", format: .jpg)
	}
}

protocol MultipartFormDataAppendable {
	func append(to formData: MultipartFormData, withName name: String)
}

extension Data: MultipartFormDataAppendable {
	func append(to formData: MultipartFormData, withName name: String) {
		formData.append(self, withName: name)
	}
}

extension String: MultipartFormDataAppendable {
	func append(to formData: MultipartFormData, withName name: String) {
		guard let data = data(using: .utf8) else { return }
		
		formData.append(data, withName: name)
	}
}

extension File: MultipartFormDataAppendable {
	func append(to formData: MultipartFormData, withName name: String) {
		formData.append(data, withName: name, fileName: self.name, mimeType: mimeType)
	}
}
