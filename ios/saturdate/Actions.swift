//
//  Actions.swift
//  flow-playground
//
//  Created by Ali Ersoz on 11/19/18.
//  Copyright © 2018 Saturdate. All rights reserved.
//

import Foundation

enum ActionType {
    case flow
    case screen
    case navigation
    case function
}

protocol Action {
    var type: ActionType { get }
}

protocol FunctionAction: Action {
    
}

extension FunctionAction {
    var type: ActionType { return .function }
}

enum FlowAction: Action {
    case welcome
    case home
    
    var type: ActionType { return .flow }
    var presentationStyle: PresentationStyle {
        return .root
    }
    
    enum PresentationStyle {
        case push
        case modal
        case root
    }
}

protocol FlowActionDelegate: AnyObject {
    func onAction(_ action: FlowAction)
    func onAction(_ action: ScreenAction)
    func onAction(_ action: NavigationAction)
}

protocol Reaction {
    
}

enum ScreenAction: Action {
    case login
    case register
    case profileInfoStep
    case profilePreferencesStep
    case location
    case settings(_ flowController: SettingsFlowControllerProtocol)
    case home(_ flowController: HomeFlowControllerProtocol)
    
    var type: ActionType { return .screen }
}

enum NavigationAction: Action {
    case screenDismissal
    case screenWillAppear
    case screenWillDisappear
    case screenDidAppear
    case screenDidDisappear

    var type: ActionType { return .navigation }
}

