//
//  Router.swift
//  flow-playground
//
//  Created by Ali Ersoz on 11/19/18.
//  Copyright © 2018 Saturdate. All rights reserved.
//

import Foundation
import UIKit

class Router {
    static let instance = Router()
    var fcs = [FlowController]()
    var flows = [UIViewController]()
    
    var currentFlow: UIViewController? {
        return flows.last
    }
    
    var currentFlowController: FlowController? {
        return fcs.last
    }
    
    func navigate(to action: FlowAction) {
        let flow = try! createFlow(for: action)
        switch action.presentationStyle {
        case .root:
            flows.removeAll()
            fcs.removeFirst(fcs.count - 1)
            flows.append(flow)
            root(flow)
        default:
            return
        }

        print("fcs :\(fcs.count) -- flows: \(flows.count)")
    }
    
    @discardableResult
    func navigate(to action: ScreenAction, style: PresentationStyle = .push) -> ScreenResult? {
        guard let currentFlow = currentFlow as? UINavigationController else { return nil}
        
        let result = createScreen(for: action)
        switch style {
        case .modal:
            currentFlow.present(result.viewController, animated: true)
        default:
            currentFlow.pushViewController(result.viewController, animated: true)
        }
        
        return result
    }
    
    func dismissFlow() {
        let dismissingFlow = flows.dropLast().first
        flows.removeLast()
        fcs.removeLast()
        dismissingFlow?.dismiss(animated: true, completion: nil)
        
        print("fcs :\(fcs.count) -- flows: \(flows.count)")
    }
    
    func popScreen() {
        guard let nc = currentFlow as? UINavigationController else { return }
        nc.popViewController(animated: true)
    }

    func createFlow(for action: FlowAction) throws -> UIViewController {
        let flow = FlowControllerFactory.createFlow(for: action)
        guard let screen = flow.currentScreen else {
            throw RouterError.missingRootScreen
        }

        fcs.append(flow)
        let nc = UINavigationController(rootViewController: screen.viewController)
        return nc
    }
    
    private func root(_ viewController: UIViewController) {
        guard let window = AppDelegate.shared.window else { return }
        
        let snapshot = (window.snapshotView(afterScreenUpdates: false))
        let scale: CGFloat = 0.9
        window.backgroundColor = .white
        window.rootViewController = viewController
        window.rootViewController?.view.transform = CGAffineTransform(scaleX: scale, y: scale)
        window.rootViewController?.view.alpha = 0
        if snapshot != nil {
            window.addSubview(snapshot!)
        }
        
        UIView.transition(with: window, duration: 0.3, options: .curveEaseInOut, animations: {
            snapshot?.alpha = 0
            snapshot?.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, completion: { _ in
            snapshot?.removeFromSuperview()
            UIView.transition(with: window, duration: 0.3, options: .curveEaseInOut, animations: {
                window.rootViewController?.view.alpha = 1
                window.rootViewController?.view.transform = .identity
            })
        })
    }
}

func createScreen(for action: ScreenAction) -> ScreenResult {
    switch action {
    case .login:
        return .auth(AuthScreen(viewController: AuthViewController(mode: .login)))
    case .register:
        return .auth(AuthScreen(viewController: AuthViewController(mode: .register)))
    case .profileInfoStep:
        return .profileInfoStep(ProfileInfoStepScreen(viewController: ProfileInfoStepViewController()))
    case .profilePreferencesStep:
        return .profilePreferencesStep(ProfilePreferencesStepScreen(viewController: ProfilePreferencesStepViewController()))
    case .location:
        return .location(viewController: LocationViewController())
    case .home(let flowController):
        return .home(viewController: HomeViewController(flowController: flowController, interactor: HomeInteractor()))
    case .settings(let flowController):
        return .settings(viewController: SettingsViewController(flowController: flowController, interactor: SettingsInteractor()))
    }
}

enum ScreenResult {
    case welcome(_: WelcomeScreen)
    case auth(_: AuthScreen)
    case profileInfoStep(_: ProfileInfoStepScreen)
    case profilePreferencesStep(_: ProfilePreferencesStepScreen)
    case location(viewController: LocationViewController)
    case home(viewController: HomeViewController)
    case settings(viewController: SettingsViewController)
    
    var viewController: UIViewController {
        switch self {
        case .welcome(let screen):
            return screen.viewController
        case .auth(let screen):
            return screen.viewController
        case .profileInfoStep(let screen):
            return screen.viewController
        case .profilePreferencesStep(let screen):
            return screen.viewController
        case .location(let vc):
            return vc
        case .home(let vc):
            return vc
        case .settings(let vc):
            return vc
        }
    }
}

class FlowControllerFactory {
    class func createFlow(for action: FlowAction) -> FlowController {
        switch action {
        case .welcome:
            let screen = WelcomeScreen(viewController: WelcomeViewController())
            let wfc = WelcomeFlowController()
            wfc.onStart(screen: .welcome(screen))

            return wfc
        case .home:
            let hfc = HomeFlowController()
            hfc.onStart(screen: .home(viewController: HomeViewController(flowController: hfc, interactor: HomeInteractor())))
            
            return hfc
        }
    }
}

enum RouterError: Error {
    case missingRootScreen
    case generic( message: String)
}

enum PresentationStyle {
    case push
    case modal
}
