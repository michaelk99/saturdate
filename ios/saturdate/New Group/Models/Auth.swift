//
//  Auth.swift
//  saturdate
//
//  Created by Ali Ersoz on 3/2/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation

struct Account: Codable {
    var id: String
    var email: String
}

struct Auth: Codable {
    var token: String
}

struct Profile: Codable {
    var id: String?
    var accountId: String?
    var images: [String]?
    var name: String?
	var bio: String?
    var age: Int?
    var gender: Gender?
    var preferences: Preference?
    var location: Location?
    var birthday: Date? {
        get {
            guard let birthdayString = birthdayString else { return nil }
            
            let formatter = DateFormatter()
			formatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            let date = formatter.date(from: birthdayString)
            
            return date
        }
        set {
            guard let birthday = newValue else {
                birthdayString = nil
                return
            }
            
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            birthdayString = formatter.string(from: birthday)
        }
    }
    
    private var birthdayString: String?
    
    enum CodingKeys: String, CodingKey {
        case id, images, name, gender, preferences, location, bio
        case birthdayString = "birthday"
        case accountId = "account_id"
    }
}

extension Profile: Hashable {
    static func == (lhs: Profile, rhs: Profile) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id ?? "")
    }
    
    var hashValue: Int {
        return (self.id ?? "").hashValue
    }
}


struct Preference: Codable {
    var gender: Gender?
    var minAge: Int?
    var maxAge: Int?
    
    init(gender: Gender, minAge: Int, maxAge: Int) {
        self.gender = gender
        self.minAge = minAge
        self.maxAge = maxAge
    }
    
    enum CodingKeys: String, CodingKey {
        case minAge = "min_age"
        case maxAge = "max_age"
        case gender = "sex"
    }
}

struct Location: Codable {
    var city: String?
    var state: String?
    var zipCode: String?
    var latitude: Double?
    var longitude: Double?
    var neighborhood: String?
    
    enum CodingKeys: String, CodingKey {
        case zipCode = "zipcode"
        case city, state, latitude, longitude, neighborhood
    }
    
    init(city: String, state: String, zipCode: String, latitude: Double, longitude: Double, neighborhood: String) {
        self.city = city
        self.state = state
        self.zipCode = zipCode
        self.latitude = latitude
        self.longitude = longitude
        self.neighborhood = neighborhood
    }
}

enum Gender: Int, Codable {
    case male = 0
    case female = 1
    case unspecified = 2
}

/*
 "id": "87f56d0a-37c2-455d-a76c-5b40480d3d30",
 "account_id": "1a92dc62-e362-4ed4-af9f-2b49571ee279",
 "images": [
 "ef4d9bda-ea8e-45e5-b4dd-a0a57cdbf0e8 ",
 "a7a32b1d-b6e0-449a-843e-4e6aafcfec32",
 "11d4efa4-bc95-456b-9169-b50c98523cdc"
 ],
 "name": "Bob",
 "gender": 0,
 "age": 22,
 "preference_sex": 0,
 "preference_min_age": 20,
 "preference_max_age": 35,
 "city": "NYC",
 "state": "",
 "zipcode": "",
 "latitude": 0,
 "longitude": 0,
 "neighborhood": "",
 "created_at": "2019-02-23T16:27:54Z",
 "updated_at": "2019-02-24T15:10:21Z"
 */

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
