//
//  Image.swift
//  saturdate
//
//  Created by Ali Ersoz on 7/28/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation

struct Image: Codable {
	var id: String
	var url: String
	var approvalScore: Int
	var archived: Bool
	
	enum CodingKeys: String, CodingKey {
		case id, url
		case approvalScore = "approval_score"
		case archived = "is_archived"
	}
}
