//
//  Match.swift
//  saturdate
//
//  Created by Ali Ersoz on 4/28/19.
//  Copyright © 2019 Ali Ersöz. All rights reserved.
//

import Foundation

struct Match: Codable {
    var id: String
    var profiles: [String]
    var notified: [String]
}
