//
//  ViewController.swift
//  saturdate
//
//  Created by Ali Ersöz on 12/15/18.
//  Copyright © 2018 Ali Ersöz. All rights reserved.
//

import UIKit
import Foundation
import SnapKit

class BaseViewController: UIViewController {
    var didSetupConstraints = false
    var bottomConstraint: Constraint?
    var isNavigationBarHidden = false
    var onKeyboardChange: ((CGFloat) -> Void)?
    var customBottomInset: CGFloat?
    
    override func loadView() {
        super.loadView()
        
        view.set(viewProps: .background(color: .white))
        view.setNeedsUpdateConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(isNavigationBarHidden, animated: animated)
    }
    
    func addKeyboardNotifications(_ completion: ((CGFloat) -> Void)? = nil) {
        onKeyboardChange = completion
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisppear(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    func keyboardWillAppear(_ notification: Notification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue  else { return }
        guard let beginSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue, !keyboardSize.equalTo(beginSize) else {
            // no need to update if the keyboard size hasn't changed
            return
        }
        
        let duration = 0.25
        if #available(iOS 11.0, *) {
            debugPrint("keyboard height: \(keyboardSize.height)")
            let extraSpacing = customBottomInset ?? 0
            bottomConstraint?.update(offset: -keyboardSize.height + CGFloat(extraSpacing))
        } else {
            // Fallback on earlier versions
            bottomConstraint?.update(offset: -keyboardSize.height)
        }
        
        view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: TimeInterval(duration)) { [weak self] in
            self?.onKeyboardChange?(keyboardSize.height)
            self?.view.layoutIfNeeded()
        }
    }
    
    @objc
    func keyboardWillDisppear(_ notification: NSNotification) {
        debugPrint("keyboard will disappear")
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? CGFloat ?? 0.5
        
        bottomConstraint?.update(offset: 0)
        view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: TimeInterval(duration)) { [weak self] in
            self?.onKeyboardChange?(0)
            self?.view.layoutIfNeeded()
        }
    }
}

class ViewController<S: Screen>: BaseViewController {
    weak var screen: S?
    
    override func didMove(toParent viewController: UIViewController?) {
        if viewController == nil {
            screen?.trigger(navigation: .screenDismissal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        screen?.trigger(navigation: .screenWillAppear)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        screen?.trigger(navigation: .screenWillDisappear)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        screen?.trigger(navigation: .screenDidAppear)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        screen?.trigger(navigation: .screenDidDisappear)
    }
}

class ScrollableViewController: BaseViewController {
	let contentView = UIView()
	let scrollView = ScrollView()
	
	override func loadView() {
		super.loadView()
		
		view.addSubview(scrollView)
		contentView.set(viewProps: .clipsToBounds)
		scrollView.addSubview(contentView)
	}
	
	override func updateViewConstraints() {
		super.updateViewConstraints()
		if didSetupConstraints { return }
		
		contentView.snp.makeConstraints { (make) in
			make.top.bottom.centerX.equalToSuperview()
			make.width.equalToSuperview()
		}
	}
}

