## Changelog

All changes to `saturdate` will be documented in this file

## v1.0.1

### Changed
- Database moved to postgres

## v1.0.0

### Added
- Event service
- Match service

### Changed
- Scraped bars on yelp and added a `bars` table to the db
