package client

import (
	"encoding/json"
	"fmt"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"net/http"
	"net/url"
)

type MatchClient interface {
	GetMatch(jwt string, matchID string) (*models.Match, error)
}

// Client is an http matchClient
type matchClient struct {
	c        *http.Client
	matchURL *url.URL
}

// NewClient is a constructor for our matchClient
func NewMatchClient(matchURL *url.URL) MatchClient {
	c := &matchClient{
		c:        &http.Client{},
		matchURL: matchURL,
	}

	return c
}

// GetMatch get match by id
func (c *matchClient) GetMatch(jwt string, matchID string) (*models.Match, error) {
	u, err := url.Parse(fmt.Sprintf("%s/%s", c.matchURL.String(), matchID))
	if err != nil {
		fmt.Printf("Failed to parse url %s", err)
		return nil, err
	}

	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		fmt.Printf("Failed to create new request %s", err)
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", jwt))

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("GetMatch: Code: %d Status: %s", resp.StatusCode, resp.Status)
	}

	var match *models.Match
	err = json.NewDecoder(resp.Body).Decode(&match)
	if err != nil {
		return nil, fmt.Errorf("GetMatch: failed to decode resp body: %s", resp.Body)
	}

	return match, nil
}
