package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"net/http"
	"net/url"
)

type EventClient interface {
	CreateEvent(jwt string, e *models.Event) (*models.Event, error)
}

// Client is an http eventClient
type eventClient struct {
	c        *http.Client
	eventURL *url.URL
}

// NewClient is a constructor for our eventClient
func NewEventClient(eventURL *url.URL) EventClient {
	c := &eventClient{
		c:        &http.Client{},
		eventURL: eventURL,
	}

	return c
}

// CreateEvent create event
func (c *eventClient) CreateEvent(jwt string, e *models.Event) (*models.Event, error) {
	u, err := url.Parse(c.eventURL.String())
	if err != nil {
		fmt.Printf("Failed to parse url %s", err)
		return nil, err
	}

	b, err := json.Marshal(e)
	if err != nil {
		return nil, fmt.Errorf("CreateEvent: failed to marshal event: %s", err)
	}

	req, err := http.NewRequest("POST", u.String(), bytes.NewBuffer(b))
	if err != nil {
		return nil, fmt.Errorf("CreateEvent: failed to create request: %s", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", jwt))

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("CreateEvent: Code: %d Status: %s", resp.StatusCode, resp.Status)
	}

	var ee *models.Event
	err = json.NewDecoder(resp.Body).Decode(&ee)
	if err != nil {
		return nil, fmt.Errorf("CreateEvent: failed to decode resp body: %s", resp.Body)
	}

	return ee, nil
}
