package models

import (
	"github.com/lib/pq"
)

// Match is a retrieved match
type Match struct {
	ID        string         `json:"id" db:"id"`
	Profiles  pq.StringArray `validate:"required" json:"profiles" db:"profiles"`
	Notified  pq.StringArray `json:"notified" db:"notified"`
	CreatedAt string         `json:"created_at" db:"created_at"`
	UpdatedAt string         `json:"updated_at" db:"updated_at"`
}

// Blacklist is a retrieved blacklist
type Blacklist struct {
	ID                   string `json:"id" db:"id"`
	ProfileID            string `json:"profile_id" db:"profile_id"`
	ProfileIDBlacklisted string `json:"profile_id_blacklisted" db:"profile_id_blacklisted"`
	ExpiresAt            string `json:"expires_at" db:"expires_at"`
	CreatedAt            string `json:"created_at" db:"created_at"`
	UpdatedAt            string `json:"updated_at" db:"updated_at"`
}

// MissedMatch is a retrieved missed match
type MissedMatch struct {
	ID        string         `json:"id" db:"id"`
	Profiles  pq.StringArray `json:"profiles" db:"profiles"`
	CreatedAt string         `json:"created_at" db:"created_at"`
	UpdatedAt string         `json:"updated_at" db:"updated_at"`
}
