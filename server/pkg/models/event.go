package models

import (
	"encoding/json"
	"fmt"
	"github.com/jmoiron/sqlx/types"
	"github.com/lib/pq"
	"gopkg.in/guregu/null.v3"
	"strconv"
)

// State enum
type State int

const (
	// ProfileFirst state of the first profile
	ProfileFirst State = iota
	// ProfileSecond state of the second profile
	ProfileSecond
	// Complete state
	Complete
)

// MarshalJSON implements json.Marshaler.
func (st State) MarshalJSON() ([]byte, error) {
	switch st {
	case ProfileFirst:
		return []byte(strconv.FormatInt(0, 10)), nil
	case ProfileSecond:
		return []byte(strconv.FormatInt(1, 10)), nil
	case Complete:
		return []byte(strconv.FormatInt(2, 10)), nil
	}

	return nil, fmt.Errorf("Error marshaling state")
}

// UnmarshalJSON implements json.Unmarshaler.
func (st *State) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}

	i, ok := v.(float64)

	if !ok {
		return fmt.Errorf("State must be float64: %T %+v", v, v)
	}

	switch int(i) {
	case 0:
		*st = ProfileFirst
		return nil
	case 1:
		*st = ProfileSecond
		return nil
	case 2:
		*st = Complete
		return nil
	default:
		return fmt.Errorf("State invalid: %f", i)
	}
}

// Event is a retrieved event
type Event struct {
	ID        string             `json:"id" db:"id"`
	MatchID   string             `validate:"required" json:"match_id" db:"match_id"`
	Profiles  pq.StringArray     `json:"profiles" db:"profiles"`
	Notified  pq.StringArray     `json:"notified" db:"notified"`
	Turn      null.String        `json:"turn" db:"turn"`
	Bars      types.NullJSONText `json:"bars" db:"bars"`
	State     State              `json:"state" db:"state"`
	StartDate string             `json:"start_date" db:"start_date"`
	StartTime string             `json:"start_time" db:"start_time"`
	CreatedAt string             `json:"created_at" db:"created_at"`
	UpdatedAt string             `json:"updated_at" db:"updated_at"`
}

// Bar is a retrieved bar
type Bar struct {
	ID               string      `json:"id" db:"id"`
	BarName          null.String `json:"bar_name" db:"bar_name"`
	BarCategory      null.String `json:"bar_category" db:"bar_category"`
	BarPriceRange    null.String `json:"bar_price_range" db:"bar_price_range"`
	BarNumberReviews null.String `json:"bar_number_reviews" db:"bar_number_reviews"`
	BarNumberStars   null.String `json:"bar_number_stars" db:"bar_number_stars"`
	BarHood          null.String `json:"bar_hood" db:"bar_hood"`
	BarPhone         null.String `json:"bar_phone" db:"bar_phone"`
	BarReservations  null.String `json:"bar_reservations" db:"bar_reservations"`
	BarCreditCard    null.String `json:"bar_credit_card" db:"bar_credit_card"`
	BarParking       null.String `json:"bar_parking" db:"bar_parking"`
	BarWeelChair     null.String `json:"bar_weel_chair" db:"bar_weel_chair"`
	BarAttire        null.String `json:"bar_attire" db:"bar_attire"`
	BarDancing       null.String `json:"bar_dancing" db:"bar_dancing"`
	BarHappy         null.String `json:"bar_happy" db:"bar_happy"`
	BarOutdoor       null.String `json:"bar_outdoor" db:"bar_outdoor"`
	BarTV            null.String `json:"bar_tv" db:"bar_tv"`
	BarDogs          null.String `json:"bar_dogs" db:"bar_dogs"`
	BarPoolTable     null.String `json:"bar_pool_table" db:"bar_pool_table"`
	// not stored in DB, only used for FetchBars query
	BarDistance    float64 `json:"bar_distance" db:"bar_distance"`
	Latitude       float64 `json:"latitude" db:"latitude"`
	Longitude      float64 `json:"longitude" db:"longitude"`
	ImageURL       string  `json:"image_url" db:"image_url"`
	YelpURL        string  `json:"yelp_url" db:"yelp_url"`
	Address        string  `json:"address" db:"address"`
	AddressDisplay string  `json:"address_display" db:"address_display"`
}
