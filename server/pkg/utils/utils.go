package utils

import (
	"github.com/jinzhu/now"
	"time"
)

// GetDateTime wrapper around now.MustParse
func GetDateTime(dateTime string) time.Time {
	layout := "2006-01-02T15:04:05Z"
	t, err := time.Parse(layout, dateTime)

	if err == nil {
		return t
	}

	return now.MustParse(dateTime)
}
