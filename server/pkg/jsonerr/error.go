package jsonerr

import (
	"encoding/json"
	"fmt"
	"github.com/getsentry/sentry-go"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"os"
)

// Additional any additional info
type Additional interface{}

// Response json response
type Response struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	// Additional must be json serializable or expect errors
	Additional `json:"additional,omitempty"`
}

// initialization
func init() {
	// Use sentry for prod only [for now]
	if os.Getenv("ENVIRONMENT") != "prod" {
		return
	}

	if os.Getenv("SENTRY_DSN") != "" {
		err := sentry.Init(sentry.ClientOptions{
			AttachStacktrace: true,
			Release:          os.Getenv("RELEASE_ID"),
			Environment:      os.Getenv("ENVIRONMENT"),
			Dsn:              os.Getenv("SENTRY_DSN"),
		})
		if err != nil {
			fmt.Printf("Sentry initialization failed: %v\n", err)
		}
	}
}

// Error JsonError works like http.Error but uses our response
// struct as the body of the response. Like http.Error
// you will still need to call a naked return in the http handler
func Error(req *http.Request, w http.ResponseWriter, r *Response, httpcode int) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Content-Type-Options", "nosniff")

	requestDump, err := httputil.DumpRequestOut(req, true)

	if err != nil {
		requestDump = []byte{}
	}

	var body []byte

	switch req.Method {
	case http.MethodPost, http.MethodPut, http.MethodPatch:
		// track the body from the request if it exists
		body, err = ioutil.ReadAll(req.Body)
		if err != nil {
			body = []byte{}
		}
	}

	// tag report with user's `accountID` if it exists in the session
	session := req.Context().Value("Session")

	sentry.ConfigureScope(func(scope *sentry.Scope) {
		scope.SetLevel(sentry.LevelError)
		scope.SetTag("code", r.Code)
		scope.SetTag("url", req.URL.String())
		scope.SetTag("method", req.Method)

		if session != nil {
			sess := session.(*token.Session)
			accountID := sess.AccountID
			scope.SetUser(sentry.User{ID: sess.AccountID})
			scope.SetTag("accountID", accountID)
		}

		scope.SetExtra("request_dump", string(requestDump))
		if len(body) > 0 {
			scope.SetExtra("request_body", string(body))
		}
	})

	sentry.CaptureMessage(r.Message)
	w.WriteHeader(httpcode)
	b, _ := json.Marshal(r)

	w.Write(b)
}
