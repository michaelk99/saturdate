package middleware

import (
	"context"
	"log"
	"net/http"
	"strings"

	"gitlab.com/michaelk99/saturdate/server/pkg/token"
)

const (
	Session = "Session"
	Token   = "Token"
)

// Auth authenticates the incoming JWT and calls the next middleware in the
// chain
func Auth(ts token.Validator, next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get the jwt token from the request
		bearer := r.Header.Get("Authorization")
		// no authorization header sent
		if bearer == "" {
			log.Printf("Authorization is required")
			http.Error(w, "no authorization", http.StatusUnauthorized)
			return
		}
		splitToken := strings.Split(bearer, "Bearer ")
		jwt := splitToken[1]

		// confirm the token is sent in the request
		if len(jwt) == 0 {
			log.Printf("JWT not found")
			http.Error(w, "token not found", http.StatusUnauthorized)
			return
		}

		// validate the token and get the session
		var session *token.Session
		session, err := ts.Validate(jwt)

		if err != nil {
			http.Error(w, "invalid token", http.StatusUnauthorized)
			return
		}

		// store the session in the config
		ctx := context.WithValue(r.Context(), Session, session)
		// store the jwt in the context for service to service communication
		ctx = context.WithValue(ctx, Token, jwt)

		// call next to continue processing the request
		next(w, r.WithContext(ctx))
	}
}
