# Saturdate-Go
This repo holds microservices making up the Saturdate application.

## Local
- `brew install go`
- `brew install postgres`
- run `bootstrap.sql` against local `saturdate` db
- run each microservice --> `cd cmd/services/[*]; go run main.go`

### Insomnia API testing
- [Download insomnia](https://insomnia.rest/download/)
- Import the saturdate workspace: `insomnia/saturdate.insomnia.json`

# Prod

### Deployments
- See [CONTRIBUTING.md](https://gitlab.com/michaelk99/saturdate/blob/master/CONTRIBUTING.md)
to learn how prod deployments work
