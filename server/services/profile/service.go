package profile

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

// Service is a public interface for implementing our Profile service
type Service interface {
	// Create creates a profile
	Create(prof Profile) (*Profile, error)
	// Fetch retrieves a profile from the db
	Fetch(query IDQuery) (*Profile, error)
	// Search search other profiles from the db
	Search(whereCondition string) ([]*Profile, error)
	// Update updates a profile in the db
	Update(prof Profile) (*Profile, error)
	// Delete deletes a profile from the db
	Delete(query IDQuery) error
}

// service is a private implementation of our profile service
type service struct {
	ds ProfileStore
}

// NewService is a constructor for our Profile service implementation
func NewService(ds ProfileStore) Service {
	return &service{
		ds: ds,
	}
}

func (s *service) Create(prof Profile) (*Profile, error) {
	// fetch by account ID, see if profile already exists
	// 1-1 association between profile and account
	p, _ := s.ds.FetchByAccountID(prof.AccountID)
	if p != nil {
		return nil, ErrProfileExists{}
	}

	ts := time.Now().Format(time.RFC3339)
	prof.CreatedAt = ts
	prof.UpdatedAt = ts
	prof.ID = uuid.New().String()

	_, err := s.ds.Create(&prof)
	if err != nil {
		return nil, ErrProfileCreate{}
	}

	return &prof, nil
}

func (s *service) Fetch(query IDQuery) (*Profile, error) {
	var prof *Profile
	var err error

	switch query.Type {
	case ID:
		prof, err = s.ds.Fetch(query.Value)
		if err != nil {
			return nil, ErrProfileNotFound{}
		}
	case AccountID:
		prof, err = s.ds.FetchByAccountID(query.Value)
		if err != nil {
			return nil, ErrProfileNotFound{}
		}
	default:
		return nil, fmt.Errorf("Invalid query type")
	}

	return prof, nil
}

func (s *service) Search(whereCondition string) ([]*Profile, error) {
	profs, err := s.ds.Search(whereCondition)
	if err != nil {
		return nil, err
	}

	// TODO would be great to consolidate and remove the reliance on  :/
	var p []*Profile
	for _, prof := range profs {
		p = append(p, prof)
	}

	return p, nil
}

func (s *service) Update(prof Profile) (*Profile, error) {
	ts := time.Now().Format(time.RFC3339)
	prof.UpdatedAt = ts

	_, err := s.ds.Update(&prof)
	if err != nil {
		return nil, err
	}

	return &prof, nil
}

func (s *service) Delete(query IDQuery) error {
	switch query.Type {
	case ID:
		err := s.ds.Delete(query.Value)
		if err != nil {
			return err
		}
	case AccountID:
		err := s.ds.DeleteByAccountID(query.Value)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Invalid query type")
	}

	return nil
}
