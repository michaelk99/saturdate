// +build integration

package postgres_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"gitlab.com/michaelk99/saturdate/server/services/profile/postgres"
)

const (
	DeleteProfileQuery = `DELETE FROM profile;`
	CreateProfileQuery = `INSERT INTO profile (id, account_id, images, name, gender, birthday, preference_sex, preference_min_age, preference_max_age, city, state, zipcode, latitude, longitude, neighborhood, created_at, updated_at) VALUES (:id, :account_id, :images, :name, :gender, :birthday, :preference_sex, :preference_min_age, :preference_max_age, :city, :state, :zipcode, :latitude, :longitude, :neighborhood, :created_at, :updated_at);`
	FetchProfileQuery  = `SELECT * FROM profile WHERE id = $1;`
)

func Setup(t *testing.T) (*sqlx.DB, profile.ProfileStore, func(), func()) {
	connString := "host=localhost user=postgres dbname=saturdate password='' sslmode=disable"
	if os.Getenv("POSTGRES_CONN_STRING") != "" {
		connString = os.Getenv("POSTGRES_CONN_STRING")
	}
	db, err := sqlx.Connect("postgres", connString)
	if err != nil {
		t.Fatalf("setup: could not open connection to db: %s", err)
	}

	ds := postgres.NewProfileStore(db)

	return db, ds, func() {
			defer db.Close()
			_, err := db.Exec(DeleteProfileQuery)
			if err != nil {
				t.Fatalf("failed to delete profiles: %v. manual cleanup is necessary", err)
			}
		},
		func() {
			_, err := db.Exec(DeleteProfileQuery)
			if err != nil {
				t.Fatalf("failed to delete profiles: %v. manual cleanup is necessary", err)
			}
		}
}

var ProfileTestingTable = []*profile.Profile{
	&profile.Profile{
		ID:        uuid.New().String(),
		AccountID: uuid.New().String(),
		Name:      "Name1",
		Gender:    profile.Male,
		Images:    pq.StringArray{},
		Birthday:  time.Now().Format(time.RFC3339),
		Preferences: profile.Preferences{
			PreferenceSex:    profile.PreferenceMale,
			PreferenceMinAge: 0,
			PreferenceMaxAge: 10,
		},
		Location: profile.Location{
			City:         "city",
			State:        "state",
			Zipcode:      "zip",
			Latitude:     10.50,
			Longitude:    11.50,
			Neighborhood: "neighborhood1",
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
	&profile.Profile{
		ID:        uuid.New().String(),
		AccountID: uuid.New().String(),
		Name:      "Name2",
		Images:    pq.StringArray{},
		Birthday:  time.Now().Format(time.RFC3339),
		Gender:    profile.Female,
		Preferences: profile.Preferences{
			PreferenceSex:    profile.PreferenceFemale,
			PreferenceMinAge: 1,
			PreferenceMaxAge: 100,
		},
		Location: profile.Location{
			City:         "city",
			State:        "state",
			Zipcode:      "zip",
			Latitude:     11.1050,
			Longitude:    12222.5092,
			Neighborhood: "neighborhood1",
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
}

// Test the creation of an profile struct into postgres DB
func TestCreateProfile(t *testing.T) {
	_, ds, teardown, _ := Setup(t)
	defer teardown()

	for _, p := range ProfileTestingTable {
		_, err := ds.Create(p)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}
	}
}

// Test the update of a profile struct into postgres DB
func TestUpdateProfile(t *testing.T) {
	db, ds, teardown, clearDB := Setup(t)
	defer teardown()

	for _, p := range ProfileTestingTable {
		_, err := db.NamedExec(CreateProfileQuery, p)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}

		p.Name = "name-change"
		_, err = ds.Update(p)
		if err != nil {
			t.Fatalf("failed to update profile: %v", err)
		}

		// perform fetch and assert
		var pp profile.Profile
		err = db.Get(&pp, FetchProfileQuery, p.ID)
		if err != nil {
			t.Fatalf("failed to fetch updated profile: %v", err)
		}

		assert.Equal(t, "name-change", pp.Name)

		clearDB()
	}
}

// Test the delete of a profile struct into postgres DB
func TestDeleteProfile(t *testing.T) {
	db, ds, teardown, clearDB := Setup(t)
	defer teardown()

	for _, p := range ProfileTestingTable {
		_, err := db.NamedExec(CreateProfileQuery, p)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}

		err = ds.Delete(p.ID)
		if err != nil {
			t.Fatalf("failed to delete profile: %v", err)
		}
		clearDB()
	}

	rows, err := db.Queryx("SELECT * FROM profile")
	if err != nil {
		t.Fatalf("failed to query db: %s", err)
	}

	assert.Equal(t, false, rows.Next())
}

func TestDeleteProfileByAccountID(t *testing.T) {
	db, ds, teardown, clearDB := Setup(t)
	defer teardown()

	for _, p := range ProfileTestingTable {
		_, err := db.NamedExec(CreateProfileQuery, p)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}

		err = ds.DeleteByAccountID(p.AccountID)
		if err != nil {
			t.Fatalf("failed to delete profile: %v", err)
		}
		clearDB()
	}

	rows, err := db.Queryx("SELECT * FROM profile")
	if err != nil {
		t.Fatalf("failed to query db: %s", err)
	}

	assert.Equal(t, false, rows.Next())
}

// Test the fetch of a profile struct by email from postgres DB
func TestFetchProfile(t *testing.T) {
	db, ds, teardown, clearDB := Setup(t)
	defer teardown()

	for _, p := range ProfileTestingTable {
		_, err := db.NamedExec(CreateProfileQuery, p)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}

		pp, err := ds.Fetch(p.ID)

		if err != nil {
			t.Fatalf("failed to get profile from DB: %v", err)
		}

		assert.Equal(t, p.ID, pp.ID)
		clearDB()
	}
}

// Test the fetch of a profile struct by email from postgres DB
func TestFetchProfileByAccountID(t *testing.T) {
	db, ds, teardown, clearDB := Setup(t)
	defer teardown()

	for _, p := range ProfileTestingTable {
		_, err := db.NamedExec(CreateProfileQuery, p)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}

		pp, err := ds.FetchByAccountID(p.AccountID)

		if err != nil {
			t.Fatalf("failed to get profile from DB: %v", err)
		}

		assert.Equal(t, p.ID, pp.ID)
		clearDB()
	}
}
