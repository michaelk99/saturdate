package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
)

// queries are written to use sqlx.NamedExec() method. this method maps "db" struct tags with
// the : prefixed names in the values parameter
const (
	CreateProfileQuery            = `INSERT INTO profile (id, account_id, images, name, gender, birthday, preference_sex, preference_min_age, preference_max_age, city, state, zipcode, bio, job, university, show_percentage, is_verified, latitude, longitude, neighborhood, created_at, updated_at) VALUES (:id, :account_id, :images, :name, :gender, :birthday, :preference_sex, :preference_min_age, :preference_max_age, :city, :state, :zipcode, :bio, :job, :university, :show_percentage, :is_verified, :latitude, :longitude, :neighborhood, :created_at, :updated_at);`
	UpdateProfileQuery            = `UPDATE profile SET images = :images, name = :name, gender = :gender, birthday = :birthday, preference_sex = :preference_sex, preference_min_age = :preference_min_age, preference_max_age = :preference_max_age, city = :city, state = :state, zipcode = :zipcode, bio = :bio, job = :job, university = :university, show_percentage = :show_percentage, is_verified = :is_verified, latitude = :latitude, longitude = :longitude, neighborhood = :neighborhood, updated_at = :updated_at WHERE id = :id;`
	FetchProfileQuery             = `SELECT * FROM profile WHERE id = $1;`
	FetchProfileByAccountIDQuery  = `SELECT * FROM profile WHERE account_id = $1`
	SearchProfilesQuery           = `SELECT * FROM profile`
	DeleteProfileQuery            = `DELETE FROM profile WHERE id = $1;`
	DeleteProfileByAccountIDQuery = `DELETE FROM profile WHERE account_id = $1`
)

// profileStore is a private implementation of the profile.ProfileStore interface
type profileStore struct {
	// a sqlx database object
	db *sqlx.DB
}

// NewProfileStore returns a postgres db implementation of the profile.ProfileStore interface
func NewProfileStore(db *sqlx.DB) profile.ProfileStore {
	return &profileStore{
		db: db,
	}
}

// Create creates a profile in a postgres db
func (s *profileStore) Create(p *profile.Profile) (*profile.Profile, error) {
	row, err := s.db.NamedExec(CreateProfileQuery, p)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return p, nil
}

func (s *profileStore) Delete(ID string) error {
	res, err := s.db.Exec(DeleteProfileQuery, ID)
	if err != nil {
		return err
	}

	i, err := res.RowsAffected()
	switch {
	case i <= 0:
		return fmt.Errorf("%d rows affected by delete", i)
	case err != nil:
		return err
	}

	return nil
}

func (s *profileStore) DeleteByAccountID(accountID string) error {
	res, err := s.db.Exec(DeleteProfileByAccountIDQuery, accountID)
	if err != nil {
		return err
	}

	i, err := res.RowsAffected()
	switch {
	case i <= 0:
		return fmt.Errorf("%d rows affected by delete", i)
	case err != nil:
		return err
	}

	return nil
}

func (s *profileStore) Update(p *profile.Profile) (*profile.Profile, error) {
	_, err := s.Fetch(p.ID)
	if err != nil {
		return nil, err
	}

	// perform update on values we allow to change
	row, err := s.db.NamedExec(UpdateProfileQuery, p)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return p, nil
}

func (s *profileStore) Fetch(ID string) (*profile.Profile, error) {
	p := profile.Profile{}

	err := s.db.Get(&p, FetchProfileQuery, ID)
	if err != nil {
		return nil, err
	}

	return &p, nil
}

func (s *profileStore) Search(whereCondition string) ([]*profile.Profile, error) {
	var profiles []*profile.Profile
	query := fmt.Sprintf("%s WHERE %s", SearchProfilesQuery, whereCondition)
	err := s.db.Select(&profiles, query)

	if err != nil {
		return nil, err
	}

	return profiles, nil
}

func (s *profileStore) FetchByAccountID(accountID string) (*profile.Profile, error) {
	p := profile.Profile{}

	err := s.db.Get(&p, FetchProfileByAccountIDQuery, accountID)
	if err != nil {
		return nil, err
	}

	return &p, nil
}
