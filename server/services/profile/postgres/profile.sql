-- Connect to our database
\connect saturdate

-- Create user table
CREATE TABLE profile (
    id varchar(255) PRIMARY KEY,
    account_id varchar(255),
    images text[],
    name varchar(255),
    gender int,
    birthday timestamp,
    preference_sex int,
    preference_min_age int,
    preference_max_age int,
    city varchar(255),
    state varchar(255),
    zipcode varchar(255),
    job varchar(255),
    university varchar(255),
    show_percentage varchar(255),
    bio text,
    is_verified boolean DEFAULT FALSE,
    latitude float,
    longitude float,
    neighborhood varchar(255),
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
