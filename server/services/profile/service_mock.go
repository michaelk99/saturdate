// Code generated by MockGen. DO NOT EDIT.
// Source: services/profile/service.go

// Package profile is a generated GoMock package.
package profile

import (
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockService is a mock of Service interface
type MockService struct {
	ctrl     *gomock.Controller
	recorder *MockServiceMockRecorder
}

// MockServiceMockRecorder is the mock recorder for MockService
type MockServiceMockRecorder struct {
	mock *MockService
}

// NewMockService creates a new mock instance
func NewMockService(ctrl *gomock.Controller) *MockService {
	mock := &MockService{ctrl: ctrl}
	mock.recorder = &MockServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockService) EXPECT() *MockServiceMockRecorder {
	return m.recorder
}

// Create mocks base method
func (m *MockService) Create(prof Profile) (*Profile, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", prof)
	ret0, _ := ret[0].(*Profile)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Create indicates an expected call of Create
func (mr *MockServiceMockRecorder) Create(prof interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockService)(nil).Create), prof)
}

// Fetch mocks base method
func (m *MockService) Fetch(query IDQuery) (*Profile, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Fetch", query)
	ret0, _ := ret[0].(*Profile)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Fetch indicates an expected call of Fetch
func (mr *MockServiceMockRecorder) Fetch(query interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Fetch", reflect.TypeOf((*MockService)(nil).Fetch), query)
}

// Search mocks base method
func (m *MockService) Search(whereCondition string) ([]*Profile, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Search", whereCondition)
	ret0, _ := ret[0].([]*Profile)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Search indicates an expected call of Search
func (mr *MockServiceMockRecorder) Search(whereCondition interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Search", reflect.TypeOf((*MockService)(nil).Search), whereCondition)
}

// Update mocks base method
func (m *MockService) Update(prof Profile) (*Profile, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", prof)
	ret0, _ := ret[0].(*Profile)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Update indicates an expected call of Update
func (mr *MockServiceMockRecorder) Update(prof interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockService)(nil).Update), prof)
}

// Delete mocks base method
func (m *MockService) Delete(query IDQuery) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", query)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete
func (mr *MockServiceMockRecorder) Delete(query interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockService)(nil).Delete), query)
}
