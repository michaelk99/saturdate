package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
)

const (
	// FetchErrCode code
	FetchErrCode = "profile.fetch.error"
	// FetchExistsCode code
	FetchExistsCode = "profile.fetch.exists"
)

// Fetch checks email against password and assigns a token if valid
func Fetch(ps profile.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support POST
		if r.Method != http.MethodGet {
			log.Printf("profile.fetch.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		// req can look like the following:
		session := r.Context().Value("Session").(*token.Session)
		id := session.ProfileID
		accountID := session.AccountID

		// !!! IMPORTANT !!!
		// ?account_id query param overrides the session. This allows clients
		// to fetch other profiles on demand. Ideally this gets separated out
		// into a /search endpoint. For now it lives here
		if r.URL.Query().Get("account_id") != "" {
			accountID = r.URL.Query().Get("account_id")
		}

		if id == "" && accountID == "" {
			log.Printf("profile.fetch.invalid_id")
			http.Error(w, "invalid request, need id or account id", http.StatusBadRequest)
			return
		}

		// default query to profile id
		query := profile.IDQuery{
			Type:  profile.ID,
			Value: id,
		}

		// accountID overrides profile ID
		if accountID != "" {
			query = profile.IDQuery{
				Type:  profile.AccountID,
				Value: accountID,
			}
		}

		prof, err := ps.Fetch(query)
		if err != nil {
			resp := &je.Response{
				Code:    FetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, profile.ServiceToHTTPErrorMap(err))
			return
		}

		// return created profile
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(prof)
		if err != nil {
			log.Printf("%s: %v", FetchErrCode, err)
			resp := &je.Response{
				Code:    FetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully fetched profile id %s", id)
		return
	}
}
