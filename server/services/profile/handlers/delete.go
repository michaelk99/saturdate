package handlers

import (
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
)

const (
	// DeleteErrCode code
	DeleteErrCode = "profile.delete.error"
)

// Delete checks email against password and assigns a token if valid
func Delete(ps profile.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support DELETE
		if r.Method != http.MethodDelete {
			log.Printf("profile.delete.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		// look for the id in the session
		session := r.Context().Value("Session").(*token.Session)
		id := session.ProfileID

		if id == "" {
			log.Printf("profile.delete.no_id")
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		query := profile.IDQuery{
			Type:  profile.ID,
			Value: id,
		}

		err := ps.Delete(query)
		if err != nil {
			resp := &je.Response{
				Code:    DeleteErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, profile.ServiceToHTTPErrorMap(err))
			return
		}

		// return deleted profile
		w.WriteHeader(http.StatusOK)
		log.Printf("successfully deleted profile id %s", id)
		return
	}
}
