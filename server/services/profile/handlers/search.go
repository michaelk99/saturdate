package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"strings"
)

const (
	// SearchErrCode code
	SearchErrCode = "profile.search.error"
)

// Search checks email against password and assigns a token if valid
func Search(ps profile.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support POST
		if r.Method != http.MethodPost {
			log.Printf("profile.search.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		var ids []string
		err := json.NewDecoder(r.Body).Decode(&ids)
		if err != nil {
			log.Printf("%s: %v", CreateErrCode, err)
			resp := &je.Response{
				Code:    CreateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// surround each ID with quotes to format properly in query
		for i, id := range ids {
			ids[i] = fmt.Sprintf("'%s'", id)
		}

		// whereCondition = id IN (1,2,3,4,5...)
		whereCondition := fmt.Sprintf("id IN (%s)", strings.Join(ids, ","))
		profs, err := ps.Search(whereCondition)

		if err != nil {
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, profile.ServiceToHTTPErrorMap(err))
			return
		}

		// return created profile
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(profs)
		if err != nil {
			log.Printf("%s: %v", SearchErrCode, err)
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully retrieved profiles %s", ids)
		return
	}
}
