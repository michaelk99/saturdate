package profile

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/lib/pq"
	"gopkg.in/guregu/null.v3"
)

// GenderType gender enum
type GenderType int

const (
	// Male gender
	Male GenderType = iota
	// Female gender
	Female
)

// MarshalJSON implements json.Marshaler.
func (gt GenderType) MarshalJSON() ([]byte, error) {
	switch gt {
	case Male:
		return []byte(strconv.FormatInt(0, 10)), nil
	case Female:
		return []byte(strconv.FormatInt(1, 10)), nil
	}

	return nil, fmt.Errorf("Error marshaling gender type")
}

// UnmarshalJSON implements json.Unmarshaler.
func (gt *GenderType) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}

	i, ok := v.(float64)

	if !ok {
		return fmt.Errorf("GenderType must be float64: %T %+v", v, v)
	}

	switch int(i) {
	case 0:
		*gt = Male
		return nil
	case 1:
		*gt = Female
		return nil
	default:
		return fmt.Errorf("GenderType invalid sex %f", i)
	}
}

// PreferenceSexType sexual preference of user
type PreferenceSexType int

const (
	// PreferenceMale prefer males
	PreferenceMale PreferenceSexType = iota
	// PreferenceFemale prefer females
	PreferenceFemale
	// PreferenceBoth prefer male or females
	PreferenceBoth
)

// MarshalJSON implements json.Marshaler.
func (ps PreferenceSexType) MarshalJSON() ([]byte, error) {
	switch ps {
	case PreferenceMale:
		return []byte(strconv.FormatInt(0, 10)), nil
	case PreferenceFemale:
		return []byte(strconv.FormatInt(1, 10)), nil
	case PreferenceBoth:
		return []byte(strconv.FormatInt(2, 10)), nil
	}
	return nil, fmt.Errorf("Error marshaling preference sex")
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports number and null input.
// 0 will not be considered a null Bool.
// It also supports unmarshalling a sql.NullBool.
func (ps *PreferenceSexType) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}

	i, ok := v.(float64)

	if !ok {
		return fmt.Errorf("PreferenceSex must be float64: %T %+v", v, v)
	}

	switch int(i) {
	case 0:
		*ps = PreferenceMale
		return nil
	case 1:
		*ps = PreferenceFemale
		return nil
	case 2:
		*ps = PreferenceBoth
		return nil
	default:
		return fmt.Errorf("PreferenceSex invalid sex %f", i)
	}
}

// Location data
type Location struct {
	City         string  `json:"city" db:"city"`
	State        string  `json:"state" db:"state"`
	Zipcode      string  `json:"zipcode" db:"zipcode"`
	Latitude     float64 `json:"latitude" db:"latitude"`
	Longitude    float64 `json:"longitude" db:"longitude"`
	Neighborhood string  `json:"neighborhood" db:"neighborhood"`
}

// Preferences data
type Preferences struct {
	// !!! IMPORTANT !!!: pref sex type validation happens in unmarshaller
	PreferenceSex    PreferenceSexType `json:"sex" db:"preference_sex"`
	PreferenceMinAge int               `validate:"required,gte=18,lte=100" json:"min_age" db:"preference_min_age"`
	PreferenceMaxAge int               `validate:"required,gte=18,lte=100" json:"max_age" db:"preference_max_age"`
}

// Profile is a retrieved profile
type Profile struct {
	ID        string         `json:"id" db:"id"`
	AccountID string         `json:"account_id" db:"account_id"`
	Images    pq.StringArray `json:"images" db:"images"`
	Name      string         `validate:"required" json:"name" db:"name"`
	// !!! IMPORTANT !!!: gender validation happens in unmarshaller
	Gender         GenderType  `json:"gender" db:"gender"`
	Birthday       string      `validate:"required" json:"birthday" db:"birthday"`
	Bio            null.String `json:"bio" db:"bio"`
	Job            null.String `json:"job" db:"job"`
	University     null.String `json:"university" db:"university"`
	ShowPercentage null.String `json:"show_percentage" db:"show_percentage"`
	IsVerified     bool        `json:"is_verified" db:"is_verified"`
	Location       `json:"location"`
	Preferences    `json:"preferences"`
	CreatedAt      string `json:"created_at" db:"created_at"`
	UpdatedAt      string `json:"updated_at" db:"updated_at"`
}
