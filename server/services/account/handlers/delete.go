package handlers

import (
	"log"
	"net/http"
	"path/filepath"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/account"
	"strings"
)

const (
	// DeleteErrCode code
	DeleteErrCode = "account.delete.error"
)

// Delete checks email against password and assigns a token if valid
func Delete(s account.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support DELETE
		if r.Method != http.MethodDelete {
			log.Printf("account.delete.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/account/")

		if id == "" {
			log.Printf("account.delete.no_id")
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		idQuery := account.IDQuery{
			Type:  account.ID,
			Value: id,
		}

		err := s.Delete(idQuery)
		if err != nil {
			resp := &je.Response{
				Code:    DeleteErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, account.ServiceToHTTPErrorMap(err))
			return
		}

		// return deleted account
		w.WriteHeader(http.StatusOK)
		log.Printf("successfully deleted account id %s", id)
		return
	}
}
