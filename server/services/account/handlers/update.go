package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/account"
)

const (
	// UpdateErrCode code
	UpdateErrCode = "account.update.error"
	// UpdateErrAccountNotFoundCode code
	UpdateErrAccountNotFoundCode = "account.update.notfound"
	// UpdateErrInvalidIDCode code
	UpdateErrInvalidIDCode = "account.update.invalid_id"
	// UpdateMethodNotSupportedCode code
	UpdateMethodNotSupportedCode = "account.update.method_not_supported"
	// UpdatePath is the path we expect the request specify
	UpdatePath = "/api/v1/account/"
)

// Update receives an account and updates the specified public feilds. Update will not be used
// for password reset.
func Update(s account.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support PUT
		if r.Method != http.MethodPut {
			log.Printf(UpdateMethodNotSupportedCode)
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		// parse ID
		id := strings.TrimPrefix(r.URL.Path, UpdatePath)
		if id == "" {
			log.Print(UpdateErrInvalidIDCode)
			resp := &je.Response{
				Code:    UpdateErrInvalidIDCode,
				Message: "invalid or no id specified in path",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// deserialize provided account
		var acc account.Account
		err := json.NewDecoder(r.Body).Decode(&acc)
		if err != nil {
			log.Printf(UpdateErrCode)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: "bad request provided",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// confirm provided account id matches path
		if id != acc.ID {
			log.Printf("path id: %s does not match account id: %s. code: %s", id, acc.ID, UpdateErrCode)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: "provided account id in body does not match path parameter",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// confirm account exists
		// TODO: Implement a service.Exists method to determine if an account exists or not
		idQuery := account.IDQuery{
			Type:  account.ID,
			Value: id,
		}
		_, err = s.Fetch(idQuery)
		if err != nil {
			log.Printf("%s: %v", UpdateErrAccountNotFoundCode, err)
			resp := &je.Response{
				Code:    UpdateErrAccountNotFoundCode,
				Message: "provided account id does not exist",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// update account with details
		_, err = s.Update(acc)
		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: "failed to update account",
			}
			je.Error(r, w, resp, http.StatusInternalServerError)
			return
		}

		// return updated account
		err = json.NewEncoder(w).Encode(acc)
		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: "failed to update account",
			}
			je.Error(r, w, resp, http.StatusInternalServerError)
			return
		}

		return
	}
}
