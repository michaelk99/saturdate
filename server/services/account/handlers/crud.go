package handlers

import (
	"net/http"

	"gitlab.com/michaelk99/saturdate/server/services/account"
)

// CRUD forwards request based on http method
func CRUD(s account.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			Fetch(s).ServeHTTP(w, r)
			return
		case http.MethodDelete:
			Delete(s).ServeHTTP(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}
	}
}
