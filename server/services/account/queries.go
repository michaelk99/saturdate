package account

type Identifier int

const (
	EmailID Identifier = iota
	ID
)

type IDQuery struct {
	Type  Identifier
	Value string
}
