package account

import (
	"time"

	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/michaelk99/saturdate/server/pkg/crypto"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
)

// Service is a public interface for implementing our Account service
type Service interface {
	// SignUp creates an account into the backing account store
	SignUp(req AccountCredentials) (*Account, error)
	// LogIn takes account credentials and returns a token if a successful login occurs
	LogIn(ctx context.Context, logIn AccountCredentials) (token string, err error)
	// Fetch retrieves a user from the backing account store
	Fetch(q IDQuery) (*Account, error)
	// Update updates public fields on an account
	Update(acc Account) (*Account, error)
	// Delete removes an account from the backend account store
	Delete(q IDQuery) error
}

// Service is a private implementation of our account Service
type service struct {
	ds AccountStore
	tc TokenCreator
	pc profile.Client
}

// NewService is a constructor for our Account service implementation
func NewService(ds AccountStore, tc TokenCreator, pc profile.Client) Service {
	return &service{
		ds: ds,
		tc: tc,
		pc: pc,
	}
}

// SignUp registers a new user and persists them to the backing account store
func (s *service) SignUp(signUp AccountCredentials) (*Account, error) {
	// check if account email exists in db. if err is nil an account was found.
	// TODO: make AccountStore specific errors indicating specific conditions
	_, err := s.ds.FetchByEmail(signUp.Email)
	if err == nil {
		return nil, ErrUserExists{}
	}

	// create account
	ts := time.Now().Format(time.RFC3339)
	a := &Account{
		CreatedAt: ts,
		UpdatedAt: ts,
		ID:        uuid.New().String(),
		Email:     signUp.Email,
		// default to `true`. Can use this field in the future to disable
		// accounts at will and prevent from logging in using the app
		Enabled: true,
	}

	pass, err := crypto.HashPassword(signUp.Password)
	if err != nil {
		return nil, ErrPasswordHash{}
	}

	// set the hashed password to the account struct
	a.Password = pass

	// store account in AccountStore
	_, err = s.ds.Create(a)
	if err != nil {
		return nil, ErrInternal{msg: err.Error()}
	}

	return a, nil
}

// LogIn authenticates an account's credentials and returns a token if successful.
func (s *service) LogIn(ctx context.Context, logIn AccountCredentials) (token string, err error) {
	// check if account email exists in db. if err is nil an account was found.
	// TODO: make AccountStore specific errors indicating specific conditions
	a, err := s.ds.FetchByEmail(logIn.Email)
	if err != nil {
		return "", ErrUserNotFound{}
	}

	// confirm password
	valid := crypto.ValidatePassword(logIn.Password, a.Password)
	if !valid {
		return "", ErrInvalidLogin{}
	}

	// fashion token [with mock profileID]
	token, err = s.tc.Create(a, "-1")
	if err != nil {
		return "", ErrCreateToken{err}
	}

	// get profile id
	// !!! IMPORTANT !!!
	// it is okay if the profile is not found, users can still login without
	// a profile [only need an account to login]
	prof, err := s.pc.GetProfileByAccountID(token, a.ID)
	if err != nil {
		fmt.Println("Profile not found, continuing..")
	}
	profID := ""
	if prof != nil {
		profID = prof.ID
	}

	// fashion token [with correct profile ID]
	token, err = s.tc.Create(a, profID)
	if err != nil {
		return "", ErrCreateToken{err}
	}

	return token, nil
}

// Fetch retrieves a user from the backing account store. We return an error if any issues occurs
func (s *service) Fetch(q IDQuery) (*Account, error) {
	var a *Account
	var err error

	switch q.Type {
	case EmailID:
		a, err = s.ds.FetchByEmail(q.Value)
		if err != nil {
			return nil, ErrUserNotFound{}
		}
	case ID:
		a, err = s.ds.Fetch(q.Value)
		if err != nil {
			return nil, ErrUserNotFound{}
		}
	default:
		return nil, ErrInvalidIDType{}
	}

	return a, nil
}

// Update updates public fields of an account. This method will always update the UpdatedAt
// timestamp when called with an account.
func (s *service) Update(acc Account) (*Account, error) {
	// confirm account exists
	_, err := s.ds.Fetch(acc.ID)
	if err != nil {
		return nil, ErrUserNotFound{}
	}

	// update timestamps on account
	ts := time.Now().Format(time.RFC3339)
	acc.UpdatedAt = ts

	a, err := s.ds.Update(&acc)
	if err != nil {
		return nil, ErrUpdateFail{err}
	}

	return a, nil
}

// Delete removes an account from the backing account store
func (s *service) Delete(q IDQuery) error {
	switch q.Type {
	case EmailID:
		// TODO: Implement delete by email
	case ID:
		err := s.ds.Delete(q.Value)
		if err != nil {
			return ErrDeleteFail{err}
		}
	default:
		return ErrInvalidIDType{}
	}

	return nil
}
