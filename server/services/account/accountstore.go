package account

type AccountStore interface {
	Create(*Account) (*Account, error)
	Delete(ID string) error
	Update(*Account) (*Account, error)
	Fetch(ID string) (*Account, error)
	FetchByEmail(email string) (*Account, error)
}
