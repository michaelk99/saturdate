package account

// Creator is a public interface which creates a token for the provided
// subject. Implementations can decide what format this token can be (jwt, saml, etc...)
type TokenCreator interface {
	Create(acct *Account, profID string) (string, error)
}
