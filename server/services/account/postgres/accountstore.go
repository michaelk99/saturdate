package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/services/account"
)

// queries are written to use sqlx.NamedExec() method. this method maps "db" struct tags with
// the : prefixed names in the values parameter
const (
	CreateAccountQuery = `INSERT INTO account (id, email, password, created_at, updated_at, enabled)
							VALUES (:id, :email, :password, :created_at, :updated_at, :enabled);`
	UpdateAccountQuery       = `UPDATE account SET email = :email, password = :password WHERE id = :id;`
	FetchAccountQuery        = `SELECT * FROM account WHERE id = $1;`
	FetchAccountByEmailQuery = `SELECT * FROM account WHERE email = $1;`
	DeleteAccountQuery       = `DELETE FROM account WHERE id = $1;`
)

// accountStore is a private implementation of the account.AccountStoreinterface
type accountStore struct {
	// a sqlx database object
	db *sqlx.DB
}

// NewAccountStore returns a postgres db implementation of the account.AccountStore interface
func NewAccountStore(db *sqlx.DB) account.AccountStore {
	return &accountStore{
		db: db,
	}
}

// Create creates a account in a postgres db
func (s *accountStore) Create(a *account.Account) (*account.Account, error) {
	row, err := s.db.NamedExec(CreateAccountQuery, a)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return a, nil
}

func (s *accountStore) Delete(ID string) error {
	res, err := s.db.Exec(DeleteAccountQuery, ID)
	if err != nil {
		return err
	}

	i, err := res.RowsAffected()
	switch {
	case i <= 0:
		return fmt.Errorf("%d rows affected by delete", i)
	case err != nil:
		return err
	}

	return nil
}

func (s *accountStore) Update(a *account.Account) (*account.Account, error) {
	_, err := s.Fetch(a.ID)
	if err != nil {
		return nil, err
	}

	// perform update on values we allow to change
	row, err := s.db.NamedExec(UpdateAccountQuery, a)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return a, nil
}

func (s *accountStore) Fetch(ID string) (*account.Account, error) {
	a := account.Account{}

	err := s.db.Get(&a, FetchAccountQuery, ID)
	if err != nil {
		return nil, err
	}

	return &a, nil
}

func (s *accountStore) FetchByEmail(email string) (*account.Account, error) {
	a := account.Account{}

	err := s.db.Get(&a, FetchAccountByEmailQuery, email)
	if err != nil {
		return nil, err
	}

	return &a, nil
}
