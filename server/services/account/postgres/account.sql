-- Connect to our database
\connect saturdate

-- Create user table
CREATE TABLE account (
    id varchar(255) PRIMARY KEY,
    email varchar(255) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    enabled boolean DEFAULT TRUE,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
