package account

// Account is a retrieved and authenticated user.
type Account struct {
	ID        string `json:"id" db:"id"`
	Email     string `json:"email" db:"email"`
	Password  string `json:"-" db:"password"`
	CreatedAt string `json:"created_at" db:"created_at"`
	UpdatedAt string `json:"updated_at" db:"updated_at"`
	Enabled   bool   `json:"enabled" db:"enabled"`
}
