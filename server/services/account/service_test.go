package account_test

import (
	"context"
	"fmt"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/account"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
)

func getContext() context.Context {
	ctx := context.Background()
	tk := &token.Session{}
	ctx = context.WithValue(ctx, "Session", tk)
	ctx = context.WithValue(ctx, "Token", "some-jwt")
	return ctx
}

var TestSignupTT = []struct {
	signUpReq              account.AccountCredentials
	accoutStoreFetchReturn []interface{}
	// should we expect account store to be called with Create
	expectCreate bool
}{
	{
		account.AccountCredentials{
			Email:    "test1@email.com",
			Password: "pass1234!@#$",
		},
		[]interface{}{
			nil, fmt.Errorf("not found"),
		},
		true,
	},
	{
		account.AccountCredentials{
			Email:    "test2@email.com",
			Password: "pass5678%%^&*",
		},
		// simulate account existing
		[]interface{}{
			&account.Account{}, nil,
		},
		false,
	},
}

// TestSignup confirms our signup method doe the correct things. Making sure we create
// the account if it does not exist and we return back a account with valid fields.
func TestSignup(t *testing.T) {
	// create mock account store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestSignupTT {
		// not used in this test
		tc := account.NewMockTokenCreator(ctrl)

		// create mock account store
		as := account.NewMockAccountStore(ctrl)
		as.EXPECT().FetchByEmail(tt.signUpReq.Email).Return(tt.accoutStoreFetchReturn...)
		if tt.expectCreate {
			as.EXPECT().Create(gomock.Any())
		}

		// create mock profile client. not called
		pc := profile.NewMockClient(ctrl)

		// test our service
		s := account.NewService(as, tc, pc)
		acc, err := s.SignUp(tt.signUpReq)

		// handle when we expect create to fail
		if !tt.expectCreate {
			// we should see an error
			assert.NotNil(t, err)
			// err should be account exists
			assert.IsType(t, account.ErrUserExists{}, err)
			// we should had back nil account
			assert.Nil(t, acc)
			continue
		}

		// handle when create passes
		assert.NotNil(t, acc)
		assert.Nil(t, err)

		assert.Equal(t, tt.signUpReq.Email, acc.Email)
		assert.NotEmpty(t, acc.CreatedAt)
		assert.NotEmpty(t, acc.UpdatedAt)
		assert.NotEmpty(t, acc.Email)
		assert.NotEmpty(t, acc.ID)
		assert.NotEmpty(t, acc.Password)
		assert.True(t, acc.Enabled)
	}
}

var TestLogInTT = []struct {
	// friendly name to identify test case
	name string
	// credentials used to login
	creds account.AccountCredentials
	// do we expect account store's fetch of user email to succeed
	expectFetch bool
	// do we expect password validation to succeed
	expectValidate bool
	// do we expect token creation to succeed
	expectToken bool
	// do we expect profile to be fetched
	profileFetch bool
	// do we expect profile fetch to fail
	expectProfileFetch bool
	// returned value from profile.Client.GetProfileByAccountID
	profileGetReturn []interface{}
	// returned values from AccountStore.FetchyByEmail
	fetchReturn []interface{}
	// returned values from TokenCreator.Create
	createReturn []interface{}
	// mock token create
	tokenCreate bool
}{
	// case where everything should pass
	{
		name: "no errors",
		creds: account.AccountCredentials{
			Email:    "test-user",
			Password: "_8Ff>t8%RAwLV}#2",
		},
		expectFetch:        true,
		expectValidate:     true,
		expectToken:        true,
		profileFetch:       true,
		expectProfileFetch: true,
		profileGetReturn: []interface{}{
			&profile.Profile{
				ID: "1",
			},
			nil,
		},
		fetchReturn: []interface{}{
			&account.Account{Password: "$2y$07$eUVy5hzNYM9tZLs4XdZjdubggKVREGlou5KDUsMMy97sZwv5.B.Ra"},
			nil,
		},
		createReturn: []interface{}{
			"successful-token", nil,
		},
		tokenCreate: true,
	},
	// case where we should get ErrUserNotFound
	{
		name: "account not found",
		creds: account.AccountCredentials{
			Email:    "test-user",
			Password: "_8Ff>t8%RAwLV}#2",
		},
		expectFetch:        false,
		expectValidate:     true,
		expectToken:        false,
		profileFetch:       false,
		expectProfileFetch: false,
		profileGetReturn: []interface{}{
			&profile.Profile{
				ID: "1",
			},
			nil,
		},
		fetchReturn: []interface{}{
			nil,
			fmt.Errorf("fetch by email error"),
		},
		createReturn: []interface{}{
			"successful-token", nil,
		},
		tokenCreate: false,
	},
	// case where we should get ErrInvalidLogin
	{
		name: "invalid login",
		creds: account.AccountCredentials{
			Email:    "test-user",
			Password: "failing-password",
		},
		expectFetch:        true,
		expectValidate:     false,
		expectToken:        true,
		profileFetch:       false,
		expectProfileFetch: false,
		profileGetReturn: []interface{}{
			&profile.Profile{
				ID: "1",
			},
			nil,
		},
		fetchReturn: []interface{}{
			&account.Account{Password: "$2y$07$eUVy5hzNYM9tZLs4XdZjdubggKVREGlou5KDUsMMy97sZwv5.B.Ra"},
			nil,
		},
		createReturn: []interface{}{
			"successful-token", nil,
		},
		tokenCreate: false,
	},
	// case where we should get ErrCreateToken
	{
		name: "token creation failure",
		creds: account.AccountCredentials{
			Email:    "test-user",
			Password: "_8Ff>t8%RAwLV}#2",
		},
		expectFetch:        true,
		expectValidate:     true,
		expectToken:        false,
		profileFetch:       true,
		expectProfileFetch: true,
		profileGetReturn: []interface{}{
			&profile.Profile{
				ID: "1",
			},
			nil,
		},
		fetchReturn: []interface{}{
			&account.Account{Password: "$2y$07$eUVy5hzNYM9tZLs4XdZjdubggKVREGlou5KDUsMMy97sZwv5.B.Ra"},
			nil,
		},
		createReturn: []interface{}{
			"", fmt.Errorf("token creation failed"),
		},
		tokenCreate: true,
	},
	// case where profile service errors
	{
		name: "profile service error",
		creds: account.AccountCredentials{
			Email:    "test-user",
			Password: "_8Ff>t8%RAwLV}#2",
		},
		expectFetch:        true,
		expectValidate:     true,
		expectToken:        true,
		profileFetch:       true,
		expectProfileFetch: false,
		profileGetReturn: []interface{}{
			// in actuality we'd return a nil profile here, but we feed this ID to the tokenCreate method
			&profile.Profile{
				ID: "",
			},
			fmt.Errorf("failed to contact profile service"),
		},
		fetchReturn: []interface{}{
			&account.Account{Password: "$2y$07$eUVy5hzNYM9tZLs4XdZjdubggKVREGlou5KDUsMMy97sZwv5.B.Ra"},
			nil,
		},
		createReturn: []interface{}{
			"successful-token", nil,
		},
		tokenCreate: true,
	},
}

// TestLogIn confirms the LogIn method returns the correct errors when the fetch for a user fails
// a password is not validated correctly, or a token creation fails. Also confirms we return a
// token when all the above work.
func TestLogIn(t *testing.T) {
	// create mock account store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestLogInTT {
		t.Logf("test table: %v", tt.name)

		as := account.NewMockAccountStore(ctrl)
		tc := account.NewMockTokenCreator(ctrl)
		// create mock profile client. not called
		pc := profile.NewMockClient(ctrl)

		as.EXPECT().FetchByEmail(tt.creds.Email).Return(tt.fetchReturn...)

		if tt.profileFetch {
			accID := tt.fetchReturn[0].(*account.Account).ID
			pc.EXPECT().GetProfileByAccountID(gomock.Any(), accID).Return(tt.profileGetReturn...)
		}

		if tt.tokenCreate {
			acc := tt.fetchReturn[0].(*account.Account)
			profID := tt.profileGetReturn[0].(*profile.Profile).ID
			token := tt.createReturn[0].(string)
			tokenErr := tt.createReturn[1]
			first := tc.EXPECT().Create(acc, "-1").Return(gomock.Any().String(), nil)
			second := tc.EXPECT().Create(acc, profID).Return(token, tokenErr)
			gomock.InOrder(
				first,
				second,
			)
		}

		service := account.NewService(as, tc, pc)
		token, err := service.LogIn(getContext(), tt.creds)

		if !tt.expectFetch {
			assert.Empty(t, token)
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrUserNotFound{}, err)
			continue
		}

		if !tt.expectValidate {
			assert.Empty(t, token)
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrInvalidLogin{}, err)
			continue
		}

		// special case: when profile fetch fails we want to confirm a token is still created and error is nill
		// then we do not continue to next entry, we follow the code to the JWT check at the end
		if !tt.expectProfileFetch {
			assert.NotEmpty(t, token)
			assert.Nil(t, err)
		}

		if !tt.expectToken {
			assert.Empty(t, token)
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrCreateToken{}, err)
			continue
		}

		assert.Equal(t, tt.createReturn[0].(string), token)
		assert.Nil(t, err)
	}
}

var TestFetchTT = []struct {
	// friendly name for test
	name string
	// the IDQuery passed to our service Fetch method
	idQuery account.IDQuery
	// do we expect accountstore.FetchByEmail to be called
	expectFetchByEmail bool
	// do we expect accountstore.Fetch to be called
	expectFetch bool
	// do we expect ErrInvalidIDType to be returned from fetch
	expectErrInvalidIDType bool
	// do we expect ErrUserNotFound
	expectErrUserNotFound bool
	// the returned arguments from either accountstore.Fetch(ByEmail) call
	fetchReturn []interface{}
}{
	{
		name: "fetch; invalid id type",
		idQuery: account.IDQuery{
			Type:  account.Identifier(999), //  abitrary large enum value
			Value: "testemail@testmail.com",
		},
		expectFetchByEmail:     false,
		expectFetch:            false,
		expectErrInvalidIDType: true,
		expectErrUserNotFound:  false,
		fetchReturn: []interface{}{
			&account.Account{Email: "testemail@testmail.com"},
			nil,
		},
	},
	{
		name: "fetch by email; success",
		idQuery: account.IDQuery{
			Type:  account.EmailID,
			Value: "testemail@testmail.com",
		},
		expectFetchByEmail:     true,
		expectFetch:            false,
		expectErrInvalidIDType: false,
		expectErrUserNotFound:  false,
		fetchReturn: []interface{}{
			&account.Account{Email: "testemail@testmail.com"},
			nil,
		},
	},
	{
		name: "fetch by email; user not found",
		idQuery: account.IDQuery{
			Type:  account.EmailID,
			Value: "testemail@testmail.com",
		},
		expectFetchByEmail:     true,
		expectFetch:            false,
		expectErrInvalidIDType: false,
		expectErrUserNotFound:  true,
		fetchReturn: []interface{}{
			nil,
			fmt.Errorf("user not found"),
		},
	},
	{
		name: "fetch by id; success",
		idQuery: account.IDQuery{
			Type:  account.ID,
			Value: "8e185b16-5695-4f06-820d-5463f679f37c",
		},
		expectFetchByEmail:     false,
		expectFetch:            true,
		expectErrInvalidIDType: false,
		expectErrUserNotFound:  false,
		fetchReturn: []interface{}{
			&account.Account{ID: "8e185b16-5695-4f06-820d-5463f679f37c"},
			nil,
		},
	},
	{
		name: "fetch by id; user not found",
		idQuery: account.IDQuery{
			Type:  account.ID,
			Value: "8e185b16-5695-4f06-820d-5463f679f37c",
		},
		expectFetchByEmail:     false,
		expectFetch:            true,
		expectErrInvalidIDType: false,
		expectErrUserNotFound:  true,
		fetchReturn: []interface{}{
			nil,
			fmt.Errorf("user not found"),
		},
	},
}

// TestFetch tests that the fetch method on our service returns the correct errors and calls
// the correct methods on it's account store.
func TestFetch(t *testing.T) {
	// create mock account store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestFetchTT {
		t.Logf("test table: %v", tt.name)

		as := account.NewMockAccountStore(ctrl)
		tc := account.NewMockTokenCreator(ctrl) // only used for constructor of service
		// create mock profile client. not called
		pc := profile.NewMockClient(ctrl)

		if tt.expectFetch {
			as.EXPECT().Fetch(tt.idQuery.Value).Return(tt.fetchReturn...)
		}
		if tt.expectFetchByEmail {
			as.EXPECT().FetchByEmail(tt.idQuery.Value).Return(tt.fetchReturn...)
		}

		service := account.NewService(as, tc, pc)
		acc, err := service.Fetch(tt.idQuery)

		if tt.expectErrInvalidIDType {
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrInvalidIDType{}, err)
			continue
		}

		if tt.expectErrUserNotFound {
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrUserNotFound{}, err)
			continue
		}

		assert.Nil(t, err)
		assert.NotNil(t, acc)

		switch tt.idQuery.Type {
		case account.EmailID:
			assert.Equal(t, tt.fetchReturn[0].(*account.Account).Email, acc.Email)
		case account.ID:
			assert.Equal(t, tt.fetchReturn[0].(*account.Account).ID, acc.ID)
		}
	}
}

// TestUpdateTT is a test table for testing the Update method
var TestUpdateTT = []struct {
	name                  string
	testAcc               account.Account
	expectFetch           bool
	expectUpdate          bool
	expectErrUserNotFound bool
	expectErrUpdateFail   bool
	fetchReturn           []interface{}
	updateReturn          []interface{}
}{
	{
		name: "update successful",
		testAcc: account.Account{
			ID:    "cebd548f-11c7-4460-84e2-a098feda72f3",
			Email: "updated@email.com",
		},
		expectFetch:           true,
		expectUpdate:          true,
		expectErrUserNotFound: false,
		expectErrUpdateFail:   false,
		fetchReturn: []interface{}{
			nil, // used because update method throws this value away
			nil,
		},
		updateReturn: []interface{}{
			&account.Account{
				ID:    "cebd548f-11c7-4460-84e2-a098feda72f3",
				Email: "updated@email.com",
			},
			nil,
		},
	},
	{
		name: "update failure; fetch fails",
		testAcc: account.Account{
			ID:    "cebd548f-11c7-4460-84e2-a098feda72f3",
			Email: "updated@email.com",
		},
		expectFetch:           true,
		expectUpdate:          false,
		expectErrUserNotFound: true,
		expectErrUpdateFail:   false,
		fetchReturn: []interface{}{
			nil, // used because update method throws this value away
			fmt.Errorf("failed to fetch user"),
		},
		updateReturn: []interface{}{
			&account.Account{
				ID:    "cebd548f-11c7-4460-84e2-a098feda72f3",
				Email: "updated@email.com",
			},
			nil,
		},
	},
	{
		name: "update failure; update fails",
		testAcc: account.Account{
			ID:    "cebd548f-11c7-4460-84e2-a098feda72f3",
			Email: "updated@email.com",
		},
		expectFetch:           true,
		expectUpdate:          true,
		expectErrUserNotFound: false,
		expectErrUpdateFail:   true,
		fetchReturn: []interface{}{
			nil, // used because update method throws this value away
			nil,
		},
		updateReturn: []interface{}{
			nil,
			fmt.Errorf("failed to update account"),
		},
	},
}

// TestUpdate tests that the update method returns the correct errors and output.
func TestUpdate(t *testing.T) {
	// create mock account store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestUpdateTT {
		t.Logf("test table: %v", tt.name)

		as := account.NewMockAccountStore(ctrl)
		tc := account.NewMockTokenCreator(ctrl) // only used for constructor of service
		// create mock profile client. not called
		pc := profile.NewMockClient(ctrl)

		if tt.expectFetch {
			as.EXPECT().Fetch(tt.testAcc.ID).Return(tt.fetchReturn...)
		}
		if tt.expectUpdate {
			// this is a little bit hacky, but becuase we cannot predict the timestamp appended
			// to the account before the accountstore.Update method is called, we just ensure that
			// accountstore.Update is called with an *account.Account type
			as.EXPECT().Update(gomock.AssignableToTypeOf(&account.Account{})).Return(tt.updateReturn...)
		}

		service := account.NewService(as, tc, pc)
		acc, err := service.Update(tt.testAcc)

		if tt.expectErrUserNotFound {
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrUserNotFound{}, err)
			continue
		}

		if tt.expectErrUpdateFail {
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrUpdateFail{}, err)
			continue
		}

		assert.Nil(t, err)
		assert.Equal(t, &tt.testAcc, acc)
	}
}

// TestDeleteTT is the test table for TestDelete
var TestDeleteTT = []struct {
	// name of test case
	name string
	// IDQuery passed to our Delete method
	idQuery account.IDQuery
	// do we expect the Delete method on acount store to be called
	expectStoreDelete bool
	// do we expect ErrDeleteFail to be returned
	expectErrDeleteFail bool
	// do we expect ErrInvalidIDType to be returned
	expectErrInvalidIDType bool
	// the returned error from our store mock
	storeDeleteReturn error
}{
	{
		name: "successful delete",
		idQuery: account.IDQuery{
			Type:  account.ID,
			Value: "test-uuid",
		},
		expectStoreDelete:      true,
		expectErrDeleteFail:    false,
		expectErrInvalidIDType: false,
		storeDeleteReturn:      nil,
	},
	{
		name: "delete fail",
		idQuery: account.IDQuery{
			Type:  account.ID,
			Value: "test-uuid",
		},
		expectStoreDelete:      true,
		expectErrDeleteFail:    true,
		expectErrInvalidIDType: false,
		storeDeleteReturn:      fmt.Errorf("failed to delete"),
	},
	{
		name: "invalid id type",
		idQuery: account.IDQuery{
			Type:  account.Identifier(999), // arbitrary large enum value
			Value: "test-uuid",
		},
		expectStoreDelete:      false,
		expectErrDeleteFail:    false,
		expectErrInvalidIDType: true,
		storeDeleteReturn:      nil,
	},
}

// TestDelete confirms the delete method no our service returns the correct errors and calls the account store correctly
func TestDelete(t *testing.T) {
	// create mock account store
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestDeleteTT {
		t.Logf("test table: %v", tt.name)

		as := account.NewMockAccountStore(ctrl)
		tc := account.NewMockTokenCreator(ctrl) // only used for constructor of service
		// create mock profile client. not called
		pc := profile.NewMockClient(ctrl)

		if tt.expectStoreDelete {
			as.EXPECT().Delete(tt.idQuery.Value).Return(tt.storeDeleteReturn)
		}

		service := account.NewService(as, tc, pc)
		err := service.Delete(tt.idQuery)

		if tt.expectErrDeleteFail {
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrDeleteFail{}, err)
			continue
		}

		if tt.expectErrInvalidIDType {
			assert.NotNil(t, err)
			assert.IsType(t, account.ErrInvalidIDType{}, err)
			continue
		}

		assert.Nil(t, err)

	}
}
