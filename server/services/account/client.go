package account

import (
	"net/http"
	"net/url"
)

const (
	loginPath = "/api/v1/account/login/"
)

type Client interface {
	LogIn(logIn AccountCredentials) (token string, err error)
}

// Client is an http client
type client struct {
	c       *http.Client
	jwt     string
	profURL *url.URL
}

// NewClient is a constructor for our client
func NewClient(jwt string, profURL *url.URL) Client {
	c := &client{
		c:       &http.Client{},
		jwt:     jwt,
		profURL: profURL,
	}

	return c
}

func (c *client) LogIn(logIn AccountCredentials) (token string, err error) {
	// TODO
	// req := http.NewRequest("POST", c.profURL.Host(),
	return "", nil
}
