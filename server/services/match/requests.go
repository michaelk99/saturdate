package match

// Location data struct
type Location struct {
	Latitude  float64 `validate:"required,gte=-90,lte=90" json:"latitude"`
	Longitude float64 `validate:"required,gte=-180,lte=180" json:"longitude"`
}

// SwipeReq swipe data request
type SwipeReq struct {
	ProfileID string `validate:"required" json:"profile_id"`
	Decision  *bool  `validate:"required" json:"decision"`
}
