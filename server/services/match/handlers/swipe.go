package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/match"
)

const (
	// SwipeErrCode error code
	SwipeErrCode = "match.swipe.create.error"
	// SwipeExistsCode error code exists
	SwipeExistsCode = "match.swipe.create.exists"
	// SwipeBadDataCode bad data
	SwipeBadDataCode = "match.swipe.create.bad_data"
)

// Swipe sign up handler
func Swipe(v validator.Validator, ms match.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support POST
		if r.Method != http.MethodPost {
			log.Printf("match.swipe.create.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		var sr match.SwipeReq
		err := json.NewDecoder(r.Body).Decode(&sr)
		if err != nil {
			log.Printf("%s: %v", SwipeBadDataCode, err)
			resp := &je.Response{
				Code:    SwipeBadDataCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// validate match
		ok, fieldErrors := v.Struct(sr)
		if !ok {
			resp := &je.Response{
				Code:       SwipeBadDataCode,
				Message:    SwipeBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		ee, err := ms.SwipeCandidate(r.Context(), sr)
		if err != nil {
			resp := &je.Response{
				Code:    SwipeErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, match.ServiceToHTTPErrorMap(err))
			return
		}

		// if successful and no event returned, return empty response
		if ee == nil {
			// return 200 ok that match was created
			w.WriteHeader(http.StatusCreated)
			log.Println("successfully completed swipe")
			return
		}

		err = json.NewEncoder(w).Encode(ee)
		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Println("successfully created match")
		return
	}
}
