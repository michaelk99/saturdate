package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/match"
)

const (
	// FetchAllErrCode code
	FetchAllErrCode = "match.fetchall.error"
)

// FetchAll checks email against password and assigns a token if valid
func FetchAll(s match.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("match.fetchall.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		session := r.Context().Value("Session").(*token.Session)
		matches, err := s.FetchAll(session.ProfileID)
		if err != nil {
			resp := &je.Response{
				Code:    FetchAllErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, match.ServiceToHTTPErrorMap(err))
			return
		}

		// return created match
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(matches)
		if err != nil {
			log.Printf("%s: %v", FetchAllErrCode, err)
			resp := &je.Response{
				Code:    FetchAllErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully fetched all matches for profile ID %s", session.ProfileID)
		return
	}
}
