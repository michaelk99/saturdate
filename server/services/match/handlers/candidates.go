package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/match"
)

const (
	// CandidatesErrCode error code
	CandidatesErrCode = "match.candidates.error"
	// CandidatesFetchErrCode error code
	CandidatesFetchErrCode = "match.candidates.fetch_error"
	// CandidatesBadDataCode bad data
	CandidatesBadDataCode = "match.candidates.bad_data"
)

// Candidates fetch candidates for client
func Candidates(v validator.Validator, ms match.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("match.candidates.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		r.ParseForm()
		lat := r.URL.Query().Get("lat")
		lon := r.URL.Query().Get("lon")

		latf, err := strconv.ParseFloat(lat, 64)
		if err != nil {
			resp := &je.Response{
				Code:    CandidatesBadDataCode,
				Message: CandidatesBadDataCode,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		lonf, err := strconv.ParseFloat(lon, 64)
		if err != nil {
			resp := &je.Response{
				Code:    CandidatesBadDataCode,
				Message: CandidatesBadDataCode,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		loc := match.Location{
			Latitude:  latf,
			Longitude: lonf,
		}

		// validate lat and lon
		ok, fieldErrors := v.Struct(loc)
		if !ok {
			resp := &je.Response{
				Code:       CandidatesBadDataCode,
				Message:    CandidatesBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// get profileID from token
		session := r.Context().Value("Session").(*token.Session)
		profID := session.ProfileID

		profs, err := ms.FetchCandidates(r.Context(), loc, profID)
		if err != nil {
			resp := &je.Response{
				Code:    CandidatesFetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, match.ServiceToHTTPErrorMap(err))
			return
		}

		// return profiles
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(profs)
		if err != nil {
			log.Printf("%s: %v", CandidatesErrCode, err)
			resp := &je.Response{
				Code:    CandidatesErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully fetched profile candidates for: %s", profID)
		return
	}
}
