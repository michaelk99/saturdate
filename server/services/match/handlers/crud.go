package handlers

import (
	"net/http"

	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/match"
	"path/filepath"
	"strings"
)

// CRUD forwards request based on http method
func CRUD(v validator.Validator, ms match.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		switch r.Method {
		case http.MethodGet:
			// check between fetch and fetch all within GET, since you cannot
			// differentiate http verbs between the two
			path := filepath.Clean(r.URL.Path)
			id := strings.TrimPrefix(path, "/api/v1/matches")
			if id == "" {
				FetchAll(ms).ServeHTTP(w, r)
				return
			}
			Fetch(ms).ServeHTTP(w, r)
			return
		case http.MethodPatch:
			Update(v, ms).ServeHTTP(w, r)
			return
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}
	}
}
