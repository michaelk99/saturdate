package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"path/filepath"
	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/match"
)

const (
	// UpdateErrCode code
	UpdateErrCode = "match.update.error"
	// UpdateExistsCode code
	UpdateExistsCode = "match.update.exists"
	// UpdateBadDataCode bad data
	UpdateBadDataCode = "match.update.bad_data"
)

// Update checks email against password and assigns a token if valid
func Update(v validator.Validator, s match.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support PATCH
		if r.Method != http.MethodPatch {
			log.Printf("match.update.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/matches/")

		if id == "" {
			log.Printf("match.update.invalid_id")
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		var m models.Match
		err := json.NewDecoder(r.Body).Decode(&m)

		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// set ID
		m.ID = id

		// validate match
		ok, fieldErrors := v.Struct(m)
		if !ok {
			resp := &je.Response{
				Code:       UpdateBadDataCode,
				Message:    UpdateBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// set the id to the id in the request
		mm, err := s.Update(r.Context(), m)
		if err != nil {
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, match.ServiceToHTTPErrorMap(err))
			return
		}

		// return created match
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(mm)
		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully updated match id %s", m.ID)
		return
	}
}
