package match_test

import (
	"context"
	"fmt"
	gomock "github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"gitlab.com/michaelk99/saturdate/server/pkg/client"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	"gitlab.com/michaelk99/saturdate/server/services/match"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"github.com/stretchr/testify/assert"
	// "sort"
	"testing"
)

func getContext() context.Context {
	ctx := context.Background()
	tk := &token.Session{
		ProfileID: uuid.New().String(),
	}
	ctx = context.WithValue(ctx, "Session", tk)
	ctx = context.WithValue(ctx, "Token", "some-jwt")
	return ctx
}

func getProfiles() []*profile.Profile {
	var p []*profile.Profile
	p = append(p, &profile.Profile{
		ID: "1",
	})
	p = append(p, &profile.Profile{
		ID: "2",
	})
	return p
}

func getGeoResponses() []*geo.GeoResponse {
	var g []*geo.GeoResponse
	g = append(g, &geo.GeoResponse{
		ProfileID: "1",
	})
	g = append(g, &geo.GeoResponse{
		ProfileID: "2",
	})
	return g
}

func TestFetchCandidates(t *testing.T) {
	var TestFetchCandidatesTT = []struct {
		name            string
		fetchCandidates func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should throw error if search fails",
			fetchCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				ctx := getContext()

				loc := match.Location{
					Latitude:  30.0123,
					Longitude: 29.999,
				}

				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				mockGeoClient.EXPECT().Search(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(
					nil, fmt.Errorf("Failed search"),
				)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FetchCandidates(ctx, loc, "profile-id")

				assert.NotNil(t, err)
				assert.Nil(t, profs)
				assert.Equal(t, "Failed search", err.Error())
			},
		}, {

			name: "should throw error if fetch profile fails",
			fetchCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				ctx := getContext()

				loc := match.Location{
					Latitude:  30.0123,
					Longitude: 29.999,
				}

				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				mockGeoClient.EXPECT().Search(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(
					getGeoResponses(), nil,
				)

				mockProfileClient.EXPECT().GetProfile(gomock.Any(), gomock.Any()).Return(
					nil, fmt.Errorf("Failed to get profile"),
				)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FetchCandidates(ctx, loc, "profile-id")

				assert.NotNil(t, err)
				assert.Nil(t, profs)
				assert.Equal(t, "FetchCandidates: failed to fetch profile: Failed to get profile", err.Error())
			},
		}, {
			name: "should throw error if filter profiles fails",
			fetchCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				ctx := getContext()

				loc := match.Location{
					Latitude:  30.0123,
					Longitude: 29.999,
				}

				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				mockGeoClient.EXPECT().Search(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(
					getGeoResponses(), nil,
				)

				mockProfileClient.EXPECT().GetProfile(gomock.Any(), gomock.Any()).Return(
					nil, nil,
				)

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return(
					nil, fmt.Errorf("Failed to filter profiles"),
				)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FetchCandidates(ctx, loc, "profile-id")

				assert.NotNil(t, err)
				assert.Nil(t, profs)
				assert.Equal(t, "FilterCandidates: failed to filter profiles: Failed to filter profiles", err.Error())
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestFetchCandidatesTT {
		tt.fetchCandidates(ctrl, t)
	}
}

func TestFilterCandidates(t *testing.T) {
	var TestFilterCandidatesTT = []struct {
		name             string
		filterCandidates func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should throw error if search fails",
			filterCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				prof := &profile.Profile{
					ID: "1",
				}

				geor := []*geo.GeoResponse{
					&geo.GeoResponse{
						ProfileID: "1",
					},
				}

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return([]interface{}{
					nil, fmt.Errorf("Failed search")}...)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FilterCandidates("", geor, prof)

				assert.NotNil(t, err)
				assert.Nil(t, profs)
				assert.Equal(t, "Failed search", err.Error())
			},
		}, {
			name: "should filter on age and preferencee sex [both]",
			filterCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				// age and preference sex both
				prof := &profile.Profile{
					ID: "1",
					Preferences: profile.Preferences{
						PreferenceMinAge: 30,
						PreferenceMaxAge: 40,
						PreferenceSex:    profile.PreferenceBoth,
					},
				}

				// age and preference sex both
				profs := []*profile.Profile{
					&profile.Profile{
						ID: "1",
					},
					&profile.Profile{
						ID:       "2",
						Birthday: "1985-01-02T00:00:00Z",
						Gender:   profile.Male,
					},
				}

				geor := []*geo.GeoResponse{
					&geo.GeoResponse{
						ProfileID: "1",
					},
					&geo.GeoResponse{
						ProfileID: "2",
					},
				}

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return([]interface{}{
					profs, nil}...)

				mockMatchStore.EXPECT().FetchAllBlacklist(prof.ID).Return([]interface{}{[]*models.Blacklist{}, nil}...)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FilterCandidates("", geor, prof)

				assert.NotNil(t, profs)
				assert.Nil(t, err)
				assert.Equal(t, len(profs), 1)
			},
		}, {
			name: "should filter on age and preferencee sex [female]",
			filterCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				// age and preference female filter
				prof := &profile.Profile{
					ID: "1",
					Preferences: profile.Preferences{
						PreferenceMinAge: 30,
						PreferenceMaxAge: 40,
						PreferenceSex:    profile.PreferenceFemale,
					},
				}

				// age and preference female filter
				profs := []*profile.Profile{
					&profile.Profile{
						ID: "1",
					},
					&profile.Profile{
						ID:       "2",
						Birthday: "1985-01-02T00:00:00Z",
						Gender:   profile.Female,
					},
				}

				geor := []*geo.GeoResponse{
					&geo.GeoResponse{
						ProfileID: "1",
					},
					&geo.GeoResponse{
						ProfileID: "2",
					},
				}

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return([]interface{}{
					profs, nil}...)

				mockMatchStore.EXPECT().FetchAllBlacklist(prof.ID).Return([]interface{}{[]*models.Blacklist{}, nil}...)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FilterCandidates("", geor, prof)

				assert.NotNil(t, profs)
				assert.Nil(t, err)
				assert.Equal(t, len(profs), 1)
			},
		}, {
			name: "should filter on age and preferencee sex [male]",
			filterCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				// age and preference male filter
				prof := &profile.Profile{
					ID: "1",
					Preferences: profile.Preferences{
						PreferenceMinAge: 30,
						PreferenceMaxAge: 40,
						PreferenceSex:    profile.PreferenceMale,
					},
				}

				// age and preference male filter
				profs := []*profile.Profile{
					&profile.Profile{
						ID: "1",
					},
					&profile.Profile{
						ID:       "2",
						Birthday: "1985-01-02T00:00:00Z",
						Gender:   profile.Male,
					},
				}

				geor := []*geo.GeoResponse{
					&geo.GeoResponse{
						ProfileID: "1",
					},
					&geo.GeoResponse{
						ProfileID: "2",
					},
				}

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return([]interface{}{
					profs, nil}...)

				mockMatchStore.EXPECT().FetchAllBlacklist(prof.ID).Return([]interface{}{[]*models.Blacklist{}, nil}...)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FilterCandidates("", geor, prof)

				assert.NotNil(t, profs)
				assert.Nil(t, err)
				assert.Equal(t, len(profs), 1)
			},
		}, {
			name: "should fail on age filter",
			filterCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				prof := &profile.Profile{
					ID: "1",
					Preferences: profile.Preferences{
						PreferenceMinAge: 30,
						PreferenceMaxAge: 40,
						PreferenceSex:    profile.PreferenceMale,
					},
				}

				profs := []*profile.Profile{
					&profile.Profile{
						ID: "1",
					},
					&profile.Profile{
						ID:       "2",
						Birthday: "1935-01-02T00:00:00Z",
						Gender:   profile.Male,
					},
					&profile.Profile{
						ID:       "3",
						Birthday: "1985-01-02T00:00:00Z",
						Gender:   profile.Male,
					},
				}

				geor := []*geo.GeoResponse{
					&geo.GeoResponse{
						ProfileID: "1",
					},
					&geo.GeoResponse{
						ProfileID: "2",
					},
					&geo.GeoResponse{
						ProfileID: "3",
					},
				}

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return([]interface{}{
					profs, nil}...)

				mockMatchStore.EXPECT().FetchAllBlacklist(prof.ID).Return([]interface{}{[]*models.Blacklist{}, nil}...)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FilterCandidates("", geor, prof)

				assert.NotNil(t, profs)
				assert.Nil(t, err)
				assert.Equal(t, len(profs), 1)
			},
		}, {
			name: "should pass on age filter",
			filterCandidates: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockMatchStore := match.NewMockMatchStore(ctrl)
				mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)
				mockGeoClient := geo.NewMockClient(ctrl)
				mockEventClient := client.NewMockEventClient(ctrl)

				// age filter passes
				prof := &profile.Profile{
					ID: "1",
					Preferences: profile.Preferences{
						PreferenceMinAge: 10,
						PreferenceMaxAge: 1000,
						PreferenceSex:    profile.PreferenceMale,
					},
				}

				// age filter passes
				profs := []*profile.Profile{
					&profile.Profile{
						ID: "1",
					},
					&profile.Profile{
						ID:       "2",
						Birthday: "1985-01-02T00:00:00Z",
						Gender:   profile.Male,
					},
					&profile.Profile{
						ID:       "3",
						Birthday: "1985-01-02T00:00:00Z",
						Gender:   profile.Male,
					},
				}

				// age filter passes
				geor := []*geo.GeoResponse{
					&geo.GeoResponse{
						ProfileID: "1",
					},
					&geo.GeoResponse{
						ProfileID: "2",
					},
					&geo.GeoResponse{
						ProfileID: "3",
					},
				}

				mockProfileClient.EXPECT().SearchProfiles(gomock.Any(), gomock.Any()).Return([]interface{}{
					profs, nil}...)

				mockMatchStore.EXPECT().FetchAllBlacklist(prof.ID).Return([]interface{}{[]*models.Blacklist{}, nil}...)

				// test service
				s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient, mockEventClient)
				profs, err := s.FilterCandidates("", geor, prof)

				assert.NotNil(t, profs)
				assert.Nil(t, err)
				assert.Equal(t, len(profs), 2)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestFilterCandidatesTT {
		tt.filterCandidates(ctrl, t)
	}
}

/*
var TestSwipeCandidateTT = []struct {
	swipeRequest                 match.SwipeReq
	ctx                          context.Context
	createBlacklistReturn        []interface{}
	cacheGetReturn               []interface{}
	cacheDeleteReturn            error
	createMatchMissedReturn      []interface{}
	cachePutReturn               []interface{}
	shouldBlacklistFail          bool
	shouldCacheGetFail           bool
	shouldCacheDeleteFail        bool
	shouldMatchMissedFail        bool
	shouldCachePutFail           bool
	shouldGetAndDeleteInRedis    bool
	shouldGetAndNotDeleteInRedis bool
	shouldPutAndCreateMatch      bool
	shouldPutAndNotCreateMatch   bool
}{
	{
		// 1
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  false,
		},
		getContext(),
		[]interface{}{
			nil, fmt.Errorf("Error creating blacklist"),
		},
		[]interface{}{
			1, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
	}, {
		// 2
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  false,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			-1, fmt.Errorf("Error getting field in cache"),
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
	}, {
		// 3
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  false,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		fmt.Errorf("Error deleting field in cache"),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		false,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
		false,
	}, {
		// 4
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  false,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		nil,
		[]interface{}{
			nil, fmt.Errorf("Error creating match missed in db"),
		},
		[]interface{}{
			1, nil,
		},
		false,
		false,
		false,
		true,
		false,
		false,
		false,
		false,
		false,
	}, {
		// 5
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  false,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			-1, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		false,
		false,
		false,
		false,
		false,
		false,
		// should get and should not delete from redis
		true,
		false,
		false,
	}, {
		// 6
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  false,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		false,
		false,
		false,
		false,
		false,
		// should get and should delete from redis
		true,
		false,
		false,
		false,
	}, {
		// 7. Decision, true. Blacklist fail
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  true,
		},
		getContext(),
		[]interface{}{
			nil, fmt.Errorf("Error creating blacklist"),
		},
		[]interface{}{
			-1, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		// blacklist fail
		true,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
	}, {
		// 8. Decision true, cache put fail
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  true,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			0, fmt.Errorf("Failed to insert field into cache"),
		},
		false,
		false,
		false,
		false,
		true,
		false,
		false,
		false,
		false,
	}, {
		// 9. Decision true, should put in redis and not create match
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  true,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			1, nil,
		},
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		// should put in redis and should not create match
		true,
	}, {
		// 10. Decision true, should put in redis and create match
		match.SwipeReq{
			ProfileID: uuid.New().String(),
			Decision:  true,
		},
		getContext(),
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			nil, nil,
		},
		nil,
		[]interface{}{
			nil, nil,
		},
		[]interface{}{
			2, nil,
		},
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		// should put in redis and create match
		true,
		false,
	},
}

func TestSwipeCandidate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// db, _, _ := sqlmock.New()

	for _, tt := range TestSwipeCandidateTT {
		// setup for testing
		mockMatchStore := match.NewMockMatchStore(ctrl)
		mockMatchStoreCache := match.NewMockMatchStoreCache(ctrl)
		mockProfileClient := profile.NewMockClient(ctrl)
		mockGeoClient := geo.NewMockClient(ctrl)

		session := tt.ctx.Value("Session").(*token.Session)
		str := []string{session.ProfileID, tt.swipeRequest.ProfileID}
		sort.Strings(str)
		field := fmt.Sprintf("%s:%s", str[0], str[1])

		// always have blacklist fetch pass for now
		mockMatchStore.EXPECT().FetchBlacklist(session.ProfileID, tt.swipeRequest.ProfileID).Return([]interface{}{nil, nil}...)

		mockMatchStore.EXPECT().DB().Return(db)

		// swipe left mocks
		mockMatchStore.EXPECT().CreateBlacklist(gomock.Any(), gomock.Any()).Return(tt.createBlacklistReturn...)

		if tt.shouldCacheGetFail {
			mockMatchStoreCache.EXPECT().Get(field).Return(tt.cacheGetReturn...)
		}

		if tt.shouldCacheDeleteFail {
			mockMatchStoreCache.EXPECT().Get(field).Return(tt.cacheGetReturn...)
			mockMatchStoreCache.EXPECT().Delete(field).Return(tt.cacheDeleteReturn)
		}

		if tt.shouldMatchMissedFail {
			mockMatchStoreCache.EXPECT().Get(field).Return(tt.cacheGetReturn...)
			mockMatchStoreCache.EXPECT().Delete(field).Return(tt.cacheDeleteReturn)
			mockMatchStore.EXPECT().CreateMatchMissed(gomock.Any(), gomock.Any()).Return(tt.createMatchMissedReturn...)
		}

		if tt.shouldCachePutFail {
			mockMatchStoreCache.EXPECT().Put(field).Return(tt.cachePutReturn...)
		}

		if tt.shouldGetAndNotDeleteInRedis {
			mockMatchStoreCache.EXPECT().Get(field).Return(tt.cacheGetReturn...)
		}

		if tt.shouldGetAndDeleteInRedis {
			mockMatchStoreCache.EXPECT().Get(field).Return(tt.cacheGetReturn...)
			mockMatchStoreCache.EXPECT().Delete(field).Return(tt.cacheDeleteReturn)
			mockMatchStore.EXPECT().CreateMatchMissed(gomock.Any(), gomock.Any()).Return(tt.createMatchMissedReturn...)
		}

		if tt.shouldPutAndNotCreateMatch {
			mockMatchStoreCache.EXPECT().Put(field).Return(tt.cachePutReturn...)
		}

		if tt.shouldPutAndCreateMatch {
			mockMatchStoreCache.EXPECT().Put(field).Return(tt.cachePutReturn...)
			mockMatchStoreCache.EXPECT().Delete(field).Return(tt.cacheDeleteReturn)
			mockMatchStore.EXPECT().Create(gomock.Any(), gomock.Any()).Return([]interface{}{
				nil, nil,
			}...)
		}

		s := match.NewService(mockMatchStore, mockMatchStoreCache, mockGeoClient, mockProfileClient)

		if tt.shouldBlacklistFail {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error creating blacklist")
			continue
		}

		if tt.shouldCacheGetFail {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error getting field in cache")
			continue
		}

		if tt.shouldCacheDeleteFail {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error deleting field in cache")
			continue
		}

		if tt.shouldMatchMissedFail {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error creating match missed in db")
			continue
		}

		if tt.shouldCachePutFail {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Failed to insert field into cache")
			continue
		}

		if tt.shouldGetAndNotDeleteInRedis {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.Nil(t, err)
			continue
		}

		if tt.shouldGetAndDeleteInRedis {
			err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
			assert.Nil(t, err)
			continue
		}

		err := s.SwipeCandidate(tt.ctx, tt.swipeRequest)
		assert.Nil(t, err)
		continue
	}
}
*/
