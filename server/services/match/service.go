package match

import (
	"context"
	"fmt"
	goage "github.com/bearbin/go-age"
	"github.com/google/uuid"
	"github.com/jinzhu/now"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"gitlab.com/michaelk99/saturdate/server/pkg/client"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/pkg/transaction"
	"gitlab.com/michaelk99/saturdate/server/pkg/utils"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"log"
	"sort"
	"time"
)

const (
	// radius used to find matches from the geo service
	radius            = 30
	isoLayoutBirthday = "2006-01-02T00:00:00Z"
)

// Service is a public interface for implementing our Match service
type Service interface {
	// CRUD endpoints
	Update(ctx context.Context, m models.Match) (*models.Match, error)
	Fetch(ID string, profileID string) (*models.Match, error)
	FetchAll(profileID string) ([]*models.Match, error)

	// match related endpoints
	FetchCandidates(ctx context.Context, loc Location, profileID string) ([]*profile.Profile, error)
	FilterCandidates(jwt string, resp []*geo.GeoResponse, p *profile.Profile) ([]*profile.Profile, error)
	SwipeCandidate(ctx context.Context, sr SwipeReq) (*models.Event, error)
}

type service struct {
	ds    MatchStore
	cache MatchStoreCache
	gc    geo.Client
	pc    profile.Client
	ec    client.EventClient
}

// NewService is a constructor for our Match service implementation
func NewService(ds MatchStore, cache MatchStoreCache, gc geo.Client, pc profile.Client, ec client.EventClient) Service {
	return &service{
		ds:    ds,
		cache: cache,
		gc:    gc,
		pc:    pc,
		ec:    ec,
	}
}

func (s *service) Update(ctx context.Context, m models.Match) (*models.Match, error) {
	session := ctx.Value("Session").(*token.Session)

	mm, err := s.ds.Fetch(m.ID, session.ProfileID)

	if err != nil {
		return nil, err
	}

	if mm == nil {
		return nil, fmt.Errorf("Match not found for ID, profile ID %s %s", m.ID, session.ProfileID)
	}

	// first, make sure profile owns match
	m.UpdatedAt = time.Now().Format(time.RFC3339)

	_, err = s.ds.Update(m)
	if err != nil {
		return nil, err
	}

	return &m, nil
}

func (s *service) Fetch(ID string, profileID string) (*models.Match, error) {
	m, err := s.ds.Fetch(ID, profileID)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func (s *service) FetchAll(profileID string) ([]*models.Match, error) {
	matches, err := s.ds.FetchAll(profileID)
	if err != nil {
		return nil, err
	}

	return matches, nil
}

func (s *service) FetchCandidates(ctx context.Context, loc Location, profileID string) ([]*profile.Profile, error) {
	token := ctx.Value("Token").(string)
	resp, err := s.gc.Search(token, loc.Latitude, loc.Longitude, radius)
	if err != nil {
		return nil, err
	}

	log.Printf("successfully searched profiles based on geo: %d\n", len(resp))

	// No profiles found (note: the user's profile will always be included in
	// the match result)
	if len(resp) <= 1 {
		return []*profile.Profile{}, nil
	}

	// !!! IMPORTANT !!!
	// no filtering is done on the profiles returned from `Search`. We must
	// filter profiles here based on current user's preferences

	// in order to filter out candidates, we'll need to fetch the profile of
	// the session user to understand what their prefs are
	prof, err := s.pc.GetProfile(token, profileID)
	if err != nil {
		return nil, fmt.Errorf("FetchCandidates: failed to fetch profile: %s", err)
	}

	log.Printf("successfully fetched logged in profile")

	profs, err := s.FilterCandidates(token, resp, prof)
	if err != nil {
		return nil, fmt.Errorf("FilterCandidates: failed to filter profiles: %s", err)
	}

	log.Printf("successfully filtered candidates: %d", len(profs))
	return profs, nil
}

func (s *service) FilterCandidates(jwt string, resp []*geo.GeoResponse, p *profile.Profile) ([]*profile.Profile, error) {
	// map to get each profile id
	var profIDs []string

	// map into profile IDs
	for _, geoResp := range resp {
		profIDs = append(profIDs, geoResp.ProfileID)
	}

	log.Printf("FilterCandidates: profIDs to search: %d", len(profIDs))

	// fetch all profiles in 1 call
	profs, err := s.pc.SearchProfiles(jwt, profIDs)
	if err != nil {
		return nil, err
	}

	log.Printf("FilterCandidates: successfully searched profiles: %d", len(profs))

	// load this profile's blacklist, use it to filter out profiles
	bl, err := s.ds.FetchAllBlacklist(p.ID)
	if err != nil {
		return nil, err
	}

	log.Printf("FilterCandidates: successfully fetched blacklist for logged in user")

	var results []*profile.Profile
	// filter profiles based on preferences
	for _, prof := range profs {
		// dont include self in results
		if p.ID == prof.ID {
			log.Printf("Filter 1: excluding %s", prof.ID)
			continue
		}

		// don't include if in blacklist
		ok := true
		for _, b := range bl {
			if prof.ID == b.ProfileIDBlacklisted {
				ok = false
				break
			}
		}
		if !ok {
			log.Printf("Filter 2: excluding %s", prof.ID)
			continue
		}

		t, _ := time.Parse(isoLayoutBirthday, prof.Birthday)
		age := goage.Age(t)

		// do not include if age is not within boundary
		if age < p.PreferenceMinAge || age > p.PreferenceMaxAge {
			log.Printf("Filter 3: excluding %s", prof.ID)
			continue
		}

		// wants males
		if p.PreferenceSex == profile.PreferenceMale && prof.Gender != profile.Male {
			log.Printf("Filter 4: excluding %s", prof.ID)
			continue
		}

		// wants females
		if p.PreferenceSex == profile.PreferenceFemale && prof.Gender != profile.Female {
			log.Printf("Filter 5: excluding %s", prof.ID)
			continue
		}

		// TODO look at other preferences in the future like religion,
		// neighborhood distance, etc
		results = append(results, prof)
	}

	return results, nil
}

func (s *service) SwipeCandidate(ctx context.Context, sr SwipeReq) (*models.Event, error) {
	session := ctx.Value("Session").(*token.Session)
	tkn := ctx.Value("Token").(string)

	// declared outside the transaction block to be populated in event
	var matchID string
	shouldCreateEvent := false

	// wrap in a transaction
	fn := func(tx *sqlx.Tx) error {
		// redis field: profile ids sorted alphabetically
		str := []string{session.ProfileID, sr.ProfileID}
		sort.Strings(str)
		key := fmt.Sprintf("%s:%s", str[0], str[1])

		// decision = false, swipe left
		if !*sr.Decision {
			// make sure blacklist doesn't exist
			bl, _ := s.ds.FetchBlacklist(session.ProfileID, sr.ProfileID)
			if bl != nil {
				return fmt.Errorf("Blacklist already exists for %s and %s", session.ProfileID, sr.ProfileID)
			}

			// add to user's blacklist (to not see again in near future)
			_, err := s.ds.CreateBlacklist(tx, models.Blacklist{
				ID:                   uuid.New().String(),
				ProfileID:            session.ProfileID,
				ProfileIDBlacklisted: sr.ProfileID,
				// 2 month expiration
				ExpiresAt: time.Now().AddDate(0, 2, 0).Format(time.RFC3339),
				CreatedAt: time.Now().Format(time.RFC3339),
				UpdatedAt: time.Now().Format(time.RFC3339),
			})

			if err != nil {
				return err
			}

			log.Printf("successfully created blacklist for logged in user: %s\n", session.ProfileID)

			// look into redis to see if other user swiped right. If they did,
			// delete the entry in redis and create a missed match [record-keeping],
			// otherwise do nothing
			val, err := s.cache.Get(key)
			if err != nil {
				return err
			}

			log.Printf("successfully checked redis with key: %s", key)

			// delete entry in redis if exists
			// create record in `matches_missed` for record keeping
			if val != -1 {
				err = s.cache.Delete(key)
				if err != nil {
					return err
				}

				log.Printf("successfully deleted key from redis: %s", key)

				_, err := s.ds.CreateMatchMissed(tx, models.Match{
					ID: uuid.New().String(),
					Profiles: pq.StringArray{
						session.ProfileID,
						sr.ProfileID,
					},
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				})
				if err != nil {
					return err
				}

				log.Printf("successfully created match_missed in db: %s %s", session.ProfileID, sr.ProfileID)
			}
			return nil
		}

		// make sure blacklist doesn't exist
		bl, _ := s.ds.FetchBlacklist(session.ProfileID, sr.ProfileID)
		if bl != nil {
			return fmt.Errorf("Blacklist already exists for %s and %s", session.ProfileID, sr.ProfileID)
		}

		log.Printf("successfully fetched blacklist for logged in user: %s\n", session.ProfileID)

		// decision = true, swipe right
		// add to user's blacklist (to not see again in near future)
		_, err := s.ds.CreateBlacklist(tx, models.Blacklist{
			ID:                   uuid.New().String(),
			ProfileID:            session.ProfileID,
			ProfileIDBlacklisted: sr.ProfileID,
			// 2 month expiration
			ExpiresAt: time.Now().AddDate(0, 2, 0).Format(time.RFC3339),
			CreatedAt: time.Now().Format(time.RFC3339),
			UpdatedAt: time.Now().Format(time.RFC3339),
		})

		if err != nil {
			return err
		}

		log.Printf("successfully created blacklist entry for profile ID: %s\n", session.ProfileID)

		// add to redis
		// remember: field must be alpha sorted and formatted like:
		// prof_id_1:prof_id_2
		// Put function uses IncrBy
		val, err := s.cache.Put(key)
		if err != nil {
			return err
		}

		log.Printf("successfully inserted entry into redis (key/value): %s / %d\n", key, val)

		// if val is < 2, no match yet, nothing to do
		// if value is > 2, somehow it got into a weird state so ignore for now
		if val != 2 {
			return nil
		}

		// !!! IMPORTANT !!!
		// this must be set to true so we know if we need to create the event
		// or not
		shouldCreateEvent = true
		matchID = uuid.New().String()

		// Match! Follow through with completing the match
		_, err = s.ds.Create(tx, models.Match{
			ID: matchID,
			Profiles: pq.StringArray{
				session.ProfileID,
				sr.ProfileID,
			},
			Notified:  []string{},
			CreatedAt: time.Now().Format(time.RFC3339),
			UpdatedAt: time.Now().Format(time.RFC3339),
		})
		if err != nil {
			return err
		}

		log.Printf("successfully created match record in DB for profileID: %s\n", session.ProfileID)

		// once the match is created in the db, delete the key from redis
		err = s.cache.Delete(key)
		if err != nil {
			// fail gracefully. Not deleting from redis is an `ok` problem to
			// have and should not completely unravel this match
			// skip
			// TODO mark as error
		}

		log.Printf("successfully deleted key from redis: %s\n", key)
		return nil
	}

	err := transaction.Transact(s.ds.DB(), fn)
	if err != nil {
		return nil, err
	}

	// don't need to create the event, return
	if !shouldCreateEvent {
		return nil, nil
	}

	// at this point we need to create the event and send back to the client so
	// they know if the `swipe` was a match or not. The `event` should only be
	// returned if the `StartDate` is for the current Saturday of the week
	// !!! IMPORTANT !!!  This was not included in the DB transaction because
	// creating the event relies on the `match` record being committed in the
	// DB.  If the event creation fails for whatever reason, we cannot roll
	// back the previous transaction. Instead, notify admins about event
	// failure and then create the event manually in the db [for now].
	ee, err := s.ec.CreateEvent(tkn, &models.Event{
		MatchID: matchID,
	})

	if err != nil {
		log.Printf("failed to create event record: %s", err)
		// TODO notify admins somehow about this failure
		return nil, nil
	}

	log.Printf("successfully created event record with matchID: %s", matchID)

	// check the startDate of the event
	y1, m1, d1 := now.EndOfWeek().Date()
	t := utils.GetDateTime(ee.StartDate)
	y2, m2, d2 := t.Date()

	// return event if the start date is scheduled for this Saturday
	if y1 == y2 && m1 == m2 && d1 == d2 {
		return ee, nil
	}

	// otherwise if event date is not this Saturday, return nil `event`
	return nil, nil
}
