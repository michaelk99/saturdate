package match

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
)

// MatchStore postgres interface
type MatchStore interface {
	// CRUD endpoints
	Create(*sqlx.Tx, models.Match) (*models.Match, error)
	FetchAll(profileID string) ([]*models.Match, error)
	Fetch(ID string, profileID string) (*models.Match, error)
	Update(m models.Match) (*models.Match, error)

	// match related endpoints
	CreateMatchMissed(*sqlx.Tx, models.Match) (*models.Match, error)
	CreateBlacklist(*sqlx.Tx, models.Blacklist) (*models.Blacklist, error)
	FetchMatchByProfileID(profileIDOne string, profileIDTwo string) (*models.Match, error)
	FetchAllBlacklist(profileID string) ([]*models.Blacklist, error)
	FetchBlacklist(profileID string, profileIDInvalid string) (*models.Blacklist, error)

	// For transaction purposes only
	DB() *sqlx.DB
}

// MatchStoreCache redis interface
type MatchStoreCache interface {
	Get(key string) (int, error)
	Put(key string) (int, error)
	Delete(key string) error
}
