-- Connect to our database
\connect saturdate

-- Create blacklist table
CREATE TABLE blacklist (
    id varchar(255) PRIMARY KEY,
    profile_id varchar(255) NOT NULL,
    profile_id_blacklisted varchar(255) NOT NULL,
    expires_at timestamp NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
