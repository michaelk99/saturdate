package postgres

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/services/match"
)

// queries are written to use sqlx.NamedExec() method. this method maps "db" struct tags with
// the : prefixed names in the values parameter
const (
	// CRUD queries
	CreateMatchQuery     = `INSERT INTO matches (id, profiles, created_at, updated_at) VALUES (:id, :profiles, :created_at, :updated_at);`
	UpdateMatchQuery     = `UPDATE matches SET notified = :notified, updated_at = :updated_at WHERE id = :id`
	FetchAllMatchesQuery = `SELECT * from matches WHERE $1 = ANY (profiles)`
	FetchMatchQuery      = `SELECT * FROM matches WHERE id = $1 AND $2 = ANY (profiles)`

	// match queries
	CreateMatchMissedQuery     = `INSERT INTO matches_missed (id, profiles, created_at, updated_at) VALUES (:id, :profiles, :created_at, :updated_at);`
	CreateBlacklistQuery       = `INSERT INTO blacklist (id, profile_id, profile_id_blacklisted, expires_at, created_at, updated_at) VALUES (:id, :profile_id, :profile_id_blacklisted, :expires_at, :created_at, :updated_at)`
	FetchMatchByProfileIDQuery = `SELECT * from matches WHERE $1 = ANY (profiles) AND $2 = ANY (profiles)`
	FetchAllBlacklistQuery     = `SELECT * FROM blacklist WHERE profile_id = $1`
	FetchBlacklistQuery        = `SELECT * FROM blacklist WHERE profile_id = $1 and profile_id_blacklisted = $2`
)

// matchStore is a private implementation of the models.MatchStore interface
type matchStore struct {
	// a sqlx database object
	db *sqlx.DB
}

// NewMatchStore returns a postgres db implementation of the models.MatchStore interface
func NewMatchStore(db *sqlx.DB) match.MatchStore {
	return &matchStore{
		db: db,
	}
}

func (s *matchStore) DB() *sqlx.DB {
	return s.db
}

// Create creates a match in postgres db
func (s *matchStore) Create(tx *sqlx.Tx, m models.Match) (*models.Match, error) {
	row, err := tx.NamedExec(CreateMatchQuery, m)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &m, nil
}

func (s *matchStore) Update(m models.Match) (*models.Match, error) {
	// perform update on values we allow to change
	row, err := s.db.NamedExec(UpdateMatchQuery, m)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &m, nil
}

// Fetch looks for a specific match in the db
func (s *matchStore) Fetch(ID string, profileID string) (*models.Match, error) {
	var m models.Match

	err := s.db.Get(&m, FetchMatchQuery, ID, profileID)
	if err != nil {
		return nil, err
	}

	return &m, nil
}

// FetchAll matches that belong to profile
func (s *matchStore) FetchAll(profileID string) ([]*models.Match, error) {
	var m []*models.Match

	err := s.db.Select(&m, FetchAllMatchesQuery, profileID)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// CreateMatchMissed creates a missed match in postgres db
func (s *matchStore) CreateMatchMissed(tx *sqlx.Tx, m models.Match) (*models.Match, error) {
	row, err := tx.NamedExec(CreateMatchMissedQuery, m)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &m, nil
}

// CreateBlacklist creates a blacklist record in postgres db
func (s *matchStore) CreateBlacklist(tx *sqlx.Tx, bl models.Blacklist) (*models.Blacklist, error) {
	row, err := tx.NamedExec(CreateBlacklistQuery, bl)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &bl, nil
}

// FetchMatchByProfile looks for a specific match in the db
func (s *matchStore) FetchMatchByProfileID(profileIDOne string, profileIDTwo string) (*models.Match, error) {
	var m models.Match

	err := s.db.Get(&m, FetchMatchByProfileIDQuery, profileIDOne, profileIDTwo)
	if err != nil {
		return nil, err
	}

	return &m, nil
}

func (s *matchStore) FetchAllBlacklist(profileID string) ([]*models.Blacklist, error) {
	var bl []*models.Blacklist

	err := s.db.Select(&bl, FetchAllBlacklistQuery, profileID)
	if err != nil {
		return nil, err
	}

	return bl, nil
}

// FetchBlacklist looks for a specific blacklist in the db
func (s *matchStore) FetchBlacklist(profileID string, profileIDInvalid string) (*models.Blacklist, error) {
	var bl models.Blacklist

	err := s.db.Get(&bl, FetchBlacklistQuery, profileID, profileIDInvalid)
	if err != nil {
		return nil, err
	}

	return &bl, nil
}
