// +build integration

package postgres_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/michaelk99/saturdate/server/services/match"
	"gitlab.com/michaelk99/saturdate/server/services/match/postgres"
)

const (
	DeleteMatchQuery = `DELETE FROM matches;`
	CreateMatchQuery = `INSERT INTO matches (id, profiles, created_at, updated_at) VALUES (:id, :profiles, :created_at, :updated_at);`
	FetchMatchQuery  = `SELECT * FROM matches WHERE id = $1;`
)

func Setup(t *testing.T) (*sqlx.DB, match.MatchStore, func(), func()) {
	connString := "host=localhost user=postgres dbname=saturdate password='' sslmode=disable"
	if os.Getenv("POSTGRES_CONN_STRING") != "" {
		connString = os.Getenv("POSTGRES_CONN_STRING")
	}
	db, err := sqlx.Connect("postgres", connString)
	if err != nil {
		t.Fatalf("setup: could not open connection to db: %s", err)
	}

	ds := postgres.NewMatchStore(db)

	return db, ds, func() {
			defer db.Close()
			_, err := db.Exec(DeleteMatchQuery)
			if err != nil {
				t.Fatalf("failed to delete matches: %v. manual cleanup is necessary", err)
			}
		},
		func() {
			_, err := db.Exec(DeleteMatchQuery)
			if err != nil {
				t.Fatalf("failed to delete matches: %v. manual cleanup is necessary", err)
			}
		}
}

var MatchTestingTable = []*match.Match{
	&match.Match{
		ID: uuid.New().String(),
		Profiles: pq.StringArray{
			uuid.New().String(),
			uuid.New().String(),
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
	&match.Match{
		ID: uuid.New().String(),
		Profiles: pq.StringArray{
			uuid.New().String(),
			uuid.New().String(),
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
}

// Test the creation of an match struct into postgres DB
func TestCreateMatch(t *testing.T) {
	db, ds, teardown, _ := Setup(t)
	defer teardown()

	for _, mtt := range MatchTestingTable {
		tx, _ := db.Beginx()
		m, err := ds.Create(tx, *mtt)
		if err != nil {
			t.Fatalf("failed to insert match into DB: %v", err)
		}
		tx.Commit()
		assert.Equal(t, mtt.ID, m.ID)
	}
}

// Test fetch match
func TestFetchMatch(t *testing.T) {
	db, ds, teardown, _ := Setup(t)
	defer teardown()

	for index, mtt := range MatchTestingTable {
		// every even insert into db
		if index%2 == 0 {
			_, err := db.NamedExec(CreateMatchQuery, mtt)
			assert.Nil(t, err)
		}

		m, err := ds.FetchMatchByProfileID(mtt.Profiles[0], mtt.Profiles[1])
		// if even should fetch match in db
		if index%2 == 0 {
			assert.Nil(t, err)
			assert.Equal(t, mtt.ID, m.ID)
			assert.Equal(t, 2, len(m.Profiles))
		} else {
			// should not find match in db
			assert.NotNil(t, err)
			assert.Nil(t, m)
		}
	}
}
