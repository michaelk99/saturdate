-- Connect to our database
\connect saturdate

-- Create matches table
CREATE TABLE matches (
    id varchar(255) PRIMARY KEY,
    profiles text[] NOT NULL,
    notified text[],
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
