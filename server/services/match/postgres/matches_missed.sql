-- Connect to our database
\connect saturdate

-- Create matches_missed table
CREATE TABLE matches_missed (
    id varchar(255) PRIMARY KEY,
    profiles text[] NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
