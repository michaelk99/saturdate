package match

import (
	"net/http"
)

// ErrMatchCreate is returned when match cannot be created
type ErrMatchCreate struct {
	msg error
}

func (e ErrMatchCreate) Error() string {
	return "could not create match"
}

// ErrMatchExists is returned when match already exists
type ErrMatchExists struct {
	msg error
}

func (e ErrMatchExists) Error() string {
	return "match already exists"
}

// ServiceToHTTPErrorMap maps the matchs service's errors to http
func ServiceToHTTPErrorMap(err error) (code int) {
	switch err.(type) {
	case ErrMatchCreate:
		return http.StatusConflict
	case ErrMatchExists:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
