// +build integration

package redis_test

import (
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/michaelk99/saturdate/server/services/match"
	matchstore "gitlab.com/michaelk99/saturdate/server/services/match/redis"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var Client *redis.Client

const (
	key = "match"
)

func SetUpClient(t *testing.T) (*redis.Client, error) {
	if Client != nil {
		return Client, nil
	}

	Client = redis.NewClient(&redis.Options{
		Addr:     "0.0.0.0:6379",
		Password: "",
		DB:       0,
	})

	_, err := Client.Ping().Result()
	if err != nil {
		fmt.Printf("Error connecting to redis client %s", err)
	}

	return Client, nil
}

func Setup(t *testing.T) (match.MatchStoreCache, func()) {
	_, err := SetUpClient(t)
	if err != nil {
		t.Fatalf("setup: could not create redis connection: %s", err)
	}
	ds := matchstore.NewMatchStoreCache(Client)

	return ds, func() {
		_ = Client.FlushAll()
	}
}

var TestGetTT = []struct {
	key        string
	shouldFind bool
}{
	{
		"profID1:profID2",
		true,
	}, {
		"profID2:profID3",
		false,
	},
}

// Test getting fields from store
func TestGet(t *testing.T) {
	ds, flushall := Setup(t)
	defer flushall()

	for _, tt := range TestGetTT {
		if tt.shouldFind {
			// insert into redis
			cmd := Client.Set(tt.key, 1, time.Duration(1000)*time.Second)
			if cmd.Err() != nil {
				t.Fatal(cmd.Err())
			}
		}

		val, err := ds.Get(tt.key)

		if tt.shouldFind {
			assert.Nil(t, err)
			assert.Equal(t, val, 1)
			continue
		}

		assert.NotNil(t, err)
		assert.Equal(t, val, -1)
	}
}

var TestPutTT = []struct {
	key      string
	finalVal int
}{
	{
		"profID1:profID2",
		1,
	}, {
		"profID2:profID3",
		2,
	},
}

// Test put fields in store
func TestPut(t *testing.T) {
	ds, flushall := Setup(t)
	defer flushall()

	for _, tt := range TestPutTT {
		if tt.finalVal == 2 {
			// insert into redis
			cmd := Client.Set(tt.key, 1, time.Duration(1000)*time.Second)
			if cmd.Err() != nil {
				t.Fatal(cmd.Err())
			}
		}

		val, err := ds.Put(tt.key)

		if tt.finalVal == 2 {
			assert.Nil(t, err)
			assert.Equal(t, 2, val)

			// assert ttl
			cmd := Client.TTL(tt.key)
			assert.Equal(t, cmd.Val(), time.Duration(2628003)*time.Second)
			continue
		}

		assert.Nil(t, err)
		assert.Equal(t, 1, val)
	}
}

var TestDeleteTT = []struct {
	key   string
	value string
}{
	{
		"profID1:profID2",
		"val",
	}, {
		"profID2:profID3",
		"val2",
	},
}

// Test deleteing fields from redis
func TestDelete(t *testing.T) {
	ds, flushall := Setup(t)
	defer flushall()

	for _, tt := range TestDeleteTT {
		// insert into redis
		cmd := Client.Set(tt.key, 1, time.Duration(1000)*time.Second)
		if cmd.Err() != nil {
			t.Fatal(cmd.Err())
		}

		err := ds.Delete(tt.key)

		// should always delete
		assert.Nil(t, err)

		// Get from redis and assert value
		cmd2 := Client.Get(key)
		assert.NotNil(t, cmd2.Err())
	}
}
