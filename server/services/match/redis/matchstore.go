package redis

import (
	"github.com/go-redis/redis"
	"gitlab.com/michaelk99/saturdate/server/services/match"
	"strconv"
	"time"
)

const (
	// TTL is 1 month in seconds
	keyTTL = time.Duration(2628003) * time.Second
)

// matchStoreCache is a private implementation of the match.MatchStoreCache
// interface
type matchStoreCache struct {
	client *redis.Client
}

// NewMatchStoreCache returns a redis implementation of the
// match.MatchStoreCache interface
func NewMatchStoreCache(client *redis.Client) match.MatchStoreCache {
	return &matchStoreCache{
		client: client,
	}
}

func (g *matchStoreCache) Get(key string) (int, error) {
	cmd := g.client.Get(key)

	if cmd.Err() != nil {
		if cmd.Err() == redis.Nil {
			return -1, nil
		}
		return -1, cmd.Err()
	}

	// convert to int, should only be int
	val := cmd.Val()
	vali, err := strconv.ParseInt(val, 10, 32)

	if err != nil {
		return -1, err
	}

	return int(vali), nil
}

func (g *matchStoreCache) Delete(key string) error {
	cmd := g.client.Del(key)
	return cmd.Err()
}

func (g *matchStoreCache) Put(key string) (int, error) {
	// incr by 1
	cmd := g.client.IncrBy(key, 1)
	if cmd.Err() != nil {
		return -1, cmd.Err()
	}

	// set ttl
	durcmd := g.client.Expire(key, keyTTL)
	if durcmd.Err() != nil {
		return -1, durcmd.Err()
	}

	return int(cmd.Val()), nil
}
