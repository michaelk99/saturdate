package event

// Feedback is retrieved record from the db
type Feedback struct {
	ID             string `json:"id" db:"id"`
	EventID        string `validate:"required" json:"event_id" db:"event_id"`
	ProfileID      string `validate:"required" json:"profile_id" db:"profile_id"`
	ProfileIDMatch string `validate:"required" json:"profile_id_match" db:"profile_id_match"`
	// although I would like `did_show` to be validate:required, if passed in
	// as false the validator logic treats it as not present, very annoying :/
	DidShow   bool   `json:"did_show" db:"did_show"`
	BarRating int    `validate:"required" json:"bar_rating" db:"bar_rating"`
	Comment   string `json:"comment" db:"comment"`
	CreatedAt string `json:"created_at" db:"created_at"`
	UpdatedAt string `json:"updated_at" db:"updated_at"`
}
