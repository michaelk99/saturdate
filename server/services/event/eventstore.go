package event

import (
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
)

// EventStore postgres interface
type EventStore interface {
	Create(models.Event) (*models.Event, error)
	Update(e models.Event) (*models.Event, error)
	Fetch(ID string, profileID string) (*models.Event, error)
	FetchByProfiles(profiles []string) (*models.Event, error)
	FetchByMatchID(matchID string) (*models.Event, error)
	FetchAll(profileID string) ([]*models.Event, error)

	// Fetching bars are tightly coupled with creating an event,
	// so this function is part of the eventstore (instead of having a
	// separate `BarStore`)
	FetchBars(lat float64, lon float64) ([]*models.Bar, error)

	// Creating a feedback record is also coupled to an event, so it's part
	// of the `EventStore` (instead of having a separate `FeedbackStore`
	CreateFeedback(f Feedback) (*Feedback, error)
	FetchFeedbackByProfileIDs(profileID string, profileIDMatch string) (*Feedback, error)
}
