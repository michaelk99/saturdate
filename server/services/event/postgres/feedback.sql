-- Connect to our database
\connect saturdate

-- Create feedback table
CREATE TABLE feedback (
    id varchar(255) PRIMARY KEY,
    event_id varchar(255) NOT NULL,
    profile_id varchar(255) NOT NULL,
    profile_id_match varchar(255) NOT NULL,
    did_show boolean DEFAULT false,
    bar_rating smallint DEFAULT 0,
    comment text,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
