package postgres

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/services/event"
	"math/rand"
)

// queries are written to use sqlx.NamedExec() method. this method maps "db" struct tags with
// the : prefixed names in the values parameter
const (
	// Events query
	CreateEventQuery          = `INSERT INTO events (id, profiles, match_id, notified, turn, bars, state, start_date, start_time, created_at, updated_at) VALUES (:id, :profiles, :match_id, :notified, :turn, :bars, :state, :start_date, :start_time, :created_at, :updated_at);`
	UpdateEventQuery          = `UPDATE events SET notified = :notified, turn = :turn, bars = :bars, state = :state, updated_at = :updated_at WHERE id = :id`
	FetchEventQuery           = `SELECT * FROM events WHERE id = $1 AND $2 = ANY (profiles)`
	FetchEventByProfilesQuery = `SELECT * FROM events WHERE $1 = ANY (profiles) AND $2 = ANY (profiles)`
	FetchEventByMatchIDQuery  = `SELECT * FROM events WHERE match_id = $1`
	FetchAllEventsQuery       = `SELECT * from events WHERE $1 = ANY (profiles) ORDER BY start_date desc`

	// Feedback query
	CreateFeedbackQuery          = `INSERT INTO feedback (id, event_id, profile_id, profile_id_match, did_show, bar_rating, comment, created_at, updated_at) VALUES (:id, :event_id, :profile_id, :profile_id_match, :did_show, :bar_rating, :comment, :created_at, :updated_at)`
	FetchFeedbackByProfilesQuery = `SELECT * FROM feedback WHERE profile_id = $1 AND profile_id_match = $2`

	// Bars query
	FetchBarsQuery = `SELECT *, point($2, $1) <@> point(longitude, latitude)::point AS bar_distance FROM bars WHERE (point($2, $1) <@> point(longitude, latitude)) < $3 ORDER BY bar_distance`
)

// eventStore is a private implementation of the models.EventStore interface
type eventStore struct {
	// a sqlx database object
	db *sqlx.DB
}

// NewEventStore returns a postgres db implementation of the models.EventStore interface
func NewEventStore(db *sqlx.DB) event.EventStore {
	return &eventStore{
		db: db,
	}
}

// Create creates a event in postgres db
func (s *eventStore) Create(e models.Event) (*models.Event, error) {
	row, err := s.db.NamedExec(CreateEventQuery, e)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &e, nil
}

func (s *eventStore) Update(e models.Event) (*models.Event, error) {
	// perform update on values we allow to change
	row, err := s.db.NamedExec(UpdateEventQuery, e)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &e, nil
}

// Fetch looks for a specific event in the db
func (s *eventStore) Fetch(ID string, profileID string) (*models.Event, error) {
	var e models.Event

	err := s.db.Get(&e, FetchEventQuery, ID, profileID)
	if err != nil {
		return nil, err
	}

	return &e, nil
}

// FetchByProfiles fetch event by profile array combination
func (s *eventStore) FetchByProfiles(profiles []string) (*models.Event, error) {
	var e models.Event

	err := s.db.Get(&e, FetchEventByProfilesQuery, profiles[0], profiles[1])
	if err != nil {
		return nil, err
	}

	return &e, nil
}

// FetchByMatchID fetch event by match id
func (s *eventStore) FetchByMatchID(matchID string) (*models.Event, error) {
	var e models.Event

	err := s.db.Get(&e, FetchEventByMatchIDQuery, matchID)
	if err != nil {
		return nil, err
	}

	return &e, nil
}

// FetchAll events that belong to profile
func (s *eventStore) FetchAll(profileID string) ([]*models.Event, error) {
	var e []*models.Event

	err := s.db.Select(&e, FetchAllEventsQuery, profileID)
	if err != nil {
		return nil, err
	}

	return e, nil
}

// FetchBars must return a total of 5 bars per our spec. If 5 bars are not
// found, query again with a larger radius until we find a list of at least 5
// bars
func (s *eventStore) FetchBars(lat float64, lon float64) ([]*models.Bar, error) {
	var b []*models.Bar

	// radius in miles [1,2,3,4,5,13,21,34,100]
	rads := []int{1, 2, 3, 4, 5, 13, 21, 34, 100}
	for _, rad := range rads {
		err := s.db.Select(&b, FetchBarsQuery, lat, lon, rad)
		if err != nil {
			return nil, err
		}
		// if we have at least 5 bars in the result set, break
		if len(b) >= 5 {
			break
		}
	}

	// randomize the results and pick the first 5 records
	// Fisher-Yates shuffle http://marcelom.github.io/2013/06/07/goshuffle.html
	for i := range b {
		j := rand.Intn(i + 1)
		b[i], b[j] = b[j], b[i]
	}

	return b[0:5], nil
}

func (s *eventStore) CreateFeedback(f event.Feedback) (*event.Feedback, error) {
	row, err := s.db.NamedExec(CreateFeedbackQuery, f)
	if err != nil {
		return nil, err
	}

	i, err := row.RowsAffected()
	switch {
	case i <= 0:
		return nil, fmt.Errorf("%d rows affected by update", i)
	case err != nil:
		return nil, err
	}

	return &f, nil
}

// FetchFeedback looks for a specific feedback record in the db
func (s *eventStore) FetchFeedbackByProfileIDs(profileID string, profileIDMatch string) (*event.Feedback, error) {
	var f event.Feedback

	err := s.db.Get(&f, FetchFeedbackByProfilesQuery, profileID, profileIDMatch)
	if err != nil {
		return nil, err
	}

	return &f, nil
}
