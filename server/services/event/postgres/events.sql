-- Connect to our database
\connect saturdate

-- Create events table
CREATE TABLE events (
    id varchar(255) PRIMARY KEY,
    match_id varchar(255) NOT NULL,
    profiles text[] NOT NULL,
    notified text[],
    turn varchar(255),
    state int,
    bars json,
    start_date timestamp NOT NULL,
    start_time time NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
