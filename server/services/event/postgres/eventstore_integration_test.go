// +build integration

package postgres_test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/michaelk99/saturdate/server/services/event"
	"gitlab.com/michaelk99/saturdate/server/services/event/postgres"
)

const (
	DeleteEventQuery = `DELETE FROM events;`
	CreateEventQuery = `INSERT INTO events (id, profiles, match_id, start_date, start_time, state, created_at, updated_at) VALUES (:id, :profiles, :match_id, :start_date, :start_time, :state, :created_at, :updated_at);`
	FetchEventQuery  = `SELECT * FROM events WHERE id = $1;`
)

func Setup(t *testing.T) (*sqlx.DB, event.EventStore, func(), func()) {
	connString := "host=localhost user=postgres dbname=saturdate password='' sslmode=disable"
	if os.Getenv("POSTGRES_CONN_STRING") != "" {
		connString = os.Getenv("POSTGRES_CONN_STRING")
	}
	db, err := sqlx.Connect("postgres", connString)
	if err != nil {
		t.Fatalf("setup: could not open connection to db: %s", err)
	}

	ds := postgres.NewEventStore(db)

	return db, ds, func() {
			defer db.Close()
			_, err := db.Exec(DeleteEventQuery)
			if err != nil {
				t.Fatalf("failed to delete events: %v. manual cleanup is necessary", err)
			}
		},
		func() {
			_, err := db.Exec(DeleteEventQuery)
			if err != nil {
				t.Fatalf("failed to delete events: %v. manual cleanup is necessary", err)
			}
		}
}

var EventTestingTable = []*event.Event{
	&event.Event{
		ID:        uuid.New().String(),
		MatchID:   uuid.New().String(),
		StartDate: time.Now().Format(time.RFC3339),
		StartTime: "8:00pm",
		State:     event.ProfileFirst,
		Profiles: pq.StringArray{
			uuid.New().String(),
			uuid.New().String(),
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
	&event.Event{
		ID:        uuid.New().String(),
		MatchID:   uuid.New().String(),
		StartDate: time.Now().Format(time.RFC3339),
		StartTime: "8:00pm",
		State:     event.ProfileFirst,
		Profiles: pq.StringArray{
			uuid.New().String(),
			uuid.New().String(),
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
	&event.Event{
		ID:        uuid.New().String(),
		MatchID:   uuid.New().String(),
		StartDate: time.Now().Format(time.RFC3339),
		StartTime: "8:00pm",
		State:     event.ProfileFirst,
		Profiles: pq.StringArray{
			"static-profile-id",
			uuid.New().String(),
		},
		CreatedAt: time.Now().Format(time.RFC3339),
		UpdatedAt: time.Now().Format(time.RFC3339),
	},
}

// Test the creation of an event struct into postgres DB
func TestCreateEvent(t *testing.T) {
	_, ds, teardown, _ := Setup(t)
	defer teardown()

	for _, ett := range EventTestingTable {
		e, err := ds.Create(*ett)
		if err != nil {
			t.Fatalf("failed to insert event into DB: %v", err)
		}
		assert.Equal(t, ett.ID, e.ID)
	}
}

// Test the update of a profile struct into postgres DB
func TestUpdateEvent(t *testing.T) {
	db, ds, teardown, clearDB := Setup(t)
	defer teardown()

	for _, ett := range EventTestingTable {
		_, err := db.NamedExec(CreateEventQuery, ett)
		if err != nil {
			t.Fatalf("failed to insert profile into DB: %v", err)
		}

		ett.State = event.ProfileSecond
		_, err = ds.Update(*ett)
		if err != nil {
			t.Fatalf("failed to update profile: %v", err)
		}

		// perform fetch and assert
		var ee event.Event
		err = db.Get(&ee, FetchEventQuery, ett.ID)
		if err != nil {
			t.Fatalf("failed to fetch updated event: %v", err)
		}

		assert.Equal(t, event.ProfileSecond, ee.State)

		clearDB()
	}
}

// Test fetch event
func TestFetchEvent(t *testing.T) {
	db, ds, teardown, _ := Setup(t)
	defer teardown()

	for index, ett := range EventTestingTable {
		// every even insert into db
		if index%2 == 0 {
			_, err := db.NamedExec(CreateEventQuery, ett)
			assert.Nil(t, err)
		}

		e, err := ds.Fetch(ett.ID, ett.Profiles[0])
		// if even should fetch event in db
		if index%2 == 0 {
			assert.Nil(t, err)
			assert.Equal(t, ett.ID, e.ID)
			assert.Equal(t, 2, len(e.Profiles))
		} else {
			// should not find event in db
			assert.NotNil(t, err)
			assert.Nil(t, e)
		}
	}
}

func TestFetchByProfiles(t *testing.T) {
	db, ds, teardown, _ := Setup(t)
	defer teardown()

	for index, ett := range EventTestingTable {
		// every even insert into db
		if index%2 == 0 {
			_, err := db.NamedExec(CreateEventQuery, ett)
			assert.Nil(t, err)
		}

		e, err := ds.FetchByProfiles(ett.Profiles)
		// if even should fetch event in db
		if index%2 == 0 {
			assert.Nil(t, err)
			assert.Equal(t, ett.ID, e.ID)
			assert.Equal(t, 2, len(e.Profiles))
		} else {
			// should not find event in db
			assert.NotNil(t, err)
			assert.Nil(t, e)
		}
	}
}

func TestFetchByMatchID(t *testing.T) {
	db, ds, teardown, _ := Setup(t)
	defer teardown()

	for index, ett := range EventTestingTable {
		// every even insert into db
		if index%2 == 0 {
			_, err := db.NamedExec(CreateEventQuery, ett)
			assert.Nil(t, err)
		}

		e, err := ds.FetchByMatchID(ett.MatchID)
		// if even should fetch event in db
		if index%2 == 0 {
			assert.Nil(t, err)
			assert.Equal(t, ett.ID, e.ID)
			assert.Equal(t, 2, len(e.Profiles))
		} else {
			// should not find event in db
			assert.NotNil(t, err)
			assert.Nil(t, e)
		}
	}
}

func TestFetchAll(t *testing.T) {
	db, ds, teardown, _ := Setup(t)
	defer teardown()

	for _, ett := range EventTestingTable {
		// every even insert into db
		_, err := db.NamedExec(CreateEventQuery, ett)
		assert.Nil(t, err)

		all, err := ds.FetchAll("static-profile-id")

		if ett.Profiles[0] == "static-profile-id" {
			assert.Nil(t, err)
			assert.Equal(t, 1, len(all))
		}
	}
}
