-- Connect to our database
\connect saturdate

-- Create bars table
CREATE TABLE bars (
    id varchar(255) PRIMARY KEY,
    bar_name varchar(255),
    bar_category varchar(255),
    bar_price_range varchar(255),
    bar_number_reviews varchar(255),
    bar_number_stars varchar(255),
    bar_hood varchar(255),
    bar_phone varchar(255),
    bar_reservations varchar(255),
    bar_credit_card varchar(255),
    bar_parking varchar(255),
    bar_weel_chair varchar(255),
    bar_attire varchar(255),
    bar_dancing varchar(255),
    bar_happy varchar(255),
    bar_outdoor varchar(255),
    bar_tv varchar(255),
    bar_dogs varchar(255),
    bar_pool_table varchar(255),
    latitude float,
    longitude float,
    image_url varchar(255),
    yelp_url varchar(255),
    address varchar(255),
    address_display varchar(255),
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
