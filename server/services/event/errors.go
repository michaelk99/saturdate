package event

import (
	"net/http"
)

// ErrEventCreate is returned when event cannot be created
type ErrEventCreate struct {
	msg error
}

// ErrFeedbackExists is returned when event cannot be created
type ErrFeedbackExists struct {
	msg error
}

func (e ErrEventCreate) Error() string {
	return "could not create event"
}

func (e ErrFeedbackExists) Error() string {
	return "feedback already exists"
}

// ErrEventExists is returned when event already exists
type ErrEventExists struct {
	msg error
}

func (e ErrEventExists) Error() string {
	return "event already exists"
}

// ServiceToHTTPErrorMap maps the events service's errors to http
func ServiceToHTTPErrorMap(err error) (code int) {
	switch err.(type) {
	case ErrEventCreate:
		return http.StatusConflict
	case ErrEventExists:
		return http.StatusBadRequest
	case ErrFeedbackExists:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
