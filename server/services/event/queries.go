package event

const (
	// MatchID match id
	MatchID int = iota
	// ProfileID profile ID
	ProfileID
	// ID will contain ID and profileID
	ID
)

// IDQuery id query
type IDQuery struct {
	Type  int
	Value []string
}
