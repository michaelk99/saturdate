package event

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/jinzhu/now"
	"github.com/jmoiron/sqlx/types"
	gogeo "github.com/kellydunn/golang-geo"
	"gitlab.com/michaelk99/saturdate/server/pkg/client"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/pkg/utils"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"gopkg.in/guregu/null.v3"
	"time"
)

// Service is a public interface for implementing our Event service
type Service interface {
	Create(ctx context.Context, e models.Event) (*models.Event, error)
	Update(ctx context.Context, e models.Event) (*models.Event, error)
	Fetch(ID string, profileID string) (*models.Event, error)
	FetchAll(profileID string) ([]*models.Event, error)
	// Helper func which gets an open Saturday that works for each user
	GetMutualDate(profs []string) (string, error)
	CreateFeedback(ctx context.Context, f Feedback) (*Feedback, error)
}

type service struct {
	ds EventStore
	mc client.MatchClient
	pc profile.Client
}

// NewService is a constructor for our Event service implementation
func NewService(ds EventStore, mc client.MatchClient, pc profile.Client) Service {
	return &service{
		ds: ds,
		mc: mc,
		pc: pc,
	}
}

func (s *service) Create(ctx context.Context, e models.Event) (*models.Event, error) {
	tkn := ctx.Value("Token").(string)
	session := ctx.Value("Session").(*token.Session)

	// check if event exists
	ee, err := s.ds.FetchByMatchID(e.MatchID)

	// we do not want the event to exist, so if we find it in the db return
	// an err
	if ee != nil {
		return nil, ErrEventExists{}
	}

	// fetch the match to get both profiles
	m, err := s.mc.GetMatch(tkn, e.MatchID)
	if err != nil {
		return nil, err
	}

	// fetch both profiles to get lat/lon position of each
	profIDOne := m.Profiles[0]
	profIDTwo := m.Profiles[1]

	// make sure session user is part of the match
	if session.ProfileID != profIDOne && session.ProfileID != profIDTwo {
		return nil, errors.New("session user is not eligible to create the event for this match")
	}

	profiles, err := s.pc.SearchProfiles(tkn, []string{profIDOne, profIDTwo})
	if err != nil {
		return nil, err
	}

	p1 := profiles[0]
	p2 := profiles[1]

	// get 5 bars near p1
	// get 5 bars near p2
	// get 5 bars that split the middle
	// combine 1 array

	point1 := gogeo.NewPoint(p1.Latitude, p1.Longitude)
	point2 := gogeo.NewPoint(p2.Latitude, p2.Longitude)
	midpoint := point1.MidpointTo(point2)

	barsp1, err := s.ds.FetchBars(p1.Latitude, p1.Longitude)
	if err != nil {
		return nil, err
	}

	barsp2, err := s.ds.FetchBars(p2.Latitude, p2.Longitude)
	if err != nil {
		return nil, err
	}

	barsmid, err := s.ds.FetchBars(midpoint.Lat(), midpoint.Lng())
	if err != nil {
		return nil, err
	}

	e.ID = uuid.New().String()
	e.State = models.ProfileFirst
	e.CreatedAt = time.Now().Format(time.RFC3339)
	e.UpdatedAt = time.Now().Format(time.RFC3339)
	e.Profiles = []string{profIDOne, profIDTwo}
	e.Turn = null.String{
		sql.NullString{
			String: profIDOne,
			Valid:  true,
		},
	}
	// startTime is always fixed to be at 8:00pm
	e.StartTime = "8:00pm"

	// calculate the startDate for this saturdate
	startDate, err := s.GetMutualDate([]string{profIDOne, profIDTwo})
	if err != nil {
		return nil, err
	}
	e.StartDate = startDate

	// take 3 bars from the midpoint, and 1 bar from each profile
	bars := []*models.Bar{
		barsmid[0],
		barsmid[1],
		barsmid[2],
		barsp1[0],
		barsp2[0],
	}

	barsB, err := json.Marshal(bars)
	if err != nil {
		return nil, err
	}

	e.Bars = types.NullJSONText{
		JSONText: barsB,
		Valid:    true,
	}

	// create event in DB
	_, err = s.ds.Create(e)
	if err != nil {
		return nil, err
	}

	return &e, nil
}

func (s *service) Update(ctx context.Context, e models.Event) (*models.Event, error) {
	session := ctx.Value("Session").(*token.Session)

	// first, make sure profile owns event
	ee, err := s.ds.Fetch(e.ID, session.ProfileID)

	if err != nil {
		return nil, err
	}

	if ee == nil {
		return nil, fmt.Errorf("Event not found for ID, profile ID: %s %s", e.ID, session.ProfileID)
	}

	// override fields from db
	e.UpdatedAt = time.Now().Format(time.RFC3339)
	e.Profiles = ee.Profiles
	e.MatchID = ee.MatchID

	// only data to update in the `complete` state is the `notified` array
	if ee.State == models.Complete {
		// override other fields
		e.Turn = ee.Turn
		e.State = ee.State
		e.Bars = ee.Bars

		// ensure both aren't already notified
		if len(ee.Notified) == 2 {
			return nil, fmt.Errorf("Both users already notified: %s, %s", ee.Notified[0], ee.Notified[1])
		}

		// ensure this user isn't already notfied
		if len(ee.Notified) == 1 {
			if session.ProfileID == ee.Notified[0] {
				return nil, fmt.Errorf("User is already notified: %s", session.ProfileID)
			}
		}

		// add the session profileID to the notified array
		e.Notified = ee.Notified
		e.Notified = append(e.Notified, session.ProfileID)

		// update the event record in the db
		_, err = s.ds.Update(e)
		if err != nil {
			return nil, err
		}

		return &e, nil
	}

	// marshal bars into bytes
	b, err := json.Marshal(e.Bars.JSONText)
	if err != nil {
		return nil, fmt.Errorf("Unable to marshal json into bytes")
	}

	// unmarshal bytes into bars struct
	var bars []*models.Bar
	err = json.Unmarshal(b, &bars)
	if err != nil {
		return nil, fmt.Errorf("Unable to unmarshal bytes into bars struct")
	}

	// move to `ProfileSecond` state
	if ee.State == models.ProfileFirst {
		// event can only transition to the next state with the correct logged
		// in user
		if session.ProfileID != ee.Turn.String {
			return nil, fmt.Errorf("Not the user's turn: %s %s", session.ProfileID, ee.Turn.String)
		}

		// need to ensure selection process narrowed down to 3 bars
		if len(bars) != 3 {
			return nil, fmt.Errorf("Invalid number of bars, expected: %d, got: %d", 3, len(bars))
		}

		// update state
		e.State = models.ProfileSecond

		// update bars

		e.Bars = types.NullJSONText{
			JSONText: b,
			Valid:    true,
		}

		// update turn
		for _, p := range ee.Profiles {
			if p != ee.Turn.String {
				e.Turn.String = p
			}
		}
	}

	// move to `Complete` state
	if ee.State == models.ProfileSecond {
		// event can only transition to the next state with the correct logged
		// in user
		if session.ProfileID != ee.Turn.String {
			return nil, fmt.Errorf("Not the user's turn, %s %s", session.ProfileID, ee.Turn.String)
		}

		if len(bars) != 1 {
			return nil, fmt.Errorf("Invalid number of bars, expected: %d, got: %d", 1, len(bars))
		}

		// update state
		e.State = models.Complete

		// bars should be set to the only bar left
		e.Bars = types.NullJSONText{
			JSONText: b,
			Valid:    true,
		}

		// `turn` should be reset to null since it is now no longer anyone's
		// "turn"
		e.Turn = null.String{
			sql.NullString{
				String: "",
				Valid:  false,
			},
		}

		// `notified` should be set to the current profile since they will
		// get notified immediately
		// !! important !! went back and forth setting this notified array,
		// but eventually came to setting it on behalf of the user to avoid
		// ios showing the match screen multiple times

		// set `notified` to current profile
		e.Notified = []string{session.ProfileID}
	}

	// update event in db
	_, err = s.ds.Update(e)
	if err != nil {
		return nil, err
	}

	return &e, nil
}

func (s *service) Fetch(ID string, profileID string) (*models.Event, error) {
	e, err := s.ds.Fetch(ID, profileID)
	if err != nil {
		return nil, err
	}
	return e, nil
}

func (s *service) FetchAll(profileID string) ([]*models.Event, error) {
	events, err := s.ds.FetchAll(profileID)
	if err != nil {
		return nil, err
	}

	return events, nil
}

func (s *service) CreateFeedback(ctx context.Context, f Feedback) (*Feedback, error) {
	session := ctx.Value("Session").(*token.Session)
	profileID := session.ProfileID

	// override the `profileID` in the `feedback` entry to be the logged in
	// user's profile
	f.ProfileID = profileID

	// make sure `f.ProfileID` and `f.ProfileIDMatch` are not the same
	if f.ProfileID == f.ProfileIDMatch {
		return nil, errors.New("Invalid feedback body: `profile_id` and `profile_id_match` must but be unique")
	}

	// make sure the event belongs to the logged in user and other profile
	// referenced in the body. Prevents anyone from mucking up
	// `profile_id_match` in the body.
	e, err := s.ds.FetchByProfiles([]string{f.ProfileID, f.ProfileIDMatch})
	if err != nil {
		return nil, err
	}
	if e.ID != f.EventID {
		return nil, fmt.Errorf("Invalid event id, expected: %s, got: %s", e.ID, f.EventID)
	}

	// make sure the feedback record doesn't already exist
	ff, _ := s.ds.FetchFeedbackByProfileIDs(f.ProfileID, f.ProfileIDMatch)
	// if ff exists then `feedback` record already exists
	if ff != nil {
		return nil, ErrFeedbackExists{}
	}

	// set `ID`, `CreatedAt` and `UpdatedAt`
	f.ID = uuid.New().String()
	f.CreatedAt = time.Now().Format(time.RFC3339)
	f.UpdatedAt = time.Now().Format(time.RFC3339)

	ff, err = s.ds.CreateFeedback(f)
	if err != nil {
		return nil, err
	}

	return ff, nil
}

func (s *service) GetMutualDate(profs []string) (string, error) {
	profID1 := profs[0]
	profID2 := profs[1]

	// default is this Saturday
	defaultDate := now.EndOfWeek().Format(time.RFC3339)

	// fetch events from each profile
	// take the event that is on the later date
	// return the next week's Saturday
	events1, err := s.ds.FetchAll(profID1)
	if err != nil {
		return "", err
	}

	events2, err := s.ds.FetchAll(profID2)
	if err != nil {
		return "", err
	}

	// if neither users have any events, return the current Saturday of the
	// week
	if len(events1) == 0 && len(events2) == 0 {
		return defaultDate, nil
	}

	var event1 *models.Event
	var event2 *models.Event

	if len(events1) != 0 {
		event1 = events1[0]
	}

	if len(events2) != 0 {
		event2 = events2[0]
	}

	getSaturDate := func(dateTime string) string {
		date := utils.GetDateTime(dateTime)
		today := time.Now()
		// if the date is earlier today than ignore and return default date
		if date.Before(today) {
			return defaultDate
		}
		// add 24 hours to get the sunday
		sunday := date.Add(time.Hour * 24)
		n := now.Now{
			sunday,
		}
		// return end of week
		return n.EndOfWeek().Format(time.RFC3339)
	}

	// if event1 is nil, return date compatable with event2
	if event1 == nil {
		return getSaturDate(event2.StartDate), nil
	}

	// if event2 is nil, return date compatable with event1
	if event2 == nil {
		return getSaturDate(event1.StartDate), nil
	}

	// both are not nil, get the later date
	d1 := utils.GetDateTime(event1.StartDate)
	d2 := utils.GetDateTime(event2.StartDate)

	// d1 is the later date
	if d1.After(d2) {
		return getSaturDate(event1.StartDate), nil
	}

	// d2 is the later date
	return getSaturDate(event2.StartDate), nil
}
