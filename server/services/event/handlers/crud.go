package handlers

import (
	"net/http"

	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/event"
	"path/filepath"
	"regexp"
	"strings"
)

// CRUD forwards request based on http method
func CRUD(v validator.Validator, ms event.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		switch r.Method {
		case http.MethodGet:
			// check between fetch and fetch all within GET, since you cannot
			// differentiate http verbs between the two
			path := filepath.Clean(r.URL.Path)
			id := strings.TrimPrefix(path, "/api/v1/events")
			if id == "" {
				FetchAll(ms).ServeHTTP(w, r)
				return
			}
			Fetch(ms).ServeHTTP(w, r)
			return
		case http.MethodPost:
			// check regex to see if this is the `feedback` endpoint:
			// i.e., `/:uuid/feedback`
			var rUUID = regexp.MustCompile(`[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/feedback`)
			switch {
			case rUUID.MatchString(r.URL.Path):
				CreateFeedback(v, ms).ServeHTTP(w, r)
			default:
				// otherwise a normal create `event` request
				Create(v, ms).ServeHTTP(w, r)
			}
			return
		case http.MethodPatch:
			Update(v, ms).ServeHTTP(w, r)
			return
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}
	}
}
