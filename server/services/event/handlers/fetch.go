package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/event"
	"path/filepath"
)

const (
	// FetchErrCode code
	FetchErrCode = "event.fetch.error"
	// FetchExistsCode code
	FetchExistsCode = "event.fetch.exists"
)

// Fetch checks email against password and assigns a token if valid
func Fetch(s event.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("event.fetch.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/events/")

		if id == "" {
			log.Printf("event.fetch.invalid_id")
			http.Error(w, "invalid request, need id", http.StatusBadRequest)
			return
		}

		// look for the profile id in the session
		session := r.Context().Value("Session").(*token.Session)
		profileID := session.ProfileID

		e, err := s.Fetch(id, profileID)
		if err != nil {
			resp := &je.Response{
				Code:    FetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, event.ServiceToHTTPErrorMap(err))
			return
		}

		// return created event
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(e)
		if err != nil {
			log.Printf("%s: %v", FetchErrCode, err)
			resp := &je.Response{
				Code:    FetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully fetched event id %s", id)
		return
	}
}
