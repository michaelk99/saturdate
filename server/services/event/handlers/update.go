package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"path/filepath"

	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/event"
)

const (
	// UpdateErrCode code
	UpdateErrCode = "event.update.error"
	// UpdateExistsCode code
	UpdateExistsCode = "event.update.exists"
	// UpdateBadDataCode bad data
	UpdateBadDataCode = "event.update.bad_data"
)

// Update checks email against password and assigns a token if valid
func Update(v validator.Validator, s event.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support PATCH
		if r.Method != http.MethodPatch {
			log.Printf("event.update.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/events/")

		if id == "" {
			log.Printf("event.update.invalid_id")
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		var e models.Event
		err := json.NewDecoder(r.Body).Decode(&e)

		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// set ID
		e.ID = id

		// validate event
		ok, fieldErrors := v.Struct(e)
		if !ok {
			resp := &je.Response{
				Code:       UpdateBadDataCode,
				Message:    UpdateBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// update the event
		ee, err := s.Update(r.Context(), e)
		if err != nil {
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, event.ServiceToHTTPErrorMap(err))
			return
		}

		// return created event
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(ee)
		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully updated event id %s", e.ID)
		return
	}
}
