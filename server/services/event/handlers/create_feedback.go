package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/event"
)

const (
	// CreateFeedbackErrCode error code
	CreateFeedbackErrCode = "feedback.create.error"
	// CreateFeedbackExistCode error code exists
	CreateFeedbackExistCode = "feedback.create.exists"
	// CreateFeedbackBadDataCode bad data
	CreateFeedbackBadDataCode = "feedback.create.bad_data"
)

// CreateFeedback feedback entry
func CreateFeedback(v validator.Validator, es event.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support POST
		if r.Method != http.MethodPost {
			log.Printf("feedback.create.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		var f event.Feedback
		err := json.NewDecoder(r.Body).Decode(&f)
		if err != nil {
			log.Printf("%s: %v", CreateFeedbackErrCode, err)
			resp := &je.Response{
				Code:    CreateFeedbackErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// validate feedback
		ok, fieldErrors := v.Struct(f)
		if !ok {
			resp := &je.Response{
				Code:       CreateFeedbackBadDataCode,
				Message:    CreateFeedbackBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		ff, err := es.CreateFeedback(r.Context(), f)
		if err != nil {
			resp := &je.Response{
				Code:    CreateFeedbackErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, event.ServiceToHTTPErrorMap(err))
			return
		}

		// return created feedback
		w.WriteHeader(http.StatusCreated) // must write status header before NewEcoder closes body
		err = json.NewEncoder(w).Encode(ff)
		if err != nil {
			log.Printf("%s: %v", CreateFeedbackErrCode, err)
			resp := &je.Response{
				Code:    CreateFeedbackErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusInternalServerError)
			return
		}

		log.Printf("successfully created feedback for ID %s", f.ID)
		return
	}
}
