package event_test

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	gomock "github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx/types"
	"github.com/stretchr/testify/assert"
	"gitlab.com/michaelk99/saturdate/server/pkg/client"
	"gitlab.com/michaelk99/saturdate/server/pkg/models"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/event"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"gopkg.in/guregu/null.v3"
	"testing"
)

func getContext() context.Context {
	ctx := context.Background()
	tk := &token.Session{
		ProfileID: uuid.New().String(),
	}
	ctx = context.WithValue(ctx, "Session", tk)
	ctx = context.WithValue(ctx, "Token", "some-jwt")
	return ctx
}

var TestCreateTT = []struct {
	name        string
	createEvent func(ctrl *gomock.Controller, t *testing.T)
}{
	{
		name: "should create event",
		createEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			tkn := ctx.Value("Token").(string)
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			ma := &models.Match{
				ID: uuid.New().String(),
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			p1 := &profile.Profile{
				Location: profile.Location{
					Latitude:  1,
					Longitude: 2,
				},
			}

			p2 := &profile.Profile{
				Location: profile.Location{
					Latitude:  3,
					Longitude: 4,
				},
			}

			mockEventStore.EXPECT().FetchByMatchID(ev.MatchID).Return([]interface{}{
				nil, nil,
			}...)
			mockMatchClient.EXPECT().GetMatch(tkn, ev.MatchID).Return([]interface{}{
				ma, nil,
			}...)

			_ = mockProfileClient.EXPECT().SearchProfiles(tkn, []string{ev.Profiles[0], ev.Profiles[1]}).Return([]*profile.Profile{p1, p2}, nil)

			barsp1 := []*models.Bar{
				&models.Bar{
					ID: "b1",
				},
			}

			barsp2 := []*models.Bar{
				&models.Bar{
					ID: "b2",
				},
			}

			barsmid := []*models.Bar{
				&models.Bar{
					ID: "b3",
				},
				&models.Bar{
					ID: "b4",
				},
				&models.Bar{
					ID: "b5",
				},
			}

			first := mockEventStore.EXPECT().FetchBars(p1.Latitude, p1.Longitude).Return([]interface{}{
				barsp1, nil}...)
			second := mockEventStore.EXPECT().FetchBars(p2.Latitude, p2.Longitude).Return([]interface{}{
				barsp2, nil}...)
			third := mockEventStore.EXPECT().FetchBars(gomock.Any(), gomock.Any()).Return([]interface{}{
				barsmid, nil}...)

			gomock.InOrder(
				first,
				second,
				third,
			)

			first = mockEventStore.EXPECT().FetchAll(session.ProfileID).Return([]interface{}{
				[]*models.Event{}, nil,
			}...)
			second = mockEventStore.EXPECT().FetchAll("profile-two").Return([]interface{}{
				[]*models.Event{}, nil,
			}...)

			gomock.InOrder(
				first,
				second,
			)

			mockEventStore.EXPECT().Create(gomock.Any()).Return([]interface{}{
				nil, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Create(ctx, *ev)

			assert.Nil(t, err)
		},
	}, {
		name: "should throw error if event exists for matchID",
		createEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)
			mockEventStore.EXPECT().FetchByMatchID(ev.MatchID).Return([]interface{}{
				ev, nil,
			}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Create(ctx, *ev)

			assert.NotNil(t, err)
			assert.IsType(t, event.ErrEventExists{}, err)
		},
	}, {
		name: "should throw error when fetching match",
		createEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			tkn := ctx.Value("Token").(string)
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().FetchByMatchID(ev.MatchID).Return([]interface{}{
				nil, nil,
			}...)

			mockMatchClient.EXPECT().GetMatch(tkn, ev.MatchID).Return([]interface{}{
				nil, errors.New("Error fetching match"),
			}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Create(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error fetching match")
		},
	}, {
		name: "should throw error if session profileID not in db match record",
		createEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			tkn := ctx.Value("Token").(string)
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			ma := &models.Match{
				ID: uuid.New().String(),
				Profiles: []string{
					"bad-profile-id-1",
					"bad-profile-id-2",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().FetchByMatchID(ev.MatchID).Return([]interface{}{
				nil, nil,
			}...)

			mockMatchClient.EXPECT().GetMatch(tkn, ev.MatchID).Return([]interface{}{
				ma, nil,
			}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Create(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "session user is not eligible to create the event for this match")
		},
	},
}

func TestCreate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestCreateTT {
		tt.createEvent(ctrl, t)
	}
}

var TestUpdateTT = []struct {
	name        string
	updateEvent func(ctrl *gomock.Controller, t *testing.T)
}{
	{
		name: "should move event from state ProfileFirst to ProfileSecond",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			id := uuid.New().String()
			matchID := uuid.New().String()

			ev := &models.Event{
				ID:      id,
				MatchID: matchID,
				State:   models.ProfileFirst,
				Turn: null.String{
					sql.NullString{
						String: session.ProfileID,
						Valid:  true,
					},
				},
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			bars := []*models.Bar{
				&models.Bar{
					ID: "b3",
				},
				&models.Bar{
					ID: "b4",
				},
				&models.Bar{
					ID: "b5",
				},
			}

			barsB, _ := json.Marshal(bars)

			ev.Bars = types.NullJSONText{
				JSONText: barsB,
				Valid:    true,
			}

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				ev, nil}...)

			mockEventStore.EXPECT().Update(gomock.Any()).Return([]interface{}{
				ev, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			ee, err := s.Update(ctx, *ev)

			assert.Nil(t, err)
			// should now be the next user's turn
			assert.Equal(t, ee.Turn.String, "profile-two")
			assert.Equal(t, ee.ID, id)
			assert.Equal(t, ee.MatchID, matchID)
			assert.Equal(t, ee.State, models.ProfileSecond)
			assert.Equal(t, ee.Bars, ev.Bars)
			assert.Equal(t, ee.ID, ev.ID)
		},
	}, {
		name: "should move event from state ProfileSecond to Complete",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				State:   models.ProfileSecond,
				Turn: null.String{
					sql.NullString{
						String: session.ProfileID,
						Valid:  true,
					},
				},
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			bars := []*models.Bar{
				&models.Bar{
					ID: "b3",
				},
			}

			barsB, _ := json.Marshal(bars)

			ev.Bars = types.NullJSONText{
				JSONText: barsB,
				Valid:    true,
			}

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				ev, nil}...)

			mockEventStore.EXPECT().Update(gomock.Any()).Return([]interface{}{
				ev, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			ee, err := s.Update(ctx, *ev)

			assert.Nil(t, err)
			assert.Equal(t, ee.Turn.Valid, false)
			assert.Equal(t, ee.State, models.Complete)
			assert.Equal(t, ee.ID, ev.ID)
		},
	}, {
		name: "[Complete state] should update notified array in Complete state",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				State:   models.Complete,
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				ev, nil}...)

			mockEventStore.EXPECT().Update(gomock.Any()).Return([]interface{}{
				ev, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			ee, err := s.Update(ctx, *ev)

			assert.Nil(t, err)
			assert.Equal(t, ee.Turn.Valid, false)
			assert.Equal(t, ee.State, models.Complete)
			assert.Equal(t, ee.ID, ev.ID)
			assert.Equal(t, len(ee.Notified), 1)
			assert.Equal(t, ee.Notified[0], session.ProfileID)
		},
	}, {
		name: "should throw error if event fetch returns error",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID: uuid.New().String(),
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				nil, errors.New("Error fetching event")}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Update(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error fetching event")
		},
	}, {
		name: "should throw error if event does not exist",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID: uuid.New().String(),
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				nil, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Update(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), fmt.Sprintf("Event not found for ID, profile ID: %s %s", ev.ID, session.ProfileID))
		},
	}, {
		name: "[ProfileFirst state] should throw error if not user's turn",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				State:   models.ProfileFirst,
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
				Turn: null.String{
					sql.NullString{
						String: "profile-two",
						Valid:  true,
					},
				},
			}

			bars := []*models.Bar{
				&models.Bar{
					ID: "b1",
				},
				&models.Bar{
					ID: "b2",
				},
				&models.Bar{
					ID: "b3",
				},
			}

			barsB, _ := json.Marshal(bars)

			ev.Bars = types.NullJSONText{
				JSONText: barsB,
				Valid:    true,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				ev, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Update(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, fmt.Sprintf("Not the user's turn: %s %s", session.ProfileID, ev.Turn.String), err.Error())
		},
	}, {
		name: "[ProfileFirst state] should throw error if num bars is not = 3",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				State:   models.ProfileFirst,
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
				Turn: null.String{
					sql.NullString{
						String: session.ProfileID,
						Valid:  true,
					},
				},
			}

			bars := []*models.Bar{
				&models.Bar{
					ID: "b3",
				},
			}

			barsB, _ := json.Marshal(bars)

			ev.Bars = types.NullJSONText{
				JSONText: barsB,
				Valid:    true,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				ev, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Update(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, fmt.Sprintf("Invalid number of bars, expected: %d, got: %d", 3, 1), err.Error())
		},
	}, {
		name: "[Complete state] should throw error if notified array already has both profiles",
		updateEvent: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			ev := &models.Event{
				ID:      uuid.New().String(),
				MatchID: uuid.New().String(),
				State:   models.Complete,
				Profiles: []string{
					session.ProfileID,
					"profile-two",
				},
				Notified: []string{
					session.ProfileID,
					"profile-two",
				},
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			mockEventStore.EXPECT().Fetch(ev.ID, session.ProfileID).Return([]interface{}{
				ev, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.Update(ctx, *ev)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), fmt.Sprintf("Both users already notified: %s, %s", session.ProfileID, "profile-two"))
		},
	},
}

func TestUpdate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestUpdateTT {
		tt.updateEvent(ctrl, t)
	}
}

var TestFeedbackTT = []struct {
	name           string
	createFeedback func(ctrl *gomock.Controller, t *testing.T)
}{
	{
		name: "should throw error if ProfileID and ProfileIDMatch are not unique",
		createFeedback: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			f := event.Feedback{
				ID:             uuid.New().String(),
				EventID:        uuid.New().String(),
				ProfileID:      session.ProfileID,
				ProfileIDMatch: session.ProfileID,
				DidShow:        false,
				BarRating:      1,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.CreateFeedback(ctx, f)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Invalid feedback body: `profile_id` and `profile_id_match` must but be unique")
		},
	},
	{
		name: "should throw error if FetchByProfiles errs",
		createFeedback: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			eventID := uuid.New().String()

			f := event.Feedback{
				ID:             uuid.New().String(),
				EventID:        eventID,
				ProfileID:      session.ProfileID,
				ProfileIDMatch: "profile-two",
				DidShow:        false,
				BarRating:      1,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			ev := &models.Event{
				ID: eventID,
			}

			mockEventStore.EXPECT().FetchByProfiles([]string{f.ProfileID, f.ProfileIDMatch}).Return([]interface{}{
				ev, errors.New("Error fetch")}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.CreateFeedback(ctx, f)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "Error fetch")
		},
	},
	{
		name: "should throw error if feedback already exists for logged in user",
		createFeedback: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			eventID := uuid.New().String()

			f := event.Feedback{
				ID:             uuid.New().String(),
				EventID:        eventID,
				ProfileID:      session.ProfileID,
				ProfileIDMatch: "profile-two",
				DidShow:        false,
				BarRating:      1,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			ev := &models.Event{
				ID: eventID,
			}

			mockEventStore.EXPECT().FetchByProfiles([]string{f.ProfileID, f.ProfileIDMatch}).Return([]interface{}{
				ev, nil}...)

			mockEventStore.EXPECT().FetchFeedbackByProfileIDs(f.ProfileID, f.ProfileIDMatch).Return([]interface{}{
				&f, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.CreateFeedback(ctx, f)

			assert.NotNil(t, err)
			assert.IsType(t, event.ErrFeedbackExists{}, err)
		},
	},
	{
		name: "should throw error if create feedback fails",
		createFeedback: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			eventID := uuid.New().String()

			f := event.Feedback{
				EventID:        eventID,
				ProfileID:      session.ProfileID,
				ProfileIDMatch: "profile-two",
				DidShow:        false,
				BarRating:      1,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			ev := &models.Event{
				ID: eventID,
			}

			mockEventStore.EXPECT().FetchByProfiles([]string{f.ProfileID, f.ProfileIDMatch}).Return([]interface{}{
				ev, nil}...)

			mockEventStore.EXPECT().FetchFeedbackByProfileIDs(f.ProfileID, f.ProfileIDMatch).Return([]interface{}{
				nil, nil}...)

			mockEventStore.EXPECT().CreateFeedback(gomock.Any()).Return([]interface{}{
				nil, errors.New("create failed")}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.CreateFeedback(ctx, f)

			assert.NotNil(t, err)
			assert.Equal(t, err.Error(), "create failed")
		},
	},
	{
		name: "should create feedback",
		createFeedback: func(ctrl *gomock.Controller, t *testing.T) {
			ctx := getContext()
			session := ctx.Value("Session").(*token.Session)

			eventID := uuid.New().String()

			f := event.Feedback{
				ID:             uuid.New().String(),
				EventID:        eventID,
				ProfileID:      session.ProfileID,
				ProfileIDMatch: "profile-two",
				DidShow:        false,
				BarRating:      1,
			}

			// setup for testing
			mockEventStore := event.NewMockEventStore(ctrl)
			mockProfileClient := profile.NewMockClient(ctrl)
			mockMatchClient := client.NewMockMatchClient(ctrl)

			ev := &models.Event{
				ID: eventID,
			}

			mockEventStore.EXPECT().FetchByProfiles([]string{f.ProfileID, f.ProfileIDMatch}).Return([]interface{}{
				ev, nil}...)

			mockEventStore.EXPECT().FetchFeedbackByProfileIDs(f.ProfileID, f.ProfileIDMatch).Return([]interface{}{
				nil, nil}...)

			mockEventStore.EXPECT().CreateFeedback(gomock.Any()).Return([]interface{}{
				&f, nil}...)

			// test service
			s := event.NewService(mockEventStore, mockMatchClient, mockProfileClient)
			_, err := s.CreateFeedback(ctx, f)

			assert.Nil(t, err)
		},
	},
}

func TestFeedback(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestFeedbackTT {
		tt.createFeedback(ctrl, t)
	}
}
