package s3

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	defaultFileExtension = "png"
)

type imageStore struct {
	bucketName string
	// looks something like https://saturdate.s3.com
	uploadURL *url.URL
	// path is a string that looks something like `/images`
	uploadPath string
	svc        *s3.S3
}

func NewImageStore(bucketName string, uploadURL string, uploadPath string, svc *s3.S3) (image.ImageStore, error) {
	upURL, err := url.Parse(uploadURL)
	if err != nil {
		return nil, err
	}
	is := &imageStore{
		bucketName: bucketName,
		uploadURL:  upURL,
		uploadPath: uploadPath,
		svc:        svc,
	}

	return is, nil
}

// id is the image `id` in the db. The db `id` is used as the filename in s3
func (is *imageStore) Upload(ctx context.Context, id string, img []byte) (*image.UploadResp, error) {
	fileName := fmt.Sprintf("%s.%s", id, defaultFileExtension)
	pathToFile := fmt.Sprintf("%s/%s", is.uploadPath, fileName)

	fileType := http.DetectContentType(img)
	var fileSize int64
	fileSize = int64(len(img))
	params := &s3.PutObjectInput{
		Bucket:        aws.String(is.bucketName),
		Key:           aws.String(pathToFile),
		Body:          bytes.NewReader(img),
		ContentLength: aws.Int64(fileSize),
		ContentType:   aws.String(fileType),
		ACL:           aws.String("public-read"),
	}

	_, err := is.svc.PutObject(params)

	if err != nil {
		return nil, err
	}

	// set the url and url fallback to the url + path to file
	return &image.UploadResp{
		URL:         fmt.Sprintf("%s/%s", is.uploadURL.String(), pathToFile),
		URLFallback: fmt.Sprintf("%s/%s", is.uploadURL.String(), pathToFile),
	}, nil
}

func (is *imageStore) Delete(id string) error {
	fileName := fmt.Sprintf("%s.%s", id, defaultFileExtension)
	pathToFile := fmt.Sprintf("%s/%s", is.uploadPath, fileName)
	input := &s3.DeleteObjectInput{
		Bucket: aws.String(is.bucketName),
		Key:    aws.String(pathToFile),
	}

	_, err := is.svc.DeleteObject(input)
	if err != nil {
		return err
	}

	return nil
}
