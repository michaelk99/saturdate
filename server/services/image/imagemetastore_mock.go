// Code generated by MockGen. DO NOT EDIT.
// Source: services/image/imagemetastore.go

// Package image is a generated GoMock package.
package image

import (
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
)

// MockImageMetaStore is a mock of ImageMetaStore interface
type MockImageMetaStore struct {
	ctrl     *gomock.Controller
	recorder *MockImageMetaStoreMockRecorder
}

// MockImageMetaStoreMockRecorder is the mock recorder for MockImageMetaStore
type MockImageMetaStoreMockRecorder struct {
	mock *MockImageMetaStore
}

// NewMockImageMetaStore creates a new mock instance
func NewMockImageMetaStore(ctrl *gomock.Controller) *MockImageMetaStore {
	mock := &MockImageMetaStore{ctrl: ctrl}
	mock.recorder = &MockImageMetaStoreMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockImageMetaStore) EXPECT() *MockImageMetaStoreMockRecorder {
	return m.recorder
}

// Create mocks base method
func (m *MockImageMetaStore) Create(arg0 *Image) (*Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", arg0)
	ret0, _ := ret[0].(*Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Create indicates an expected call of Create
func (mr *MockImageMetaStoreMockRecorder) Create(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockImageMetaStore)(nil).Create), arg0)
}

// Delete mocks base method
func (m *MockImageMetaStore) Delete(ID string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", ID)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete
func (mr *MockImageMetaStoreMockRecorder) Delete(ID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockImageMetaStore)(nil).Delete), ID)
}

// Update mocks base method
func (m *MockImageMetaStore) Update(arg0 *Image) (*Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", arg0)
	ret0, _ := ret[0].(*Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Update indicates an expected call of Update
func (mr *MockImageMetaStoreMockRecorder) Update(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockImageMetaStore)(nil).Update), arg0)
}

// Fetch mocks base method
func (m *MockImageMetaStore) Fetch(ID string) (*Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Fetch", ID)
	ret0, _ := ret[0].(*Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Fetch indicates an expected call of Fetch
func (mr *MockImageMetaStoreMockRecorder) Fetch(ID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Fetch", reflect.TypeOf((*MockImageMetaStore)(nil).Fetch), ID)
}

// FetchAllByCondition mocks base method
func (m *MockImageMetaStore) FetchAllByCondition(whereCondition string) ([]*Image, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "FetchAllByCondition", whereCondition)
	ret0, _ := ret[0].([]*Image)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// FetchAllByCondition indicates an expected call of FetchAllByCondition
func (mr *MockImageMetaStoreMockRecorder) FetchAllByCondition(whereCondition interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "FetchAllByCondition", reflect.TypeOf((*MockImageMetaStore)(nil).FetchAllByCondition), whereCondition)
}
