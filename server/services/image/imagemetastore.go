package image

// ImageMetaStore store functions for manipulating image records in the db
type ImageMetaStore interface {
	Create(*Image) (*Image, error)
	Delete(ID string) error
	Update(*Image) (*Image, error)
	Fetch(ID string) (*Image, error)
	FetchAllByCondition(whereCondition string) ([]*Image, error)
}
