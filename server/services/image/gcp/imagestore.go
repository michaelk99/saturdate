package gcp

import (
	"context"
	"fmt"
	"net/url"

	"cloud.google.com/go/storage"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	defaultFileType = "jpg"
)

type imageStore struct {
	bucket *storage.BucketHandle
	// looks something like https://saturdate.s3.com
	uploadURL *url.URL
	// path is a string that looks something like `/images`
	uploadPath string
}

func NewImageStore(bucket *storage.BucketHandle, uploadURL string, uploadPath string) (image.ImageStore, error) {
	upURL, err := url.Parse(uploadURL)
	if err != nil {
		return nil, err
	}
	is := &imageStore{
		bucket:     bucket,
		uploadURL:  upURL,
		uploadPath: uploadPath,
	}

	return is, nil
}

func (is *imageStore) Upload(ctx context.Context, id string, img []byte) (*image.UploadResp, error) {
	// format and create gcp object
	fileName := fmt.Sprintf("%s.%s", id, defaultFileType)
	pathToFile := fmt.Sprintf("%s/%s", is.uploadPath, fileName)
	wc := is.bucket.Object(pathToFile).NewWriter(ctx)
	defer wc.Close()

	// write bytes
	_, err := wc.Write(img)
	if err != nil {
		return nil, image.ErrImageNotUploaded{err}
	}

	// add path to file
	is.uploadURL.Path = pathToFile

	// set the url and url fallback to the url + path to file
	return &image.UploadResp{
		URL:         is.uploadURL.String(),
		URLFallback: is.uploadURL.String(),
	}, nil
}

func (is *imageStore) Delete(object string) error {
	ctx := context.Background()
	o := is.bucket.Object(object)

	// delete the object from storage
	err := o.Delete(ctx)

	if err != nil {
		fmt.Printf("Failed to delete object from storage %s", err)
		return err
	}

	return nil
}
