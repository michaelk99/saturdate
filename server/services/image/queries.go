package image

const (
	IDs int = iota
	SearchQuery
	ProfileID
)

type IDQuery struct {
	Type  int
	Value string
}
