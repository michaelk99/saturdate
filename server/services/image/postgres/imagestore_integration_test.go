// +build integration

package postgres_test

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/michaelk99/saturdate/server/services/image"
	"gitlab.com/michaelk99/saturdate/server/services/image/postgres"
	"testing"
	"time"
)

const (
	CreateImageQuery = `INSERT INTO images (id, url, url_fallback, description, approval_score, is_archived, created_at, updated_at) VALUES (:id, :url, :url_fallback, :description, :approval_score, :is_archived, :created_at, :updated_at);`
	DeleteImageQuery = "DELETE from images"
	// FetchImageQuery fetch a image record from the db
	FetchImageQuery = `SELECT * FROM images WHERE id = $1`
)

func Setup(t *testing.T) (*sqlx.DB, image.ImageMetaStore, func()) {
	connString := "host=localhost user=postgres dbname=saturdate password='' sslmode=disable"
	if os.Getenv("POSTGRES_CONN_STRING") != "" {
		connString = os.Getenv("POSTGRES_CONN_STRING")
	}
	db, err := sqlx.Connect("postgres", connString)
	if err != nil {
		t.Fatalf("failed to connect to db: %s", err)
	}

	ds := postgres.NewImageMetaStore(db)

	return db, ds, func() {
		defer db.Close()

		_, err := db.Exec(DeleteImageQuery)
		if err != nil {
			t.Fatalf("failed to delete image test records %s", err)
		}
	}
}

var ImageTestingTable = []*image.Image{
	&image.Image{
		ID:          uuid.New().String(),
		URL:         "https://cloudinary.url",
		URLFallback: "https://s3.fallback",
		CreatedAt:   time.Now().Format(time.RFC3339),
		UpdatedAt:   time.Now().Format(time.RFC3339),
		IsArchived:  false,
	},
	&image.Image{
		ID:          uuid.New().String(),
		URL:         "https://cloudinary.url",
		URLFallback: "https://s3.fallback",
		CreatedAt:   time.Now().Format(time.RFC3339),
		UpdatedAt:   time.Now().Format(time.RFC3339),
		IsArchived:  false,
	},
	&image.Image{
		ID:          uuid.New().String(),
		URL:         "https://cloudinary.url",
		URLFallback: "https://s3.fallback",
		CreatedAt:   time.Now().Format(time.RFC3339),
		UpdatedAt:   time.Now().Format(time.RFC3339),
		IsArchived:  true,
	},
}

func TestCreate(t *testing.T) {
	_, ds, teardown := Setup(t)
	defer teardown()

	for _, im := range ImageTestingTable {
		_, err := ds.Create(im)
		if err != nil {
			t.Fatalf("failed to create image record in db %s", err)
		}
	}
}

func TestUpdate(t *testing.T) {
	db, ds, teardown := Setup(t)
	defer teardown()

	for _, im := range ImageTestingTable {
		// create the image record
		_, err := db.NamedExec(CreateImageQuery, im)
		if err != nil {
			t.Fatalf("failed to create image record in db %s", err)
		}

		// make a change
		im.Description = "change"
		_, err = ds.Update(im)
		if err != nil {
			t.Fatalf("failed to update image %s", err)
		}

		// fetch from the db and assert
		var img image.Image
		err = db.Get(&img, FetchImageQuery, im.ID)
		if err != nil {
			t.Fatalf("failed to fetch updated image record from db: %s", err)
		}

		assert.Equal(t, im.Description, "change")
	}
}

func TestDelete(t *testing.T) {
	db, ds, teardown := Setup(t)
	defer teardown()

	for _, im := range ImageTestingTable {
		_, err := db.NamedExec(CreateImageQuery, im)
		if err != nil {
			t.Fatalf("failed to create image record in db %s", err)
		}

		err = ds.Delete(im.ID)
		if err != nil {
			t.Fatalf("failed to delete image record from db %s", err)
		}
	}

	rows, err := db.Queryx("SELECT * from images")
	if err != nil {
		t.Fatalf("failed to query db: %s", err)
	}

	// expect row to not exist
	assert.Equal(t, false, rows.Next())
}

func TestFetch(t *testing.T) {
	db, ds, teardown := Setup(t)
	defer teardown()

	for _, im := range ImageTestingTable {
		_, err := db.NamedExec(CreateImageQuery, im)
		if err != nil {
			t.Fatalf("failed to create image record in db %s", err)
		}

		ss, err := ds.Fetch(im.ID)
		if err != nil {
			t.Fatalf("failed to fetch image record from db %s", err)
		}

		assert.Equal(t, im.ID, ss.ID)
	}
}
