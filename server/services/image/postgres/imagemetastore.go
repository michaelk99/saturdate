package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// CreateImageQuery create a image record in the DB
	CreateImageQuery = `INSERT INTO images (id, profile_id, url, url_fallback, description, approval_score, is_archived, created_at, updated_at) VALUES (:id, :profile_id, :url, :url_fallback, :description, :approval_score, :is_archived, :created_at, :updated_at);`
	// UpdateImageQuery update a image record in the db
	UpdateImageQuery = `UPDATE images SET description = :description, approval_score = :approval_score, updated_at = :updated_at WHERE id = :id;`
	// FetchImageQuery fetch a image record from the db
	FetchImageQuery = `SELECT * FROM images WHERE id = $1`
	// FetchAllImagesByCondition fetch all images by custom condition
	FetchAllImagesByCondition = `SELECT * FROM images WHERE`
	// DeleteImageQuery delete a image record from the db
	DeleteImageQuery = `DELETE FROM images WHERE id = $1`
)

// imageMetaStore is an implementation of the image.ImageStore interface
type imageMetaStore struct {
	db *sqlx.DB
}

// NewImageStore return a new instance of image.ImageStore
func NewImageMetaStore(db *sqlx.DB) image.ImageMetaStore {
	return &imageMetaStore{
		db: db,
	}
}

func (s imageMetaStore) Create(sel *image.Image) (*image.Image, error) {
	row, err := s.db.NamedExec(CreateImageQuery, sel)

	if err != nil {
		return nil, err
	}

	count, err := row.RowsAffected()

	switch {
	case count <= 0:
		return nil, fmt.Errorf("%d rows affected by create", count)
	case err != nil:
		return nil, err
	}

	return sel, nil
}

func (s imageMetaStore) Delete(ID string) error {
	res, err := s.db.Exec(DeleteImageQuery, ID)
	if err != nil {
		return err
	}

	count, err := res.RowsAffected()

	switch {
	case count <= 0:
		return fmt.Errorf("%d rows affected by delete", count)
	case err != nil:
		return err
	}

	return nil
}

func (s imageMetaStore) Update(sel *image.Image) (*image.Image, error) {
	_, err := s.Fetch(sel.ID)
	if err != nil {
		return nil, err
	}

	res, err := s.db.NamedExec(UpdateImageQuery, sel)
	if err != nil {
		return nil, err
	}

	count, err := res.RowsAffected()

	switch {
	case count <= 0:
		return nil, fmt.Errorf("%d rows affected by update", count)
	case err != nil:
		return nil, err
	}

	return nil, nil
}

func (s imageMetaStore) Fetch(ID string) (*image.Image, error) {
	sel := image.Image{}

	err := s.db.Get(&sel, FetchImageQuery, ID)
	if err != nil {
		return nil, err
	}

	return &sel, nil
}

func (s imageMetaStore) FetchAllByCondition(whereCondition string) ([]*image.Image, error) {
	images := []*image.Image{}
	query := fmt.Sprintf("%s %s", FetchAllImagesByCondition, whereCondition)
	err := s.db.Select(&images, query)
	if err != nil {
		return nil, err
	}

	return images, nil
}
