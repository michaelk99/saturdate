-- Connect to our database
\connect saturdate

-- Create image table
CREATE TABLE images (
    id varchar(255) PRIMARY KEY,
    profile_id varchar(255),
    url varchar(255) NOT NULL,
    url_fallback varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    approval_score float,
    is_archived boolean DEFAULT FALSE,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
