package image

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
)

// Client is an http client
type Client struct {
	c       *http.Client
	baseURL string
	path    string
	jwt     string
}

func getScheme() string {
	scheme := "https"
	if os.Getenv("ENV") != "production" {
		scheme = "http"
	}

	return scheme
}

// NewClient is a constructor for our client
func NewClient(jwt string) *Client {
	baseURL := os.Getenv("IMAGE_BASE_URL")
	if baseURL == "" {
		log.Println("Image: NewClient: IMAGE_BASE_URL env not found")
		// default to image.saturdate.co
		baseURL = "images.saturdate.co"
	}
	path := os.Getenv("IMAGE_CRUD_PATH")
	if path == "" {
		log.Println("Image: NewClient: IMAGE_CRUD_PATH env not found")
		// default to /api/v1/images
		path = "/api/v1/images"
	}
	c := &Client{
		c:       &http.Client{},
		baseURL: baseURL,
		path:    path,
		jwt:     jwt,
	}

	return c
}

// CreateImage posts an image to the image service
func (c *Client) CreateImage(fullPath string, data io.Reader) (*Image, error) {
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)

	fw, err := w.CreateFormFile("file", fullPath)
	if err != nil {
		return nil, err
	}

	if data != nil {
		tmp, err := ioutil.ReadAll(data)
		if err != nil {
			return nil, err
		}
		fw.Write(tmp)
	}

	w.Close()
	scheme := getScheme()
	url := fmt.Sprintf("%s://%s/%s", scheme, c.baseURL, c.path)
	req, err := http.NewRequest("POST", url, buf)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", w.FormDataContentType())
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.jwt))

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("CreateImage: Error status code: %d", resp.StatusCode)
	}

	var img *Image
	err = json.NewDecoder(resp.Body).Decode(&img)
	if err != nil {
		return nil, fmt.Errorf("CreateImage: failed to decode resp body: %s", resp.Body)
	}

	return img, nil
}

// GetImages get images by ids
func (c *Client) GetImages(IDs []string) ([]*Image, error) {
	scheme := getScheme()
	if len(IDs) == 0 {
		return nil, fmt.Errorf("GetImages: IDs is a required field")
	}

	req := &http.Request{
		URL: &url.URL{
			Scheme: scheme,
			Host:   c.baseURL,
			Path:   c.path,
		},
		Method: "GET",
	}

	query := req.URL.Query()
	for _, id := range IDs {
		query.Set("id", id)
	}

	req.URL.RawQuery = query.Encode()
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.jwt))

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetImage: Code: %d Status: %s", resp.StatusCode, resp.Status)
	}

	var imgs []*Image
	err = json.NewDecoder(resp.Body).Decode(&imgs)
	if err != nil {
		return nil, fmt.Errorf("GetImage: failed to decode resp body: %s", resp.Body)
	}

	return imgs, nil
}

// GetImage get image by id
func (c *Client) GetImage(ID string) (*Image, error) {
	scheme := getScheme()
	req := &http.Request{
		URL: &url.URL{
			Scheme: scheme,
			Host:   c.baseURL,
			Path:   fmt.Sprintf("%s/%s", c.path, ID),
		},
		Method: "GET",
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.jwt))

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GetImage: Code: %d Status: %s", resp.StatusCode, resp.Status)
	}

	var img *Image
	err = json.NewDecoder(resp.Body).Decode(&img)
	if err != nil {
		return nil, fmt.Errorf("GetImage: failed to decode resp body: %s", resp.Body)
	}

	return img, nil
}
