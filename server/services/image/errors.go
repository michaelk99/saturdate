package image

import (
	"net/http"
)

// ErrImageCreate error when creating image
type ErrImageCreate struct {
	msg error
}

//
type ErrProfileService struct {
	msg error
}

func (e ErrProfileService) Error() string {
	return e.msg.Error()
}

// implements the error interface
func (e ErrImageCreate) Error() string {
	return "could not create image record in db"
}

// ErrImageNotFound error when image not found in the db
type ErrImageNotFound struct {
	msg error
}

// implements the error interface
func (e ErrImageNotFound) Error() string {
	return "image record not found in db"
}

// ErrImageNotFound error when image not found in the db
type ErrImageNotUploaded struct {
	MSG error
}

// implements the error interface
func (e ErrImageNotUploaded) Error() string {
	return "image record not found in db"
}

// ServiceToHTTPErrorMap convert custom error types to http status codes
func ServiceToHTTPErrorMap(err error) (code int) {
	switch err.(type) {
	case ErrImageCreate:
		return http.StatusConflict
	case ErrImageNotFound:
		return http.StatusBadRequest
	case ErrImageNotUploaded:
		return http.StatusInternalServerError
	default:
		return http.StatusInternalServerError
	}
}
