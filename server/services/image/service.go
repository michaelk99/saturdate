package image

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"gopkg.in/guregu/null.v3"
)

// Service is a public interface for implementing the image service
type Service interface {
	Create(ctx context.Context, img []byte) (*Image, error)
	Update(imgMeta Image) (*Image, error)
	Delete(ctx context.Context, ID string) error
	Fetch(ID string) (*Image, error)
	FetchAll(query IDQuery) ([]*Image, error)
}

// service is an implementation of the Service interface
type service struct {
	// ds is the datastore, which is our database entrypoint
	ds ImageMetaStore
	// is i the image store which stores the binary image file
	is ImageStore
	// pc the profile client used to communicate with profile
	pc profile.Client
}

// NewService is a constructor for the service
func NewService(ds ImageMetaStore, is ImageStore, pc profile.Client) Service {
	return &service{
		ds: ds,
		is: is,
		pc: pc,
	}
}

func (s service) Create(ctx context.Context, img []byte) (*Image, error) {
	ID := uuid.New().String()

	session := ctx.Value("Session").(*token.Session)
	token := ctx.Value("Token").(string)

	imgMeta := Image{}
	imgMeta.ID = ID

	// !!! IMPORTANT !!!
	// Each image creation is associated with a `profile_id`. `profile_id` is
	// nullable though, so in the future if we ever wanted to support more
	// generic images not associated with a profile we can. For now, always
	// associate the image with the profile
	imgMeta.ProfileID = null.String{
		sql.NullString{
			String: session.ProfileID,
			Valid:  true,
		},
	}
	imgMeta.CreatedAt = time.Now().Format(time.RFC3339)
	imgMeta.UpdatedAt = time.Now().Format(time.RFC3339)

	// fields to override:
	imgMeta.ApprovalScore = 0

	// upload binary image
	uploadResp, err := s.is.Upload(ctx, ID, img)
	if err != nil {
		log.Printf("Failed to upload image to image store: %s", err)
		return nil, ErrImageCreate{}
	}
	imgMeta.URL = uploadResp.URL
	imgMeta.URLFallback = uploadResp.URLFallback

	// create metadata record
	_, err = s.ds.Create(&imgMeta)
	if err != nil {
		// TODO: cleanup image upload
		log.Printf("Failed to create image in db: %s", err)
		return nil, ErrImageCreate{}
	}

	// !!! IMPORTANT !!!
	// Must associate the image with the profile
	imgIDs := []string{imgMeta.ID}
	prof, err := s.pc.GetProfile(token, session.ProfileID)
	if err != nil {
		return nil, ErrProfileService{msg: err}
	}

	for _, v := range prof.Images {
		imgIDs = append(imgIDs, v)
	}

	prof.Images = pq.StringArray(imgIDs)

	_, err = s.pc.UpdateProfile(token, prof)
	if err != nil {
		log.Printf("Failed to update profile backend: %s", err)
		return nil, ErrImageCreate{}
	}

	return &imgMeta, nil
}

func (s service) Update(imgMeta Image) (*Image, error) {
	dbImage, err := s.ds.Fetch(imgMeta.ID)
	if err != nil {
		return nil, err
	}
	if dbImage == nil {
		return nil, ErrImageNotFound{}
	}

	// always update `UpdatedAt`
	imgMeta.UpdatedAt = time.Now().Format(time.RFC3339)

	// only attributes allowed to be updated are the following:
	// `approval_score`
	// `description`
	dbImage.ApprovalScore = imgMeta.ApprovalScore
	dbImage.Description = imgMeta.Description

	_, err = s.ds.Update(dbImage)
	if err != nil {
		return nil, err
	}

	return dbImage, nil
}

func (s service) Fetch(ID string) (*Image, error) {
	var imgMeta *Image
	var err error

	imgMeta, err = s.ds.Fetch(ID)
	if err != nil {
		return nil, err
	}
	if imgMeta == nil {
		return nil, ErrImageNotFound{}
	}

	return imgMeta, nil
}

func (s service) FetchAll(query IDQuery) ([]*Image, error) {
	var images []*Image
	var err error
	var whereCondition string

	switch query.Type {
	case IDs:
		whereCondition = fmt.Sprintf("id IN (%s)", query.Value)
	case SearchQuery:
		whereCondition = strings.Replace(query.Value, ",", "AND", -1)
	case ProfileID:
		whereCondition = fmt.Sprintf("profile_id = '%s' ORDER BY updated_at DESC", query.Value)
	default:
		return nil, fmt.Errorf("Invalid value for id query")
	}

	images, err = s.ds.FetchAllByCondition(whereCondition)
	if err != nil {
		return nil, err
	}

	return images, nil
}

func (s service) Delete(ctx context.Context, ID string) error {
	// !!! IMPORTANT !!!
	// Must remove the image that is associated with the profile
	session := ctx.Value("Session").(*token.Session)
	token := ctx.Value("Token").(string)

	// object in google storage is the path to the file
	// harcoded .jpg for now
	object := fmt.Sprintf("images/%s.jpg", ID)

	// delete the object from the bucket
	err := s.is.Delete(object)
	if err != nil {
		return err
	}

	// delete from the database
	err = s.ds.Delete(ID)
	if err != nil {
		return err
	}

	// get the session from the profile
	prof, err := s.pc.GetProfile(token, session.ProfileID)
	if err != nil {
		return ErrProfileService{msg: err}
	}
	var imgIDs []string

	for _, id := range prof.Images {
		// do not add this image to the array since we just deleted it
		if id == ID {
			continue
		}
		imgIDs = append(imgIDs, id)
	}

	prof.Images = pq.StringArray(imgIDs)

	_, err = s.pc.UpdateProfile(token, prof)
	if err != nil {
		log.Printf("Failed to update profile backend: %s", err)
		return ErrImageCreate{}
	}

	return nil
}
