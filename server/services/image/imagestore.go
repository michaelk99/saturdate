package image

import "context"

type UploadResp struct {
	URL         string
	URLFallback string
}

// Uploader uploads the binary image file to a storage location
type ImageStore interface {
	Upload(ctx context.Context, filename string, image []byte) (resp *UploadResp, err error)
	Delete(object string) error
}
