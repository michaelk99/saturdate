package image_test

import (
	"context"
	"fmt"
	gomock "github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/image"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func getContext() context.Context {
	ctx := context.Background()
	tk := &token.Session{}
	ctx = context.WithValue(ctx, "Session", tk)
	ctx = context.WithValue(ctx, "Token", "some-jwt")
	return ctx
}

func TestCreate(t *testing.T) {
	// create testing table
	var TestCreateTT = []struct {
		name        string
		createImage func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should throw error if upload fails",
			createImage: func(ctrl *gomock.Controller, t *testing.T) {
				ctx := getContext()

				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				// upload always gets called
				mockImageStore.EXPECT().Upload(gomock.Any(), gomock.Any(), gomock.Any()).Return([]interface{}{
					&image.UploadResp{
						URL:         "saturdate.co",
						URLFallback: "saturdate.co",
					}, fmt.Errorf("upload error"),
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Create(ctx, []byte("test image"))

				assert.NotNil(t, err)
				assert.IsType(t, image.ErrImageCreate{}, err)
			},
		}, {
			name: "should throw error if create fails",
			createImage: func(ctrl *gomock.Controller, t *testing.T) {
				ctx := getContext()

				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				// upload always gets called
				mockImageStore.EXPECT().Upload(gomock.Any(), gomock.Any(), gomock.Any()).Return([]interface{}{
					&image.UploadResp{
						URL:         "saturdate.co",
						URLFallback: "saturdate.co",
					}, nil,
				}...)

				mockImageMetaStore.EXPECT().Create(gomock.Any()).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						URL:         "saturdate.co",
						URLFallback: "saturdate.co",
					}, fmt.Errorf("create error"),
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Create(ctx, []byte("test image"))

				assert.NotNil(t, err)
				assert.IsType(t, image.ErrImageCreate{}, err)
			},
		}, {
			name: "should create image",
			createImage: func(ctrl *gomock.Controller, t *testing.T) {
				ctx := getContext()

				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				// upload always gets called
				mockImageStore.EXPECT().Upload(gomock.Any(), gomock.Any(), gomock.Any()).Return([]interface{}{
					&image.UploadResp{
						URL:         "saturdate.co",
						URLFallback: "saturdate.co",
					}, nil,
				}...)

				mockImageMetaStore.EXPECT().Create(gomock.Any()).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						URL:         "saturdate.co",
						URLFallback: "saturdate.co",
					}, nil,
				}...)

				mockProfileClient.EXPECT().GetProfile(gomock.Any(), gomock.Any()).Return([]interface{}{
					&profile.Profile{}, nil,
				}...)
				mockProfileClient.EXPECT().UpdateProfile(gomock.Any(), gomock.Any()).Return([]interface{}{
					&profile.Profile{}, nil,
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				img, err := s.Create(ctx, []byte("test image"))

				// handle when create passes
				assert.NotNil(t, img)
				assert.Nil(t, err)
				assert.NotEmpty(t, img.ID)
				assert.False(t, img.IsArchived)
				assert.NotEmpty(t, img.CreatedAt)
				assert.NotEmpty(t, img.UpdatedAt)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestCreateTT {
		tt.createImage(ctrl, t)
	}
}

func TestUpdate(t *testing.T) {
	// update testing table
	var TestUpdateTT = []struct {
		name        string
		updateImage func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should update image",
			updateImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        "static-id",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch always gets called
				mockImageMetaStore.EXPECT().Fetch(imgRequest.ID).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						ID: "db-id",
					}, nil,
				}...)

				// update mock
				mockImageMetaStore.EXPECT().Update(gomock.Any()).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						ID: "db-id",
					}, nil,
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				img, err := s.Update(imgRequest)

				// handle when create passes
				assert.NotNil(t, img)
				assert.Nil(t, err)
				// 	assert.Equal(t, tt.imgRequest.ID, img.ID)
				// 	assert.Equal(t, tt.imgRequest.UserUUID, img.UserUUID)
				// assert.Equal(t, tt.imgRequest.WorkoutUUID, img.WorkoutUUID)
				assert.Equal(t, imgRequest.IsArchived, img.IsArchived)
				// image UpdatedAt should change
				assert.NotEqual(t, imgRequest.UpdatedAt, img.UpdatedAt)
			},
		}, {
			name: "should throw error if update fails",
			updateImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        "static-id",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch always gets called
				mockImageMetaStore.EXPECT().Fetch(imgRequest.ID).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						ID: "db-id",
					}, nil,
				}...)

				// update mock
				mockImageMetaStore.EXPECT().Update(gomock.Any()).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						ID: "db-id",
					}, fmt.Errorf("update failed"),
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Update(imgRequest)

				assert.NotNil(t, err)
				assert.IsType(t, fmt.Errorf("update failed"), err)
			},
		}, {
			name: "should throw error if fetch fails",
			updateImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        "static-id",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch always gets called
				mockImageMetaStore.EXPECT().Fetch(imgRequest.ID).Return([]interface{}{
					// the result from our "mocked" database
					&image.Image{
						ID: "db-id",
					}, fmt.Errorf("fetch error"),
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Update(imgRequest)

				assert.NotNil(t, err)
				assert.IsType(t, fmt.Errorf("fetch error"), err)
			},
		}, {
			name: "should throw error if image not found",
			updateImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        "static-id",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch always gets called
				mockImageMetaStore.EXPECT().Fetch(imgRequest.ID).Return([]interface{}{
					// return a image not found err
					nil, nil,
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Update(imgRequest)

				assert.NotNil(t, err)
				assert.IsType(t, image.ErrImageNotFound{}, err)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestUpdateTT {
		tt.updateImage(ctrl, t)
	}
}

func TestDelete(t *testing.T) {
	// delete testing table
	var TestDeleteTT = []struct {
		name        string
		deleteImage func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should delete image",
			deleteImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        uuid.New().String(),
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// always gets called
				mockImageStore.EXPECT().Delete(gomock.Any()).Return(nil)

				mockImageMetaStore.EXPECT().Delete(imgRequest.ID).Return(nil)
				mockProfileClient.EXPECT().GetProfile(gomock.Any(), gomock.Any()).Return([]interface{}{
					&profile.Profile{}, nil,
				}...)
				mockProfileClient.EXPECT().UpdateProfile(gomock.Any(), gomock.Any()).Return([]interface{}{
					&profile.Profile{}, nil,
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				err := s.Delete(getContext(), imgRequest.ID)

				assert.Nil(t, err)
			},
		}, {
			name: "should throw error if delete fails",
			deleteImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        uuid.New().String(),
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// always gets called
				mockImageStore.EXPECT().Delete(gomock.Any()).Return(nil)

				mockImageMetaStore.EXPECT().Delete(imgRequest.ID).Return(fmt.Errorf("img store delete error"))

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				err := s.Delete(getContext(), imgRequest.ID)

				assert.NotNil(t, err)
				assert.IsType(t, fmt.Errorf("img store delete error"), err)
			},
		}, {
			name: "should throw error if img meta delete fails",
			deleteImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				imgRequest := image.Image{
					ID:        uuid.New().String(),
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// always gets called
				mockImageStore.EXPECT().Delete(gomock.Any()).Return(nil)
				mockImageMetaStore.EXPECT().Delete(imgRequest.ID).Return(fmt.Errorf("img meta delete error"))

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				err := s.Delete(getContext(), imgRequest.ID)

				assert.NotNil(t, err)
				assert.IsType(t, fmt.Errorf("img meta delete error"), err)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestDeleteTT {
		tt.deleteImage(ctrl, t)
	}
}

func TestFetch(t *testing.T) {
	// fetch testing table
	var TestFetchTT = []struct {
		name       string
		fetchImage func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should fetch image",
			fetchImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				img := image.Image{
					ID:        "copy-id-1",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch is always called
				mockImageMetaStore.EXPECT().Fetch(img.ID).Return([]interface{}{
					&image.Image{
						ID: "copy-id-1",
					}, nil,
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				imgMeta, err := s.Fetch(img.ID)

				assert.Nil(t, err)
				assert.Equal(t, img.ID, imgMeta.ID)
			},
		}, {
			name: "should throw error if img is nil",
			fetchImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				img := image.Image{
					ID:        "copy-id-1",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch is always called
				mockImageMetaStore.EXPECT().Fetch(img.ID).Return([]interface{}{
					nil, nil,
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Fetch(img.ID)

				assert.NotNil(t, err)
			},
		}, {
			name: "should throw error if img not found",
			fetchImage: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockImageMetaStore := image.NewMockImageMetaStore(ctrl)
				mockImageStore := image.NewMockImageStore(ctrl)
				mockProfileClient := profile.NewMockClient(ctrl)

				img := image.Image{
					ID:        "copy-id-1",
					CreatedAt: time.Now().Format(time.RFC3339),
					UpdatedAt: time.Now().Format(time.RFC3339),
				}

				// fetch is always called
				mockImageMetaStore.EXPECT().Fetch(img.ID).Return([]interface{}{
					&image.Image{}, image.ErrImageNotFound{},
				}...)

				// test service
				s := image.NewService(mockImageMetaStore, mockImageStore, mockProfileClient)
				_, err := s.Fetch(img.ID)

				assert.NotNil(t, err)
				assert.IsType(t, image.ErrImageNotFound{}, err)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestFetchTT {
		tt.fetchImage(ctrl, t)
	}
}
