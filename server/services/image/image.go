package image

import (
	"gopkg.in/guregu/null.v3"
)

// Image is a representation of a image record in the db
type Image struct {
	ID            string      `json:"id" db:"id"`
	ProfileID     null.String `json:"profile_id" db:"profile_id"`
	URL           string      `json:"url" db:"url"`
	URLFallback   string      `json:"url_fallback" db:"url_fallback"`
	Description   string      `json:"description" db:"description"`
	ApprovalScore float64     `json:"approval_score" db:"approval_score"`
	IsArchived    bool        `json:"is_archived" db:"is_archived"`
	CreatedAt     string      `json:"created_at" db:"created_at"`
	UpdatedAt     string      `json:"updated_at" db:"updated_at"`
}
