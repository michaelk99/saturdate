package handlers

import (
	"net/http"
	"path/filepath"
	"strings"

	"gitlab.com/michaelk99/saturdate/server/services/image"
)

// CRUD forwards request based on http method
func CRUD(s image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			// check between fetch and fetch all within GET, since you cannot
			// differentiate http verbs between the two
			path := filepath.Clean(r.URL.Path)
			id := strings.TrimPrefix(path, "/api/v1/images")
			if id == "" {
				FetchAll(s).ServeHTTP(w, r)
				return
			}
			Fetch(s).ServeHTTP(w, r)
			return
		case http.MethodPost:
			Create(s).ServeHTTP(w, r)
			return
		case http.MethodPatch:
			Update(s).ServeHTTP(w, r)
			return
		case http.MethodDelete:
			Delete(s).ServeHTTP(w, r)
			return
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}
	}
}
