package handlers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// CreateErrCode error code
	CreateErrCode = "image.create.error"
	// CreateExistsCode error code exists
	CreateExistsCode = "image.create.exists"
)

const (
	// maxFileSize is the largest file size we accept
	// 10 MB for now
	maxFileSize = 10 * 1024 * 1024
)

// Create sign up handler
func Create(service image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support POST
		if r.Method != http.MethodPost {
			log.Printf("image.create.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		// get context from request
		ctx := r.Context()

		// !!! WE ARE EXPECTING A FILE UPLOAD HERE !!!
		// file is saved in memory with maxFileSize memory allocated
		err := r.ParseMultipartForm(maxFileSize)
		if err != nil {
			log.Printf("image.create.invalid_file_size %s", err)
			http.Error(w, "invalid file size", http.StatusRequestEntityTooLarge)
			return
		}

		file, _, err := r.FormFile("file")
		if err != nil {
			log.Printf("image.create.invalid_file_for_request %s", err)
			http.Error(w, "invalid file: no file found in form", http.StatusBadRequest)
			return
		}
		defer file.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			log.Printf("image.create.invalid_file_on_read %v", err)
			http.Error(w, "invalid file on read", http.StatusBadRequest)
			return
		}

		// call our service to create meta object and upload binary file
		imgMeta, err := service.Create(ctx, fileBytes)
		if err != nil {
			resp := &je.Response{
				Code:    CreateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, image.ServiceToHTTPErrorMap(err))
			return
		}

		// return created image
		w.WriteHeader(http.StatusCreated) // must write status header before NewEcoder closes body
		err = json.NewEncoder(w).Encode(imgMeta)
		if err != nil {
			log.Printf("%s: %v", CreateErrCode, err)
			resp := &je.Response{
				Code:    CreateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusInternalServerError)
			return
		}

		log.Printf("successfully created image for ID %s", imgMeta.ID)
		return
	}
}
