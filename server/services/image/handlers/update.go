package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"path/filepath"

	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// UpdateErrCode code
	UpdateErrCode = "image.update.error"
	// UpdateExistsCode code
	UpdateExistsCode = "image.update.exists"
)

// Update checks email against password and assigns a token if valid
func Update(s image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support PATCH
		if r.Method != http.MethodPatch {
			log.Printf("image.update.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/images/")

		if id == "" {
			log.Printf("image.update.invalid_id")
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		var img image.Image
		err := json.NewDecoder(r.Body).Decode(&img)

		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// set the id to the id in the request
		dbImage, err := s.Update(img)
		if err != nil {
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, image.ServiceToHTTPErrorMap(err))
			return
		}

		// return created image
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(dbImage)
		if err != nil {
			log.Printf("%s: %v", UpdateErrCode, err)
			resp := &je.Response{
				Code:    UpdateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully updated image id %s", img.ID)
		return
	}
}
