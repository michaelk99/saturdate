package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// FetchAllErrCode code
	FetchAllErrCode = "image.fetchall.error"
	// FetchAllExistsCode code
	FetchAllExistsCode = "image.fetchall.exists"
)

// FetchAll checks email against password and assigns a token if valid
func FetchAll(s image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("image.fetchall.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		session := r.Context().Value("Session").(*token.Session)

		idQuery := image.IDQuery{
			Type:  image.ProfileID,
			Value: session.ProfileID,
		}

		imgs, err := s.FetchAll(idQuery)
		if err != nil {
			resp := &je.Response{
				Code:    FetchAllErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, image.ServiceToHTTPErrorMap(err))
			return
		}

		// return created image
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(imgs)
		if err != nil {
			log.Printf("%s: %v", FetchAllErrCode, err)
			resp := &je.Response{
				Code:    FetchAllErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully fetched all images for profile id %s", session.ProfileID)
		return
	}
}
