package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// SearchErrCode code
	SearchErrCode = "image.search.error"
	// SearchExistsCode code
	SearchExistsCode = "image.search.exists"
)

// Search checks email against password and assigns a token if valid
func Search(s image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("image.search.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		r.ParseForm()
		var ids []string
		for key, value := range r.Form {
			if key == "id" {
				for _, val := range value {
					ids = append(ids, fmt.Sprintf("'%s'", val))
				}
			}
		}

		if ids == nil || len(ids) == 0 {
			resp := &je.Response{
				Code:    FetchAllErrCode,
				Message: "Invalid query params, must include IDs",
			}
			je.Error(r, w, resp, 400)
			return
		}

		idQuery := image.IDQuery{
			Type:  image.IDs,
			Value: strings.Join(ids[:], ","),
		}

		dbImages, err := s.FetchAll(idQuery)
		if err != nil {
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, image.ServiceToHTTPErrorMap(err))
			return
		}

		// return created image
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(dbImages)
		if err != nil {
			log.Printf("%s: %v", SearchErrCode, err)
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Println("successfully searched all images based on criteria")
		return
	}
}
