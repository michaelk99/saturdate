package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"path/filepath"

	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// FetchErrCode code
	FetchErrCode = "image.fetch.error"
	// FetchExistsCode code
	FetchExistsCode = "image.fetch.exists"
)

// Fetch checks email against password and assigns a token if valid
func Fetch(s image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("image.fetch.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/images/")
		if id == "" {
			log.Printf("image.fetch.invalid_id")
			http.Error(w, "invalid request, need id or account id", http.StatusBadRequest)
			return
		}

		img, err := s.Fetch(id)
		if err != nil {
			resp := &je.Response{
				Code:    FetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, image.ServiceToHTTPErrorMap(err))
			return
		}

		// return created image
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(img)
		if err != nil {
			log.Printf("%s: %v", FetchErrCode, err)
			resp := &je.Response{
				Code:    FetchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully fetched image id %s", id)
		return
	}
}
