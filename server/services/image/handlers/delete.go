package handlers

import (
	"log"
	"net/http"

	"path/filepath"
	"strings"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/services/image"
)

const (
	// DeleteErrCode code
	DeleteErrCode = "image.delete.error"
)

// Delete checks email against password and assigns a token if valid
func Delete(s image.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support DELETE
		if r.Method != http.MethodDelete {
			log.Printf("image.delete.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		// look for the image id in the path - required
		path := filepath.Clean(r.URL.Path)
		id := strings.TrimPrefix(path, "/api/v1/images/")

		if id == "" {
			log.Printf("image.delete.no_id")
			http.Error(w, "invalid id", http.StatusBadRequest)
			return
		}

		ctx := r.Context()
		err := s.Delete(ctx, id)
		if err != nil {
			resp := &je.Response{
				Code:    DeleteErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, image.ServiceToHTTPErrorMap(err))
			return
		}

		// return deleted image
		w.WriteHeader(http.StatusOK)
		log.Printf("successfully deleted image id %s", id)
		return
	}
}
