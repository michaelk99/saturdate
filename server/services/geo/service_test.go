package geo_test

import (
	"fmt"
	gomock "github.com/golang/mock/gomock"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSetLatLon(t *testing.T) {
	var TestSetLatLonTT = []struct {
		name string
		set  func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should set lat lon",
			set: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockGeoStore := geo.NewMockGeoStore(ctrl)

				req := geo.GeoRequest{
					Latitude:  1.001,
					Longitude: 1.001,
					ProfileID: "1",
				}

				// should always expect fetchByAccountID to be called
				mockGeoStore.EXPECT().SetLatLon(req).Return(nil)

				// test service
				s := geo.NewService(mockGeoStore)
				err := s.SetLatLon(req)

				// handle when set passes
				assert.Nil(t, err)
			},
		}, {
			name: "should throw error if set lat lon fails",
			set: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockGeoStore := geo.NewMockGeoStore(ctrl)

				req := geo.GeoRequest{
					Latitude:  1.001,
					Longitude: 1.001,
					ProfileID: "1",
				}

				// should always expect fetchByAccountID to be called
				mockGeoStore.EXPECT().SetLatLon(req).Return(fmt.Errorf("Error with set"))

				// test service
				s := geo.NewService(mockGeoStore)
				err := s.SetLatLon(req)

				assert.NotNil(t, err)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestSetLatLonTT {
		tt.set(ctrl, t)
	}
}

func TestSearch(t *testing.T) {
	var TestSearchTT = []struct {
		name   string
		search func(ctrl *gomock.Controller, t *testing.T)
	}{
		{
			name: "should search correctly",
			search: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockGeoStore := geo.NewMockGeoStore(ctrl)

				req := geo.GeoRequest{
					Latitude:  1.001,
					Longitude: 1.001,
					ProfileID: "1",
				}

				var rad float64
				rad = 30

				// should always expect fetchByAccountID to be called
				mockGeoStore.EXPECT().Search(req, rad).Return([]interface{}{
					[]*geo.GeoResponse{
						&geo.GeoResponse{
							Latitude:  1,
							Longitude: 1,
							Dist:      1,
						},
					}, nil,
				}...)

				// test service
				s := geo.NewService(mockGeoStore)
				_, err := s.Search(req, 30)

				// handle when create passes
				assert.Nil(t, err)
			},
		}, {
			name: "should throw error if search fails",
			search: func(ctrl *gomock.Controller, t *testing.T) {
				// setup for testing
				mockGeoStore := geo.NewMockGeoStore(ctrl)

				req := geo.GeoRequest{
					Latitude:  1.001,
					Longitude: 1.001,
					ProfileID: "1",
				}

				var rad float64
				rad = 30

				// should always expect fetchByAccountID to be called
				mockGeoStore.EXPECT().Search(req, rad).Return([]interface{}{
					nil,
					fmt.Errorf("Error searching in redis"),
				}...)

				// test service
				s := geo.NewService(mockGeoStore)
				_, err := s.Search(req, 30)

				assert.NotNil(t, err)
			},
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	for _, tt := range TestSearchTT {
		tt.search(ctrl, t)
	}
}
