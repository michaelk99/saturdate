package redis

import (
	"github.com/go-redis/redis"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
)

// geoStore is a private implementation of the geo.GeoStore interface
type geoStore struct {
	client *redis.Client
	key    string
}

// NewGeoStore returns a redis implementation of the geo.GeoStore interface
func NewGeoStore(client *redis.Client) geo.GeoStore {
	return &geoStore{
		client: client,
		key:    "geo",
	}
}

func (g *geoStore) SetLatLon(data geo.GeoRequest) error {
	// add geo to the redis store
	cmd := g.client.GeoAdd(g.key, &redis.GeoLocation{
		Longitude: data.Longitude,
		Latitude:  data.Latitude,
		Name:      data.ProfileID,
	})

	if cmd.Err() != nil {
		return cmd.Err()
	}

	return nil
}

func (g *geoStore) Search(data geo.GeoRequest, radius float64) ([]*geo.GeoResponse, error) {
	cmd := g.client.GeoRadius(g.key, data.Longitude, data.Latitude, &redis.GeoRadiusQuery{
		Radius: radius,
		// unit is always miles
		Unit:        "mi",
		WithGeoHash: true,
		WithCoord:   true,
		WithDist:    true,
	})

	locations, err := cmd.Result()
	if err != nil {
		return nil, err
	}

	// convert location results into geo data results
	var results []*geo.GeoResponse
	for _, l := range locations {
		results = append(results, &geo.GeoResponse{
			Latitude:  l.Latitude,
			Longitude: l.Longitude,
			ProfileID: l.Name,
			Dist:      l.Dist,
			GeoHash:   l.GeoHash,
		})
	}

	return results, nil
}
