// +build integration

package redis_test

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	geostore "gitlab.com/michaelk99/saturdate/server/services/geo/redis"
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

var Client *redis.Client

func SetUpClient(t *testing.T) (*redis.Client, error) {
	if Client != nil {
		return Client, nil
	}

	Client = redis.NewClient(&redis.Options{
		Addr:     "0.0.0.0:6379",
		Password: "",
		DB:       0,
	})

	_, err := Client.Ping().Result()
	if err != nil {
		fmt.Printf("Error connecting to redis client %s", err)
	}

	return Client, nil
}

func Setup(t *testing.T) (geo.GeoStore, func()) {
	_, err := SetUpClient(t)
	if err != nil {
		t.Fatalf("setup: could not create redis connection: %s", err)
	}
	ds := geostore.NewGeoStore(Client)

	return ds, func() {
		_ = Client.FlushAll()
	}
}

var TestSetLatLonTT = []geo.GeoRequest{
	geo.GeoRequest{
		ProfileID: uuid.New().String(),
		Latitude:  1.1,
		Longitude: -1.1,
	},
}

// Test setting lat long in database properly
func TestSetLatLon(t *testing.T) {
	ds, flushall := Setup(t)
	defer flushall()

	for _, g := range TestSetLatLonTT {
		err := ds.SetLatLon(g)
		if err != nil {
			t.Fatalf("Failed to insert lat/long in redis: %v", err)
		}

		cmd := Client.GeoRadius("geo", g.Longitude, g.Latitude, &redis.GeoRadiusQuery{
			// 30 mile radius
			Radius: 30,
			// unit is always miles
			Unit:        "mi",
			WithGeoHash: true,
			WithCoord:   true,
			WithDist:    true,
			// 100 entries returned
			Count: 100,
		})

		locations, err := cmd.Result()
		if err != nil {
			t.Fatalf("Failed geo search %v", err)
		}

		for _, l := range locations {
			assert.Equal(t, math.Round(l.Latitude), math.Round(g.Latitude))
			assert.Equal(t, math.Round(l.Longitude), math.Round(g.Longitude))
			assert.Equal(t, l.Name, g.ProfileID)
		}
	}
}

var TestSearchTT = []struct {
	geoRequest geo.GeoRequest
	radius     float64
	shouldFind bool
}{
	{
		geo.GeoRequest{
			Latitude:  -73.987427,
			Longitude: 40.866032,
			ProfileID: "1",
		},
		30,
		true,
	},
	{
		geo.GeoRequest{
			Latitude:  40.866032,
			Longitude: -73.987427,
			ProfileID: "2",
		},
		30,
		true,
	},
	{
		geo.GeoRequest{
			Latitude:  40.866032,
			Longitude: -73.987427,
			ProfileID: "2",
		},
		30,
		false,
	},
}

func TestSearch(t *testing.T) {
	ds, flushall := Setup(t)
	defer flushall()

	for _, g := range TestSearchTT {
		if g.shouldFind {
			Client.GeoAdd("geo", &redis.GeoLocation{
				Longitude: g.geoRequest.Longitude,
				Latitude:  g.geoRequest.Latitude,
				Name:      g.geoRequest.ProfileID,
			})
		} else {
			Client.GeoAdd("geo", &redis.GeoLocation{
				Longitude: g.geoRequest.Longitude + 10,
				Latitude:  g.geoRequest.Latitude + 10,
				Name:      g.geoRequest.ProfileID,
			})
		}

		results, err := ds.Search(g.geoRequest, g.radius)

		if err != nil {
			t.Fatalf("Failed to geo search in redis: %v", err)
		}

		if !g.shouldFind {
			assert.Equal(t, 0, len(results))
			continue
		}

		// essentially fetching the record we just inserted above. Should
		// only be size 1
		for _, r := range results {
			assert.Equal(t, math.Round(g.geoRequest.Longitude), math.Round(r.Longitude))
			assert.Equal(t, math.Round(g.geoRequest.Latitude), math.Round(r.Latitude))
			assert.Equal(t, g.geoRequest.ProfileID, r.ProfileID)
		}
	}
}
