package geo

import (
	"net/http"
)

// ErrGeoCreate is returned when geo cannot be created
type ErrGeoCreate struct {
	msg error
}

func (e ErrGeoCreate) Error() string {
	return "could not create geo data"
}

// ServiceToHTTPErrorMap maps the geos service's errors to http
func ServiceToHTTPErrorMap(err error) (code int) {
	switch err.(type) {
	case ErrGeoCreate:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
