package geo

import (
	"fmt"
)

// Service is a public interface for implementing our Geo service
type Service interface {
	SetLatLon(g GeoRequest) error
	Search(g GeoRequest, radius float64) ([]*GeoResponse, error)
}

type service struct {
	ds GeoStore
}

// NewService is a constructor for our Geo service implementation
func NewService(ds GeoStore) Service {
	return &service{
		ds: ds,
	}
}

func (s *service) SetLatLon(g GeoRequest) error {
	err := s.ds.SetLatLon(g)
	if err != nil {
		fmt.Printf("Failed to set lat long in db %s", err)
		return err
	}
	return nil
}

func (s *service) Search(g GeoRequest, radius float64) ([]*GeoResponse, error) {
	results, err := s.ds.Search(g, radius)
	if err != nil {
		fmt.Printf("Failed to search store %s", err)
		return nil, err
	}
	return results, nil
}
