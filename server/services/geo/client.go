package geo

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// Client interface
type Client interface {
	Search(jwt string, lat float64, long float64, radius int) ([]*GeoResponse, error)
}

// Client is an http client
type client struct {
	c      *http.Client
	geoURL *url.URL
}

// NewClient is a constructor for our client
func NewClient(geoURL *url.URL) Client {
	c := &client{
		c:      &http.Client{},
		geoURL: geoURL,
	}

	return c
}

// Search search profiles based on geo location :)
func (c *client) Search(jwt string, lat float64, long float64, radius int) ([]*GeoResponse, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/%s", c.geoURL.String(), "search"), nil)

	if err != nil {
		log.Printf("Error with new request %s", err)
		return nil, err
	}

	// convert to strings
	latString := strconv.FormatFloat(lat, 'f', -1, 64)
	longString := strconv.FormatFloat(long, 'f', -1, 64)
	radString := strconv.Itoa(radius)

	// pass in fields as query params
	q := req.URL.Query()
	q.Add("lat", latString)
	q.Add("lon", longString)
	q.Add("radius", radString)
	req.URL.RawQuery = q.Encode()

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", jwt))

	resp, err := c.c.Do(req)
	if err != nil {
		log.Printf("Error retrieving geo info %s", err)
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Search Status Code Error: Code: %d Status: %s", resp.StatusCode, resp.Status)
	}

	var results []*GeoResponse
	err = json.NewDecoder(resp.Body).Decode(&results)
	if err != nil {
		return nil, fmt.Errorf("Search: failed to decode resp body: %s", resp.Body)
	}

	return results, nil
}
