package geo

// GeoResponse geo response
type GeoResponse struct {
	ProfileID string  `json:"profile_id"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Dist      float64 `json:"dist"`
	GeoHash   int64   `json:"geo_hash"`
}
