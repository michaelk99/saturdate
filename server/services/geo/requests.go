package geo

// GeoRequest defines a geo request made from the client
type GeoRequest struct {
	ProfileID string  `validate:"required" json:"profile_id"`
	Latitude  float64 `validate:"required,gte=-90,lte=90" json:"latitude"`
	Longitude float64 `validate:"required,gte=-180,lte=180" json:"longitude"`
}
