package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	"strconv"
)

const (
	// SearchErrCode error code
	SearchErrCode = "geo.search.error"
	// SearchBadDataCode bad data
	SearchBadDataCode = "geo.search.bad_data"
)

// Search sign up handler
func Search(v validator.Validator, gs geo.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support GET
		if r.Method != http.MethodGet {
			log.Printf("geo.search.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		// get lat from query param
		if r.URL.Query().Get("lat") == "" {
			log.Printf("%s: %v", SearchBadDataCode, "Missing lat field")
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: "Missing lat field",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// get lon from query param
		if r.URL.Query().Get("lon") == "" {
			log.Printf("%s: %v", SearchBadDataCode, "Missing lon field")
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: "Missing lon field",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// default radius set to 30 miles
		var radius float64
		radius = 30
		if r.URL.Query().Get("radius") != "" {
			rad := r.URL.Query().Get("radius")
			radi, err := strconv.ParseFloat(rad, 64)
			// make sure radius is in between accepted boundary, 15-50
			if err == nil {
				if radi >= 15 && radi <= 50 {
					radius = radi
				}
			}
		}

		lat, err := strconv.ParseFloat(r.URL.Query().Get("lat"), 64)

		if err != nil {
			log.Printf("%s: %v", SearchBadDataCode, "Invalid lat field")
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: "Invalid lat field",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		lon, err := strconv.ParseFloat(r.URL.Query().Get("lon"), 64)

		if err != nil {
			log.Printf("%s: %v", SearchBadDataCode, "Invalid lon field")
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: "Invalid lon field",
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// for vaidation purposes, include the profileID in the struct
		session := r.Context().Value("Session").(*token.Session)

		body := geo.GeoRequest{
			Latitude:  lat,
			Longitude: lon,
			ProfileID: session.ProfileID,
		}

		// validate geo
		ok, fieldErrors := v.Struct(body)
		if !ok {
			resp := &je.Response{
				Code:       SearchBadDataCode,
				Message:    SearchBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		results, err := gs.Search(body, radius)
		if err != nil {
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, geo.ServiceToHTTPErrorMap(err))
			return
		}

		// return 200
		w.WriteHeader(http.StatusOK)
		err = json.NewEncoder(w).Encode(results)
		if err != nil {
			log.Printf("%s: %v", SearchErrCode, err)
			resp := &je.Response{
				Code:    SearchErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		log.Printf("successfully searched geo for profile ID %s, Lat: %f, Long: %f", session.ProfileID, body.Latitude, body.Longitude)
		return
	}
}
