package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	je "gitlab.com/michaelk99/saturdate/server/pkg/jsonerr"
	"gitlab.com/michaelk99/saturdate/server/pkg/token"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
)

const (
	// CreateErrCode error code
	CreateErrCode = "geo.create.error"
	// CreateExistsCode error code exists
	CreateExistsCode = "geo.create.exists"
	// CreateBadDataCode bad data
	CreateBadDataCode = "geo.create.bad_data"
)

// Create sign up handler
func Create(v validator.Validator, gs geo.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// only support POST
		if r.Method != http.MethodPost {
			log.Printf("geo.create.method_not_supported")
			http.Error(w, "method not supported", http.StatusMethodNotAllowed)
			return
		}

		var body geo.GeoRequest
		err := json.NewDecoder(r.Body).Decode(&body)
		if err != nil {
			log.Printf("%s: %v", CreateErrCode, err)
			resp := &je.Response{
				Code:    CreateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		// override the profile ID with the session profileID
		session := r.Context().Value("Session").(*token.Session)
		body.ProfileID = session.ProfileID

		// validate geo
		ok, fieldErrors := v.Struct(body)
		if !ok {
			resp := &je.Response{
				Code:       CreateBadDataCode,
				Message:    CreateBadDataCode,
				Additional: fieldErrors,
			}
			je.Error(r, w, resp, http.StatusBadRequest)
			return
		}

		err = gs.SetLatLon(body)
		if err != nil {
			resp := &je.Response{
				Code:    CreateErrCode,
				Message: err.Error(),
			}
			je.Error(r, w, resp, geo.ServiceToHTTPErrorMap(err))
			return
		}

		// return 200
		w.WriteHeader(http.StatusCreated)

		log.Printf("successfully created geo for profile ID %s, Lat: %f, Long: %f", session.ProfileID, body.Latitude, body.Longitude)
		return
	}
}
