package geo

// GeoStore interface
type GeoStore interface {
	SetLatLon(g GeoRequest) error
	Search(g GeoRequest, radius float64) ([]*GeoResponse, error)
}
