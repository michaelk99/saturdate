CREATE DATABASE saturdate;

\connect saturdate;

-- Create user table
CREATE TABLE account (
    id varchar(255) PRIMARY KEY,
    email varchar(255) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    enabled boolean DEFAULT TRUE,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create bars table
CREATE TABLE bars (
    id varchar(255) PRIMARY KEY,
    bar_name varchar(255),
    bar_category varchar(255),
    bar_price_range varchar(255),
    bar_number_reviews varchar(255),
    bar_number_stars varchar(255),
    bar_hood varchar(255),
    bar_phone varchar(255),
    bar_reservations varchar(255),
    bar_credit_card varchar(255),
    bar_parking varchar(255),
    bar_weel_chair varchar(255),
    bar_attire varchar(255),
    bar_dancing varchar(255),
    bar_happy varchar(255),
    bar_outdoor varchar(255),
    bar_tv varchar(255),
    bar_dogs varchar(255),
    bar_pool_table varchar(255),
    latitude float,
    longitude float,
    image_url varchar(255),
    yelp_url varchar(255),
    address varchar(255),
    address_display varchar(255),
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create events table
CREATE TABLE events (
    id varchar(255) PRIMARY KEY,
    match_id varchar(255) NOT NULL,
    profiles text[] NOT NULL,
    notified text[],
    turn varchar(255),
    state int,
    bars json,
    start_date timestamp NOT NULL,
    start_time time NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create image table
CREATE TABLE images (
    id varchar(255) PRIMARY KEY,
    profile_id varchar(255),
    url varchar(255) NOT NULL,
    url_fallback varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    approval_score float,
    is_archived boolean DEFAULT FALSE,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create blacklist table
CREATE TABLE blacklist (
    id varchar(255) PRIMARY KEY,
    profile_id varchar(255) NOT NULL,
    profile_id_blacklisted varchar(255) NOT NULL,
    expires_at timestamp NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create matches table
CREATE TABLE matches (
    id varchar(255) PRIMARY KEY,
    profiles text[] NOT NULL,
    notified text[],
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create matches_missed table
CREATE TABLE matches_missed (
    id varchar(255) PRIMARY KEY,
    profiles text[] NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);

-- Create user table
CREATE TABLE profile (
    id varchar(255) PRIMARY KEY,
    account_id varchar(255),
    images text[],
    name varchar(255),
    gender int,
    birthday timestamp,
    preference_sex int,
    preference_min_age int,
    preference_max_age int,
    city varchar(255),
    state varchar(255),
    zipcode varchar(255),
    latitude float,
    longitude float,
    neighborhood varchar(255),
    created_at timestamp NOT NULL,
    updated_at timestamp NOT NULL
);
