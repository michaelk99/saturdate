package main

import (
	"github.com/crgimenes/goconfig"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
	clients "gitlab.com/michaelk99/saturdate/server/pkg/client"
	mw "gitlab.com/michaelk99/saturdate/server/pkg/middleware"
	"gitlab.com/michaelk99/saturdate/server/pkg/token/jwthmac"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	"gitlab.com/michaelk99/saturdate/server/services/match"
	"gitlab.com/michaelk99/saturdate/server/services/match/handlers"
	"gitlab.com/michaelk99/saturdate/server/services/match/postgres"
	// mc = matchcache
	mc "gitlab.com/michaelk99/saturdate/server/services/match/redis"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	v9 "gopkg.in/go-playground/validator.v9"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
)

// Config this struct is using the goconfig library for simple flag and env var
// parsing. See: https://github.com/crgimenes/goconfig
type Config struct {
	HTTPListenAddr string `cfgDefault:"0.0.0.0:8004" cfg:"MATCH_HTTP_LISTEN_ADDR"`
	JWTSecret      string `cfgDefault:"fde5247c0262798a9c" cfg:"JWT_SECRET"`
	PGConnString   string `cfgDefault:"host=localhost port=5432 user=postgres dbname=saturdate  sslmode=disable" cfg:"POSTGRES_CONN_STRING"`
	RedisAddr      string `cfgDefault:"redis:6379" cfg:"REDIS_ADDR"`
	RedisPass      string `cfgDefault:"" cfg:"REDIS_PASS"`
	PGDriver       string `cfgDefault:"postgres" cfg:"POSTGRES_DRIVER"`
	ProfileURL     string `cfgDefault:"http://profile.saturdate/server.local/api/v1/profile" cfg:"PROFILE_URL"`
	GeoURL         string `cfgDefault:"http://geo.saturdate/server.local/api/v1/geo" cfg:"GEO_URL"`
	EventURL       string `cfgDefault:"http://event.saturdate/server.local/api/v1/events" cfg:"EVENT_URL"`
}

// root is the root route, used for k8s health checks
func root(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "match service healthy")
}

func main() {
	// parse our config
	conf := Config{}
	err := goconfig.Parse(&conf)
	if err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// create token store, which will be used to handle jwt authentication
	jwthmacStore := jwthmac.NewTokenStore([]byte(conf.JWTSecret), "HMAC")

	v9Validator := v9.New()
	validator := validator.NewValidator(v9Validator)

	// create conn to db
	dbConn, err := sqlx.Connect(conf.PGDriver, conf.PGConnString)
	if err != nil {
		log.Fatalf("failed to connect to DB: %v", err)
	}

	// create conn to redis
	client := redis.NewClient(&redis.Options{
		Addr:     conf.RedisAddr,
		Password: conf.RedisPass,
		DB:       0,
	})

	_, err = client.Ping().Result()
	if err != nil {
		log.Fatalf("failed to connect to redis: %v", err)
	}

	// create match store cache (redis under the hood)
	cache := mc.NewMatchStoreCache(client)

	// create match store (postgres under the hood)
	ds := postgres.NewMatchStore(dbConn)

	profURL, err := url.Parse(conf.ProfileURL)
	if err != nil {
		log.Fatal("failed to parse profile url")
	}

	pc := profile.NewClient(profURL)

	geoURL, err := url.Parse(conf.GeoURL)
	if err != nil {
		log.Fatal("failed to parse geo url")
	}

	gc := geo.NewClient(geoURL)

	eventURL, err := url.Parse(conf.EventURL)
	if err != nil {
		log.Fatal("failed to parse event url")
	}

	ec := clients.NewEventClient(eventURL)

	// create our service
	service := match.NewService(ds, cache, gc, pc, ec)

	// create our http mux and add routes
	mux := http.NewServeMux()

	mux.Handle("/api/v1/matches/", mw.Auth(jwthmacStore, handlers.CRUD(validator, service)))

	mux.Handle("/api/v1/matches/candidates/", mw.Auth(jwthmacStore, handlers.Candidates(validator, service)))

	mux.Handle("/api/v1/matches/swipe/", mw.Auth(jwthmacStore, handlers.Swipe(validator, service)))

	// not found route
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			io.WriteString(w, "Route not found\n")
			return
		}
		root(w, r)
	})

	// create server and launch in go routine
	s := http.Server{
		Addr:    conf.HTTPListenAddr,
		Handler: mux,
	}

	sigChan := make(chan os.Signal)
	errChan := make(chan error)

	go func(errChan chan error, s http.Server) {
		log.Printf("launching http server on %v", s.Addr)
		err := s.ListenAndServe()
		if err != nil {
			errChan <- err
		}
	}(errChan, s)

	// register signal handler
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// block on sigChan or errChan
	select {
	case sig := <-sigChan:
		log.Printf("received signal %v. attempting graceful shutdown of server", sig.String())

		err = s.Shutdown(nil)
		if err != nil {
			log.Printf("graceful shutdown of server unsuccessful: %v", err)
			os.Exit(1)
		}

		log.Printf("graceful shutdown of server successful")
		os.Exit(0)
	case e := <-errChan:
		log.Printf("failed to launched http server: %v", e)
		os.Exit(1)
	}
}
