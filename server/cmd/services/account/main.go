package main

import (
	mw "gitlab.com/michaelk99/saturdate/server/pkg/middleware"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	v9 "gopkg.in/go-playground/validator.v9"

	"github.com/crgimenes/goconfig"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/michaelk99/saturdate/server/pkg/token/jwthmac"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/account"
	"gitlab.com/michaelk99/saturdate/server/services/account/handlers"
	ajwthmac "gitlab.com/michaelk99/saturdate/server/services/account/jwthmac"
	"gitlab.com/michaelk99/saturdate/server/services/account/postgres"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
)

// Config this struct is using the goconfig library for simple flag and env var
// parsing. See: https://github.com/crgimenes/goconfig
type Config struct {
	HTTPListenAddr string `cfgDefault:"0.0.0.0:8000" cfg:"ACCOUNT_HTTP_LISTEN_ADDR" cfgHelper:"http listen address in the form of host:port"`
	PGConnString   string `cfgDefault:"host=localhost port=5432 user=postgres dbname=saturdate  sslmode=disable" cfg:"POSTGRES_CONN_STRING" cfgHelper:"a valid postgres connection string"`
	PGDriver       string `cfgDefault:"postgres" cfg:"POSTGRES_DRIVER"`
	// JWT Exp set to > 2 years b/c we lazy ⌒°(ᴖ◡ᴖ)°⌒
	JWTExp     int64  `cfgDefault:"65920000000" cfg:"JWT_EXP"`
	JWTSecret  string `cfgDefault:"fde5247c0262798a9c" cfg:"JWT_SECRET"`
	JWTIssuer  string `cfgDefault:"account" cfg:"JWT_ISSUER"`
	ProfileURL string `cfgDefault:"http://profile.saturdate/server.local/api/v1/profile" cfg:"PROFILE_URL"`
}

// root is the root route, used for k8s health checks
func root(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "healthy")
}

func main() {
	// parse our config
	conf := Config{}
	err := goconfig.Parse(&conf)
	if err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// create token store, which will be used to handle jwt authentication
	jwthmacStore := jwthmac.NewTokenStore([]byte(conf.JWTSecret), "HMAC")

	// create conn to db
	dbConn, err := sqlx.Connect(conf.PGDriver, conf.PGConnString)
	if err != nil {
		log.Fatalf("failed to connect to DB: %v", err)
	}
	ds := postgres.NewAccountStore(dbConn)

	// create token creator we will use to isssue tokens via login requests
	exp := time.Duration(conf.JWTExp) * time.Millisecond
	tc := ajwthmac.NewCreator(conf.JWTSecret, conf.JWTIssuer, exp)

	profURL, err := url.Parse(conf.ProfileURL)
	if err != nil {
		log.Fatalf("Profile URL is not a valid url %s", profURL)
	}
	pc := profile.NewClient(profURL)

	// create validator
	v9Validator := v9.New()
	validator := validator.NewValidator(v9Validator)

	// create our service
	service := account.NewService(ds, tc, pc)

	// create our http mux and add routes
	mux := http.NewServeMux()

	mux.Handle("/api/v1/account/", mw.Auth(jwthmacStore, handlers.CRUD(service)))
	mux.Handle("/api/v1/account/signup/", handlers.SignUp(validator, service))
	mux.Handle("/api/v1/account/login/", handlers.Login(service))

	// not found route
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			io.WriteString(w, "Route not found\n")
			return
		}
		root(w, r)
	})

	// create server and launch in go routine
	s := http.Server{
		Addr:    conf.HTTPListenAddr,
		Handler: mux,
	}

	sigChan := make(chan os.Signal)
	errChan := make(chan error)

	go func(errChan chan error, s http.Server) {
		log.Printf("launching http server on %v", s.Addr)
		err := s.ListenAndServe()
		if err != nil {
			errChan <- err
		}
	}(errChan, s)

	// register signal handler
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// block on sigChan or errChan
	select {
	case sig := <-sigChan:
		log.Printf("received signal %v. attempting graceful shutdown of server", sig.String())

		err = s.Shutdown(nil)
		if err != nil {
			log.Printf("graceful shutdown of server unsuccessful: %v", err)
			os.Exit(1)
		}

		log.Printf("graceful shutdown of server successful")
		os.Exit(0)
	case e := <-errChan:
		log.Printf("failed to launched http server: %v", e)
		os.Exit(1)
	}
}
