package main

import (
	"github.com/crgimenes/goconfig"
	"github.com/go-redis/redis"
	mw "gitlab.com/michaelk99/saturdate/server/pkg/middleware"
	"gitlab.com/michaelk99/saturdate/server/pkg/token/jwthmac"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/geo"
	"gitlab.com/michaelk99/saturdate/server/services/geo/handlers"
	geostore "gitlab.com/michaelk99/saturdate/server/services/geo/redis"
	v9 "gopkg.in/go-playground/validator.v9"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// Config this struct is using the goconfig library for simple flag and env var
// parsing. See: https://github.com/crgimenes/goconfig
type Config struct {
	HTTPListenAddr string `cfgDefault:"0.0.0.0:8003" cfg:"GEO_HTTP_LISTEN_ADDR"`
	RedisAddr      string `cfgDefault:"redis:6379" cfg:"REDIS_ADDR"`
	RedisPass      string `cfgDefault:"" cfg:"REDIS_PASS"`
	JWTSecret      string `cfgDefault:"fde5247c0262798a9c" cfg:"JWT_SECRET"`
}

// root is the root route, used for k8s health checks
func root(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "geo service healthy")
}

func main() {
	// parse our config
	conf := Config{}
	err := goconfig.Parse(&conf)
	if err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// create token store, which will be used to handle jwt authentication
	jwthmacStore := jwthmac.NewTokenStore([]byte(conf.JWTSecret), "HMAC")

	v9Validator := v9.New()
	validator := validator.NewValidator(v9Validator)

	client := redis.NewClient(&redis.Options{
		Addr:     conf.RedisAddr,
		Password: conf.RedisPass,
		DB:       0,
	})

	_, err = client.Ping().Result()
	if err != nil {
		log.Fatalf("failed to connect to redis: %v", err)
	}

	// create geo store (redis under the hood)
	ds := geostore.NewGeoStore(client)

	// create our service
	service := geo.NewService(ds)

	// create our http mux and add routes
	mux := http.NewServeMux()

	// Create (Ping lat/lon)
	mux.Handle("/api/v1/geo/", mw.Auth(jwthmacStore, handlers.Create(validator, service)))

	// Search in geo store
	mux.Handle("/api/v1/geo/search/", mw.Auth(jwthmacStore, handlers.Search(validator, service)))

	// not found route
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			io.WriteString(w, "Route not found\n")
			return
		}
		root(w, r)
	})

	// create server and launch in go routine
	s := http.Server{
		Addr:    conf.HTTPListenAddr,
		Handler: mux,
	}

	sigChan := make(chan os.Signal)
	errChan := make(chan error)

	go func(errChan chan error, s http.Server) {
		log.Printf("launching http server on %v", s.Addr)
		err := s.ListenAndServe()
		if err != nil {
			errChan <- err
		}
	}(errChan, s)

	// register signal handler
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// block on sigChan or errChan
	select {
	case sig := <-sigChan:
		log.Printf("received signal %v. attempting graceful shutdown of server", sig.String())

		err = s.Shutdown(nil)
		if err != nil {
			log.Printf("graceful shutdown of server unsuccessful: %v", err)
			os.Exit(1)
		}

		log.Printf("graceful shutdown of server successful")
		os.Exit(0)
	case e := <-errChan:
		log.Printf("failed to launched http server: %v", e)
		os.Exit(1)
	}
}
