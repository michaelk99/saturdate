package main

import (
	"github.com/crgimenes/goconfig"
	"github.com/jmoiron/sqlx"
	"gitlab.com/michaelk99/saturdate/server/pkg/client"
	mw "gitlab.com/michaelk99/saturdate/server/pkg/middleware"
	"gitlab.com/michaelk99/saturdate/server/pkg/token/jwthmac"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/event"
	"gitlab.com/michaelk99/saturdate/server/services/event/handlers"
	"gitlab.com/michaelk99/saturdate/server/services/event/postgres"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	v9 "gopkg.in/go-playground/validator.v9"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
)

// Config this struct is using the goconfig library for simple flag and env var
// parsing. See: https://github.com/crgimenes/goconfig
type Config struct {
	HTTPListenAddr string `cfgDefault:"0.0.0.0:8005" cfg:"EVENT_HTTP_LISTEN_ADDR"`
	JWTSecret      string `cfgDefault:"fde5247c0262798a9c" cfg:"JWT_SECRET"`
	PGConnString   string `cfgDefault:"host=localhost port=5432 user=postgres dbname=saturdate  sslmode=disable" cfg:"POSTGRES_CONN_STRING"`
	PGDriver       string `cfgDefault:"postgres" cfg:"POSTGRES_DRIVER"`
	ProfileURL     string `cfgDefault:"http://profile.saturdate/server.local/api/v1/profile" cfg:"PROFILE_URL"`
	MatchURL       string `cfgDefault:"http://match.saturdate/server.local/api/v1/matches" cfg:"MATCH_URL"`
}

// root is the root route, used for k8s health checks
func root(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "event service healthy")
}

func main() {
	// parse our config
	conf := Config{}
	err := goconfig.Parse(&conf)
	if err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// create token store, which will be used to handle jwt authentication
	jwthmacStore := jwthmac.NewTokenStore([]byte(conf.JWTSecret), "HMAC")

	v9Validator := v9.New()
	validator := validator.NewValidator(v9Validator)

	// create conn to db
	dbConn, err := sqlx.Connect(conf.PGDriver, conf.PGConnString)
	if err != nil {
		log.Fatalf("failed to connect to DB: %v", err)
	}

	// create event store (postgres under the hood)
	ds := postgres.NewEventStore(dbConn)

	profURL, err := url.Parse(conf.ProfileURL)
	if err != nil {
		log.Fatal("failed to parse profile url")
	}

	pc := profile.NewClient(profURL)

	matchURL, err := url.Parse(conf.MatchURL)
	if err != nil {
		log.Fatal("failed to parse match url")
	}

	mc := client.NewMatchClient(matchURL)

	// create our service
	service := event.NewService(ds, mc, pc)

	// create our http mux and add routes
	mux := http.NewServeMux()

	mux.Handle("/api/v1/events/", mw.Auth(jwthmacStore, handlers.CRUD(validator, service)))

	// not found route
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			io.WriteString(w, "Route not found\n")
			return
		}
		root(w, r)
	})

	// create server and launch in go routine
	s := http.Server{
		Addr:    conf.HTTPListenAddr,
		Handler: mux,
	}

	sigChan := make(chan os.Signal)
	errChan := make(chan error)

	go func(errChan chan error, s http.Server) {
		log.Printf("launching http server on %v", s.Addr)
		err := s.ListenAndServe()
		if err != nil {
			errChan <- err
		}
	}(errChan, s)

	// register signal handler
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// block on sigChan or errChan
	select {
	case sig := <-sigChan:
		log.Printf("received signal %v. attempting graceful shutdown of server", sig.String())

		err = s.Shutdown(nil)
		if err != nil {
			log.Printf("graceful shutdown of server unsuccessful: %v", err)
			os.Exit(1)
		}

		log.Printf("graceful shutdown of server successful")
		os.Exit(0)
	case e := <-errChan:
		log.Printf("failed to launched http server: %v", e)
		os.Exit(1)
	}
}
