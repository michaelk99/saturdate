package main

import (
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/crgimenes/goconfig"
	"github.com/jmoiron/sqlx"
	mw "gitlab.com/michaelk99/saturdate/server/pkg/middleware"
	"gitlab.com/michaelk99/saturdate/server/pkg/token/jwthmac"

	"gitlab.com/michaelk99/saturdate/server/services/image"
	"gitlab.com/michaelk99/saturdate/server/services/image/handlers"
	"gitlab.com/michaelk99/saturdate/server/services/image/postgres"
	s3store "gitlab.com/michaelk99/saturdate/server/services/image/s3"
	"gitlab.com/michaelk99/saturdate/server/services/profile"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"

	_ "github.com/lib/pq"
)

// Config this struct is using the goconfig library for simple flag and env var
// parsing. See: https://github.com/crgimenes/goconfig
type Config struct {
	HTTPListenAddr string `cfgDefault:"0.0.0.0:8002" cfg:"IMAGE_HTTP_LISTEN_ADDR"`
	PGConnString   string `cfgDefault:"host=localhost port=5432 user=postgres dbname=saturdate  sslmode=disable" cfg:"POSTGRES_CONN_STRING"`
	PGDriver       string `cfgDefault:"postgres" cfg:"POSTGRES_DRIVER"`
	JWTSecret      string `cfgDefault:"fde5247c0262798a9c" cfg:"JWT_SECRET"`
	AccountURL     string `cfgDefault:"http://account.saturdate/server.local/api/v1/account" cfg:"ACCOUNT_URL"`
	ProfileURL     string `cfgDefault:"http://profile.saturdate/server.local/api/v1/profile" cfg:"PROFILE_URL"`
	ImageStoreType string `cfgDefault:"s3" cfg:"IMAGE_STORE_TYPE"`
	S3BucketName   string `cfgDefault:"local-saturdate-assets" cfg:"S3_BUCKET_NAME"`
	S3UploadURL    string `cfgDefault:"https://local-saturdate-assets.s3.amazonaws.com" cfg:"S3_UPLOAD_URL"`
	S3PathPrefix   string `cfgDefault:"images" cfg:"S3_PATH_PREFIX"`
	AWSAccessKey   string `cfgDefault:"access-key" cfg:"AWS_ACCESS_KEY_ID"`
	AWSSecretKey   string `cfgDefault:"secret-key" cfg:"AWS_SECRET_ACCESS_KEY"`
	AWSRegion      string `cfgDefault:"us-east-1" cfg:"AWS_REGION"`
}

// root is the root route, used for k8s health checks
func root(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "healthy")
}

func main() {
	// parse our config
	conf := Config{}
	err := goconfig.Parse(&conf)
	if err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// create token store, which will be used to handle jwt authentication
	jwthmacStore := jwthmac.NewTokenStore([]byte(conf.JWTSecret), "HMAC")

	// init our image meta store for image metadata storing
	dbConn, err := sqlx.Connect(conf.PGDriver, conf.PGConnString)
	if err != nil {
		log.Fatalf("failed to connect to DB: %v", err)
	}
	ds := postgres.NewImageMetaStore(dbConn)

	// init our image store for binary image storing
	var is image.ImageStore
	switch strings.ToLower(conf.ImageStoreType) {
	case "file":
		log.Fatalf("file image store not impemented")
	case "s3":
		if conf.S3BucketName == "" {
			log.Printf("attempting to use s3 image store without bucket name. provide a bucket name in the env vars or cli arguments")
		}
		if conf.S3PathPrefix == "" {
			log.Printf("attempting to use s3 image store without a path prefix")
		}

		// create aws connection
		token := ""
		creds := credentials.NewStaticCredentials(conf.AWSAccessKey, conf.AWSSecretKey, token)

		_, err = creds.Get()
		if err != nil {
			log.Fatalf("failed to connect to aws: %v", err)
		}

		// create the aws config from the creds
		cfg := aws.NewConfig().WithRegion(conf.AWSRegion).WithCredentials(creds)

		// instantiate with the right creds
		svc := s3.New(session.New(), cfg)

		is, err = s3store.NewImageStore(conf.S3BucketName, conf.S3UploadURL, conf.S3PathPrefix, svc)
		if err != nil {
			log.Fatalf("failed to create image store: %v", err)
		}
	}

	profURL, err := url.Parse(conf.ProfileURL)
	if err != nil {
		log.Fatal("failed to parse profile url")
	}

	// TODO
	pc := profile.NewClient(profURL)

	// create our service
	service := image.NewService(ds, is, pc)

	// create our http mux and add routes
	mux := http.NewServeMux()

	// declare custom routes that do not go through basic CRUD
	mux.Handle("/api/v1/images/search/", mw.Auth(jwthmacStore, handlers.Search(service)))
	mux.Handle("/api/v1/images/", mw.Auth(jwthmacStore, handlers.CRUD(service)))

	// not found route
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			io.WriteString(w, "Route not found\n")
			return
		}
		root(w, r)
	})

	// create server and launch in go routine
	s := http.Server{
		Addr:    conf.HTTPListenAddr,
		Handler: mux,
	}

	sigChan := make(chan os.Signal)
	errChan := make(chan error)

	go func(errChan chan error, s http.Server) {
		log.Printf("launching http server on %v", s.Addr)
		err := s.ListenAndServe()
		if err != nil {
			errChan <- err
		}
	}(errChan, s)

	// register signal handler
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// block on sigChan or errChan
	select {
	case sig := <-sigChan:
		log.Printf("received signal %v. attempting graceful shutdown of server", sig.String())

		err = s.Shutdown(nil)
		if err != nil {
			log.Printf("graceful shutdown of server unsuccessful: %v", err)
			os.Exit(1)
		}

		log.Printf("graceful shutdown of server successful")
		os.Exit(0)
	case e := <-errChan:
		log.Printf("failed to launched http server: %v", e)
		os.Exit(1)
	}
}
