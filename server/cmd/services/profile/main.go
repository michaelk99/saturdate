package main

import (
	"github.com/crgimenes/goconfig"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	mw "gitlab.com/michaelk99/saturdate/server/pkg/middleware"
	"gitlab.com/michaelk99/saturdate/server/pkg/token/jwthmac"
	"gitlab.com/michaelk99/saturdate/server/pkg/validator"
	"gitlab.com/michaelk99/saturdate/server/services/profile"
	"gitlab.com/michaelk99/saturdate/server/services/profile/handlers"
	"gitlab.com/michaelk99/saturdate/server/services/profile/postgres"
	v9 "gopkg.in/go-playground/validator.v9"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// Config this struct is using the goconfig library for simple flag and env var
// parsing. See: https://github.com/crgimenes/goconfig
type Config struct {
	HTTPListenAddr string `cfgDefault:"0.0.0.0:8001" cfg:"PROFILE_HTTP_LISTEN_ADDR"`
	PGConnString   string `cfgDefault:"host=localhost port=5432 user=postgres dbname=saturdate  sslmode=disable" cfg:"POSTGRES_CONN_STRING"`
	PGDriver       string `cfgDefault:"postgres" cfg:"POSTGRES_DRIVER"`
	JWTSecret      string `cfgDefault:"fde5247c0262798a9c" cfg:"JWT_SECRET"`
}

// root is the root route, used for k8s health checks
func root(res http.ResponseWriter, req *http.Request) {
	io.WriteString(res, "healthy")
}

func main() {
	// parse our config
	conf := Config{}
	err := goconfig.Parse(&conf)
	if err != nil {
		log.Fatalf("failed to parse config: %v", err)
	}

	// create token store, which will be used to handle jwt authentication
	jwthmacStore := jwthmac.NewTokenStore([]byte(conf.JWTSecret), "HMAC")

	// create conn to db
	dbConn, err := sqlx.Connect(conf.PGDriver, conf.PGConnString)
	if err != nil {
		log.Fatalf("failed to connect to DB: %v", err)
	}
	ds := postgres.NewProfileStore(dbConn)

	v9Validator := v9.New()
	validator := validator.NewValidator(v9Validator)

	// create our service
	service := profile.NewService(ds)

	// create our http mux and add routes
	mux := http.NewServeMux()

	mux.Handle("/api/v1/profile/", mw.Auth(jwthmacStore, handlers.CRUD(validator, service)))
	mux.Handle("/api/v1/profile/search/", mw.Auth(jwthmacStore, handlers.Search(service)))

	// not found route
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusNotFound)
			io.WriteString(w, "Route not found\n")
			return
		}
		root(w, r)
	})

	// create server and launch in go routine
	s := http.Server{
		Addr:    conf.HTTPListenAddr,
		Handler: mux,
	}

	sigChan := make(chan os.Signal)
	errChan := make(chan error)

	go func(errChan chan error, s http.Server) {
		log.Printf("launching http server on %v", s.Addr)
		err := s.ListenAndServe()
		if err != nil {
			errChan <- err
		}
	}(errChan, s)

	// register signal handler
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	// block on sigChan or errChan
	select {
	case sig := <-sigChan:
		log.Printf("received signal %v. attempting graceful shutdown of server", sig.String())

		err = s.Shutdown(nil)
		if err != nil {
			log.Printf("graceful shutdown of server unsuccessful: %v", err)
			os.Exit(1)
		}

		log.Printf("graceful shutdown of server successful")
		os.Exit(0)
	case e := <-errChan:
		log.Printf("failed to launched http server: %v", e)
		os.Exit(1)
	}
}
