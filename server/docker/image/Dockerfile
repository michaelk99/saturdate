# Multi-stage build. See:
# https://docs.docker.com/prodelop/prodelop-images/multistage-build/ Builds the
# go binary in a container with the runtime then copies to a stripped down
# container
FROM golang:latest
WORKDIR /go/src/gitlab.com/michaelk99/saturdate-go
COPY . .

RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure

# create vendor folder pulling in dependent libraries
RUN ls ./vendor # confirm vendor folder with dependencies are present

# build
RUN cd ./cmd/services/image && CGO_ENABLED=0 GOOS=linux go build -a -o image .

# second stage. copy built binary to alpine container and set entry point to
# the compiled binary
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /

# update
RUN apk update

# Copy entrypoint.sh over
COPY --from=0 /go/src/gitlab.com/michaelk99/saturdate-go/docker/image/entrypoint.sh .

# ensure entrypoint.sh is an executable
RUN chmod +x entrypoint.sh

# copy binary and service image
COPY --from=0 /go/src/gitlab.com/michaelk99/saturdate-go/cmd/services/image/image .

# run the server
ENTRYPOINT ["sh", "/entrypoint.sh"]
