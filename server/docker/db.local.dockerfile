FROM postgres:11-alpine

COPY ./bootstrap.sql ./docker-entrypoint-initdb.d/bootstrap.sql
