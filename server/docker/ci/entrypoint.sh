# fail fast
set -e

# wait on postgres to be availale
while ! nc -z postgres 5432; do
    echo "waiting for postgres to be available at postgres:5432"
    sleep 1
done

echo "running tests..."

# run tests
go test ./...
