# Saturdate
We built Saturdate to help you get to an in person date quicker.

Our goal is to get you to connect in person faster, remove the first date
jitters, and provide a more intimate experience when contrasted with other fast
selection apps like Tinder and Bumble.

## The match experience
Here's the general flow of the matching experience. The swipe experience remains
the same as other dating apps. The main differences occur post-match.

1. Before you decide to swipe right, make sure you are free for that Saturday
   night.

2. Swipe as normal, but keep in mind: only swipe right if you see yourself
   meeting in person.

3. When you match with someone: if you were the one to "complete" the match
   ("Person 1"), you will be prompted to choose from a pre selected list of 5
   bars located in NYC. Sidenote: all bars are scraped from yelp. The 5 bars
   listed to Person 1 are based on geolocation (1 bar that's near Person 1, 1
   bar that's near Person 2, and 3 bars located at the midpoint).

4. Person 1 must select 3/5 bars from the list in order to continue the
   post-match process.

5. Person 2 goes into the saturdate ios app, he/she will get notified about the
   match if Person 1 completed their bar selection. Person 2 will be presented
   with that list of 3 bars. Person 2 must now select 1 bar from the list of 3
   being presented.

6. Once a bar is mutually agreed upon, we set you up to meet at 8pm on that
   Saturday night. You can never have more than one date for Saturday night. Of
   course, you might match multiple times within a week. Those matches will get
   queued up and you won't know about them until a Saturday night is free for both
   Person 1 and Person 2.

7. We get that schedules can change. Ideally we have mechanisms in place to
   handle cancelations, no-shows, etc. 

8. Oh, and one more important thing. There's no chat within the app. We will
   have opportunities for users to verify themselves with their linkedin
   profile (this would come at a cost, i.e., $10). We also plan on having a
   "show rate" on a user's profile, i.e., 70% show rate, 85% show rate so you can
   gauge how likely this person is to show up in person.

## Technical Implementation

### server
- Microservices in go [100% complete]
- Needs testing/beta period, refinements to the code as well.

### ios
- Swift [80% complete]

### django admin
- For admins, an easy UI to view activity within the app [100% complete]
